// 修正IE6每次从服务器读取背景图片的BUG
try {
    if ($.browser.msie && $.browser.version == "6.0") {
		document.execCommand("BackgroundImageCache", false, true);
	}
}catch(e){
}

if (typeof Fai == 'undefined'){
	Fai = {};
	Fai.top = top;
}

Fai.openLog = false;
Fai.isDbg = function() {
	var faiDebug = Fai.Cookie.get("_fai_debug");
	return faiDebug == "true";
};

(function(FUNC, undefined){
	FUNC.fkEval = function(data){
		return eval(data);
	};
})(Fai);
/**
 * 调试Fai.logDbg(args...);
 * 用于网站调式，这里会拿对应的cookie来判断
 * cookie标志位写入设置在WebAppFilter中Response中自动写入debug模式下的cookies(完成) [fai.website, fai.webportal]
 */
Fai.logDbg = function(){
	var faiDebug = Fai.isDbg();
	if(faiDebug || Fai.openLog){
		var args = $.makeArray(arguments);
		if(Fai.isIE()){
			var html = '<div id="faiDebugMsg" style="position:absolute; top:30px; left:45%; margin:0px auto; width:auto; height:auto; z-index:9999;"></div>';
			var faiDebugMsg = Fai.top.$("#faiDebugMsg");
			if(faiDebugMsg.length == 0){
				faiDebugMsg = Fai.top.$(html);
				faiDebugMsg.appendTo("body");
			}
			Fai.top.$('<div class="tips" style="position:relative; top:0px; left:-50%; width:auto; _width:50px; height:24px; margin:3px 0; line-height:24px; color:#000000; border:1px solid #EAEA00; background:#FFFFC4; z-index:9999;"><div class="msg" style="width:auto; margin:0 3px; height:24px; line-height:24px; word-break:keep-all; white-space:nowrap;">' + args.join('') + '</div></div>').appendTo(faiDebugMsg);
		}else{
			console.log(args.join(''));
		}
	}
};
/**
 * 调试Fai.logAlert(args...);
 * 直接alert的
 */
Fai.logAlert = function(){
	var faiDebug = Fai.Cookie.get("_fai_debug");
	if(faiDebug == "true" || Fai.openLog){
		var args = $.makeArray(arguments);
		alert(args.join(''));
	}
};

/**
 * 为文本中url加锚
 * Fai.replaceContentOfURL("aaa bbb ccc http://www.faisco.cn ok");
 * return "aaa bbb ccc <a href="http://www.faisco.cn">http://www.faisco.cn</a> ok";
 */
Fai.replaceContentOfURL = function(srcStr){
	return (srcStr.replace(/((https?|ftp|file):\/\/[-a-zA-Z0-9+&@#\/%?=~_|!:,.;]*)/g,"<a href=\"$1\" target=\"_blank\">$1</a>"));
};

/*
	本方法只支持utf-8和utf-16的编码规则，根据传入的字符串获取字节长度
	e.g. Fai.getByteLength("xxx","utf-16");utf-8的可以传参省略，如Fai.getByteLength("xxx");
	utf-8 编码规则：
	字符代码在000000 – 00007F之间的，用一个字节编码；
	000080 – 0007FF之间的字符用两个字节；
	000800 – 00D7FF 和 00E000 – 00FFFF之间的用三个字节，注: Unicode在范围 D800-DFFF 中不存在任何字符；
	010000 – 10FFFF之间的用4个字节。
	utf-16 编码规则：
	000000 – 00FFFF 两个字节；
	010000 – 10FFFF 四个字节。
*/

Fai.getByteLength = function(str, charset){
    var total = 0,
        charCode,
        i,
        len;
    charset = charset ? charset.toLowerCase() : '';
    if(charset === 'utf-16' || charset === 'utf16'){
        for(i = 0, len = str.length; i < len; i++){
            charCode = str.charCodeAt(i);
            if(charCode <= 0xffff){
                total += 2;
            }else{
                total += 4;
            }
        }
    }else{
        for(i = 0, len = str.length; i < len; i++){
            charCode = str.charCodeAt(i);
            if(charCode <= 0x007f) {
                total += 1;
            }else if(charCode <= 0x07ff){
                total += 2;
            }else if(charCode <= 0xffff){
                total += 3;
            }else{
                total += 4;
            }
        }
    }
	return total;
}

// 注意不能用于未声明的顶层变量的判断，例如不能Fai.isNull(abc)，只能Fai.isNull(abc.d)
Fai.isNull = function (obj){
	return (typeof obj == 'undefined') || (obj == null);
};

// 此方法作废,请使用$.isArray()代替。原因：constructor在跨浏览器时会检测错误
//Fai.isArray = function (obj){
//	if(obj.constructor == window.Array){return true;}
//	else{return false;}
//}
Fai.isDate = function (obj){
	if(obj.constructor == Date){return true;}
	else{return false;}
};
Fai.isNumber = function (obj){//数字则返回true
	if (/[^\d]/.test(obj)){return false;}
	else{ return true;}
};
/**
 * check is Number
 * 判断是否浮点数字
 * e.g. -1 1.12
 * @param {numVal} @return boolean isFloat:true/false
 *     $.isFloat(numVal)
 */
Fai.isFloat = function(numVal){
	return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(numVal);
};
/**
 * check is Integer
 * 判断是否整数
 * e.g. -1 0 1
 * @param {numVal} @return boolean isInteger:true/false
 *     $.isInteger(numVal)
 */
Fai.isInteger = function(numVal){
	return /^-?\d+$/.test(numVal);
};

/**
 * 判断是小写
 */
Fai.isLowerCase = function(value){
	return /^[a-z]+$/.test(value);
};

/**
 * 决断是大写
 */
Fai.isUpperCase = function(value){
	return /^[A-Z]+$/.test(value);
};

/**
 * 把首字母转成小写
 */
Fai.toLowerCaseFirstOne = function(string){
	if(typeof string === "undefined" || Fai.isLowerCase(string.charAt(0))){
		return string;
	}else{
		var h = string.substring(0, 1).toLowerCase();
		var t = string.substring(1, string.length);
		return h + t;
	}
};

/**
 * 把首字母转成大写
 */
Fai.toUpperCaseFirstOne = function(string){
	if(typeof string === "undefined" || Fai.isUpperCase(string.charAt(0))){
		return string;
	}else{
		var h = string.substring(0, 1).toUpperCase();
		var t = string.substring(1, string.length);
		return h + t;
	}
};

/*
 * 判断是否整数数字
 */
//Fai.isDigit = function(numVal){
//	return /^\d+$/.test(numVal);
//};
Fai.isDigit = function(numVal){
	if (numVal < '0' || numVal > '9') {
		return false;
	}
	
	return true;
};

/*
 * 判断是否字母
 */
//Fai.isLetter = function(val){
//	return /^[a-zA-Z]+$/g.test(val);
//}
Fai.isLetter = function(val){
	if ((val < 'a' || val > 'z') && (val < 'A' || val > 'Z')) {
		return false;
	}
	
	return true;
};

/*
 * 判断是否中文
 */
//Fai.isChinese = function(val){
//	return /.*[\u4e00-\u9fa5]+.*$/.test(val);
//}
Fai.isChinese = function(val){
	if (val < '一' || val > '龥') {
		return false;
	}
	
	return true;
};

Fai.isIp = function( ipaddr ){
	if( typeof ipaddr != 'string' || $.trim(ipaddr) == "" ){
		return false;
	}
	var ss = ipaddr.split(".");
	if(ss.length != 4){
		return false;
	}
	var result = true;
	$.each( ss, function( i, e ){
		if ( !Fai.isNumber(  e ) || parseInt( e ) < 0 || parseInt( e )>255 )
		{
			result = false;
			return true;
		}
	});
	return result;
}

Fai.isDomain = function (obj){
	if (/^[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]$/.test(obj)){
		if (obj.indexOf("--") >= 0) {
			return false;
		}
		return true;
	} else{ 
		return false;
	}
};
Fai.isWord = function(word){
	var pattern = /^[a-zA-Z0-9_]+$/;   
	return pattern.test(word);
};
Fai.isEmail = function(email){
	var pattern = /^[a-zA-Z0-9][a-zA-Z0-9_=\&\-\.\+]*[a-zA-Z0-9]*@[a-zA-Z0-9][a-zA-Z0-9_\-\.]+[a-zA-Z0-9]$/;
	return pattern.test(email);
};
Fai.isEmailDomain = function(email){
	var pattern = /^[a-zA-Z0-9][a-zA-Z0-9_\-]*\.[a-zA-Z0-9\-][a-zA-Z0-9_\-\.]*[a-zA-Z0-9]$/;
	return pattern.test(email);
};
Fai.isMobile = function(mobile){
	var pattern = /^1\d{10}$/;
	return pattern.test(mobile);
};
Fai.isPhone = function(phone){
	var pattern1 = /^([^\d])+([^\d])*([^\d])$/;
	var pattern2 = /^([\d\+\s\(\)-])+([\d\+\s\(\)-])*([\d\+\s\(\)-])$/;
	//var p1 = /^(([0\+]\d{2,3}-)?(0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/;
	if( pattern1.test(phone) ){
		return false;
	}
	return pattern2.test(phone);
};
Fai.isNationMobile = function(mobile){
	var pattern = /^\d{8,14}$/;
	return pattern.test(mobile);
};
//验证身份证号码
Fai.isCardNo = function(card){
	var pattern = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;  
  	return pattern.test(card);
};
//supAbsuRoot   是否支持绝对路径
Fai.isUrl = function(str_url,supAbsuRoot){
	if (typeof supAbsuRoot == "undefined") {
		supAbsuRoot = true;
	}
	if(supAbsuRoot && str_url.length >= 1 && str_url.charAt(0) == '/'){
		return true;
	}
	if(supAbsuRoot && str_url.length >= 1 && str_url.charAt(0) == '#'){
		return true;
	}

//	var re=/^(\w+:\/\/).+/;
	var re=/^(\w+:).+/;
	var result = re.test(str_url);	
	return result;
};
Fai.fixUrl = function(url, supAbsuRoot){
	if (Fai.isUrl(url, supAbsuRoot)) {
		return url;
	}
	return 'http://' + url;
};
Fai.checkBit = function (flag, bitFlag) {
	//return (flag & bitFlag) == bitFlag;
	/*
	因为js位操作有关的超过了32位后无效。所有位置0。第32位代表负数。且32位的左移1位就是直接跳回到第1位。  与java long类型移位操作不符。
	20131225修改  支持long 类型62位内的checkBit 
	*/
	var bit_31 = true;
	//31位皆置1为：2147483647
	if( flag > 2147483647 || flag < 0 || bitFlag > 2147483647 || bitFlag < 0 ){
		bit_31 = false;
	}
	if( bit_31 ){
		return (flag & bitFlag) == bitFlag;
	}
	
	var flagBinary = flag.toString(2);
	var bitFlagBinary = bitFlag.toString(2);
	if( flagBinary.length > 62 || bitFlagBinary.length > 62 ){
		alert( "Does not support more than 62 bit. flagBinary.length=" + flagBinary.length + ",bitFlagBinary.length" + bitFlagBinary.length + "." );
		return false;
	}
	//flagBinary = flagBinary.split("").reverse().join("");
	//bitFlagBinary = bitFlagBinary.split("").reverse().join("");
	var flagHight=flagLow=bitFlagHight=bitFlagLow=0;
	//拆分高低位处理
	if( flagBinary.length > 31 ){
		var hightStr = flagBinary.slice(0,flagBinary.length-31);
		var lowStr = flagBinary.slice(flagBinary.length-31);
		flagHight = parseInt( hightStr, "2" );
		flagLow = parseInt( lowStr, "2" );
	}else{
		flagLow = parseInt( flagBinary.slice(0,flagBinary.length), "2" );
	}
	if( bitFlagBinary.length > 31 ){
		var hightStr = bitFlagBinary.slice(0,bitFlagBinary.length-31);
		var lowStr = bitFlagBinary.slice(bitFlagBinary.length-31);
		bitFlagHight = parseInt( hightStr, "2" );
		bitFlagLow = parseInt( lowStr, "2" );
	}else{
		bitFlagLow = parseInt( bitFlagBinary.slice(0,bitFlagBinary.length), "2" );
	}
	
	var result = (flagLow & bitFlagLow) == bitFlagLow;
	if( result ){
		result = (flagHight & bitFlagHight) == bitFlagHight;
	}
	return result;
};

Fai.andBit = function( flag, bitFlag ){
	var bit_31 = true;
	//31位皆置1为：2147483647
	if( flag > 2147483647 || flag < 0 || bitFlag > 2147483647 || bitFlag < 0 ){
		bit_31 = false;
	}
	if( bit_31 ){
		return flag &= bitFlag;
	}
	
	var flagBinary = flag.toString(2);
	var bitFlagBinary = bitFlag.toString(2);
	if( flagBinary.length > 62 || bitFlagBinary.length > 62 ){
		alert( "Does not support more than 62 bit. flagBinary.length=" + flagBinary.length + ",bitFlagBinary.length" + bitFlagBinary.length + "." );
		return 0;
	}
	var flagHight=flagLow=bitFlagHight=bitFlagLow=0;
	//拆分高低位处理
	if( flagBinary.length > 31 ){
		var hightStr = flagBinary.slice(0,flagBinary.length-31);
		var lowStr = flagBinary.slice(flagBinary.length-31);
		flagHight = parseInt( hightStr, "2" );
		flagLow = parseInt( lowStr, "2" );
	}else{
		flagLow = parseInt( flagBinary.slice(0,flagBinary.length), "2" );
	}
	if( bitFlagBinary.length > 31 ){
		var hightStr = bitFlagBinary.slice(0,bitFlagBinary.length-31);
		var lowStr = bitFlagBinary.slice(bitFlagBinary.length-31);
		bitFlagHight = parseInt( hightStr, "2" );
		bitFlagLow = parseInt( lowStr, "2" );
	}else{
		bitFlagLow = parseInt( bitFlagBinary.slice(0,bitFlagBinary.length), "2" );
	}
	flagLow &= bitFlagLow;
	flagHight &= bitFlagHight;
	flagBinary = flagLow.toString(2);
	//低位补0
	for( ;flagBinary.length < 31; ){
		flagBinary = "0" + flagBinary;
	}
	flagBinary = flagHight.toString(2)+flagBinary;
	return parseInt( flagBinary, "2" );
};

Fai.orBit = function( flag, bitFlag ){
	var bit_31 = true;
	if( flag > 2147483647 || flag < 0 || bitFlag > 2147483647 || bitFlag < 0 ){
		bit_31 = false;
	}
	if( bit_31 ){
		return flag |= bitFlag;
	}
	
	var flagBinary = flag.toString(2);
	var bitFlagBinary = bitFlag.toString(2);
	if( flagBinary.length > 62 || bitFlagBinary.length > 62 ){
		alert( "Does not support more than 62 bit. flagBinary.length=" + flagBinary.length + ",bitFlagBinary.length" + bitFlagBinary.length + "." );
		return 0;
	}
	var flagHight=flagLow=bitFlagHight=bitFlagLow=0;
	if( flagBinary.length > 31 ){
		var hightStr = flagBinary.slice(0,flagBinary.length-31);
		var lowStr = flagBinary.slice(flagBinary.length-31);
		flagHight = parseInt( hightStr, "2" );
		flagLow = parseInt( lowStr, "2" );
	}else{
		flagLow = parseInt( flagBinary.slice(0,flagBinary.length), "2" );
	}
	if( bitFlagBinary.length > 31 ){
		var hightStr = bitFlagBinary.slice(0,bitFlagBinary.length-31);
		var lowStr = bitFlagBinary.slice(bitFlagBinary.length-31);
		bitFlagHight = parseInt( hightStr, "2" );
		bitFlagLow = parseInt( lowStr, "2" );
	}else{
		bitFlagLow = parseInt( bitFlagBinary.slice(0,bitFlagBinary.length), "2" );
	}
	flagLow |= bitFlagLow;
	flagHight |= bitFlagHight;
	flagBinary = flagLow.toString(2);
	//低位补0
	for( ;flagBinary.length < 31; ){
		flagBinary = "0" + flagBinary;
	}
	flagBinary = flagHight.toString(2)+flagBinary;
	return parseInt( flagBinary, "2" );
};

Fai.renderUEditor = function( options){
	var defaultOptions = {
		ueditorId : null,
		setPageChange : null,
		initContent : null,
		minFrameHeight : 0,
		faiscoRichTip : null,
		withPage : null
	};
	
	$.extend(defaultOptions, options);
	var editor = new baidu.editor.ui.Editor({
		upLoadFlashUrl:"/ajax/upfile_h.jsp?type=50",
		upLoadImageUrl:"/ajax/upimg_h.jsp",
		ueditorChangeEvent:defaultOptions.setPageChange,
		htmlModuleRichTip:defaultOptions.faiscoRichTip,
		initialContent:defaultOptions.initContent,
		minFrameHeight:defaultOptions.minFrameHeight,
		toolbars:[[
					'shrinkopenup','removeformat','|', 'bold', 'italic', 'underline',  
					'|','fontfamily', 'fontsize', 'forecolor', 'backcolor',
					'|','insertorderedlist','insertunorderedlist','lineheight',
					
					'justifyright', '|',
					'link', 'unlink','qqservice', 
					'image', 'flash',
					'inserttable',defaultOptions.withPage,'|','source','||',
					'pasteplain','|','selectall','undo', 'redo','|','strikethrough','superscript', 'subscript','horizontal',
					'|','indent','rowspacingtop','rowspacingbottom','|',
					'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols',
					 '|','fullscreen'
		]]
	});
	editor.render( defaultOptions.ueditorId );
	return editor;
};
Fai.isEnterKey = function (e){
	 if ($.browser.msie) {   
        if (event.keyCode == 13) {   
            return true;   
        } else {   
            return false;   
        }   
    } else {   
        if (e.which == 13) {   
            return true;   
        } else {   
            return false;   
        }   
    }   
};
Fai.isNumberKey = function (e, iSminus){//按下数字键则返回true,用法：<input type="text" onkeypress="javascript:return Fai.isNumberKey(event);"/>
	 if ($.browser.msie) {
		 if(iSminus && event.keyCode == 45){
			return true; 
		}   
        if ( ((event.keyCode > 47) && (event.keyCode < 58)) ||   
              (event.keyCode == 8) ) {   
            return true;   
        } else {   
            return false;   
        }   
    } else {
		if(iSminus && e.which == 45){
			return true; 
		}
        if ( ((e.which > 47) && (e.which < 58)) ||   
              (e.which == 8) ) {   
            return true;   
        } else {   
            return false;   
        }   
    } 
};
//按下数字键则返回true,用法：<input type="text" onkeypress="javascript:return Fai.isNumberKey(event);"/>
//在isNumberKey函数下增加一个","逗号键入，兼容直拨分机号
Fai.isPhoneNumberKey = function (e, iSminus){
	 if ($.browser.msie) {
		 if(iSminus && event.keyCode == 45){
			return true; 
		}   
        if ( ((event.keyCode > 47) && (event.keyCode < 58)) ||   
              (event.keyCode == 8) || (event.keyCode == 44) ) {   
            return true;   
        } else {   
            return false;   
        }   
    } else {
		if(iSminus && e.which == 45){
			return true; 
		}
        if ( ((e.which > 47) && (e.which < 58)) ||   
              (e.which == 8) || (e.which == 44) ) {   
            return true;   
        } else {   
            return false;   
        }   
    } 
};
//控制只能输入小数点后两位
Fai.checkTwoDecimal  = function(e, id){
	var val = $("#"+id).val();
	var reg = /^[0-9]\d*(?:\.\d{1,2}|\d*)$/;
	 if ($.browser.msie) {
		if( event.keyCode > 47 && event.keyCode < 58){
			var keyVal = String.fromCharCode(e.which);  
			val = val + "" +keyVal;
			return reg.test(val);
		}
		
    } else {
		if( e.which > 47 && e.which < 58){
			 var keyVal = String.fromCharCode(e.which);  
			 val = val + "" +keyVal;
			 return reg.test(val);
		}
		
    } 
}
//控制只能输入小数点后一位
Fai.checkOneDecimal = function(e, id){
	var val = $("#"+id).val();
	var reg = /^[0-9]\d*(?:\.\d{1}|\d*)$/;
	 if ($.browser.msie) {
		if( event.keyCode > 47 && event.keyCode < 58){
			var keyVal = String.fromCharCode(e.which);  
			val = val + "" +keyVal;
			return reg.test(val);
		}
		
    } else {
		if( e.which > 47 && e.which < 58){
			 var keyVal = String.fromCharCode(e.which);  
			 val = val + "" +keyVal;
			 return reg.test(val);
		}
		
    } 
}

/*按下数字键则返回true,(iSminus:是否允许输入‘-’)
使用onafterpaste事件可防止黏贴强制输入
用法：<input type="text" onkeyup="javascript:Fai.isNumberUJs(this);" onafterpaste="javascript:Fai.isNumberUJs(this)"/>
*/
Fai.isNumberKey2 = function (obj, iSminus, e){
	/*if ($.browser.msie) {
		if(event.keyCode == 37 || event.keyCode == 39) return;
	} else {
		if(e.which == 37 || e.which == 39) return;
	}*/
	if(iSminus){
		$(obj).val($(obj).val().replace(/[^0-9\-]/g,''));
	}else{
		$(obj).val($(obj).val().replace(/[^0-9]/g,''));
	}
};
Fai.isFloatKey = function (e){//按下数字键和小数点则返回true,用法：<input type="text" onkeypress="javascript:return Fai.isFloatKey(event);"/>
	 if ($.browser.msie) {   
        if ( ((event.keyCode > 47) && (event.keyCode < 58)) ||   
              (event.keyCode == 8) || (event.keyCode == 46) ) {   
            return true;   
        } else {   
            return false;   
        }   
    } else {   
        if ( ((e.which > 47) && (e.which < 58)) ||   
              (e.which == 8) || (e.which == 46) ) {   
            return true;   
        } else {   
            return false;   
        }   
    }   
};
/*
用法：
var fls=Fai.flashChecker(); 
if(fls.f) document.write("您安装了flash,当前flash版本为: "+fls.v+".x"); 
else document.write("您没有安装flash"); 
*/
Fai.flashChecker = function() {
	var hasFlash=0;//是否安装了flash
	var flashVersion=0;//flash版本
	var isIE=/*@cc_on!@*/0;//是否IE浏览器
	  
	if(isIE){
		try{
			var swf = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
			if( swf ){
				hasFlash=1;
				VSwf=swf.GetVariable("$version");
				flashVersion=parseInt(VSwf.split(" ")[1].split(",")[0]);
			}
		}catch(ex){}
	}else{
		if( navigator.plugins && navigator.plugins.length > 0){
			var swf=navigator.plugins["Shockwave Flash"];
			if( swf ){
				hasFlash=1;
				var words = swf.description.split(" ");
				for (var i = 0; i < words.length; ++i) {
					if (isNaN(parseInt(words[i]))) continue;
					flashVersion = parseInt(words[i]);
				}
			}
		}
	}
	return {f:hasFlash,v:flashVersion};
};

/**
 * Leyewen check is IE
 * @return boolean :true/false
 *     $.isIE()
 */
Fai.isIE = function(){
	return $.browser.msie ? true : false;
};

Fai.isIE6 = function () {
    if ($.browser.msie) {
        if ($.browser.version == "6.0") return true;
    }
    return false;
};

Fai.isIE7 = function () {
    if ($.browser.msie) {
        if ($.browser.version == "7.0") return true;
    }
    return false;
};
Fai.isIE8 = function(){
	 if ($.browser.msie) {
        if ($.browser.version == "8.0") return true;
    }
    return false;
};
Fai.isIE9 = function () {
    if ($.browser.msie) {
        if ($.browser.version == "9.0") return true;
    }
    return false;
};
Fai.isIE10 = function () {
    if ($.browser.msie) {
        if ($.browser.version == "10.0") return true;
    }
    return false;
};
Fai.isIE11 = function () {
    if ($.browser.msie) {
		// browser.rv: IE11 has a new token so we will assign it msie to avoid breaking changes
        if ($.browser.version == "11.0" || $.browser.rv) return true;
    }
    return false;
};


/**
 * Leyewen check is Safari
 * @return boolean :true/false
 *     $.isSafari()
 */
Fai.isSafari = function(){
	//alert($.toJSON($.browser));
	return $.browser.safari ? true : false;
};

/**
 * Leyewen check is Webkit
 * @return boolean :true/false
 *     $.isWebkit()
 */
Fai.isWebkit = function(){
	return $.browser.webkit ? true : false;
};

/**
 * Leyewen check is Chrome
 * @return boolean :true/false
 *     $.isChrome()
 */
Fai.isChrome = function(){
	return $.browser.chrome ? true : false;
};

/**
 * Leyewen check is Mozilla
 * @return boolean :true/false
 *     $.isMozilla()
 */
Fai.isMozilla = function(){
	return $.browser.mozilla ? true : false;
};

Fai.isAppleWebKit = function(){
	var ua = window.navigator.userAgent;
	if (ua.indexOf("AppleWebKit") >= 0) {
		return true;
	}
	return false;
};

/**
 * Leyewen check is Opera
 * @return boolean :true/false
 *     $.isOpera()
 */
Fai.isOpera = function(){
	return $.browser.opera || $.browser.opr ? true : false;
};

Fai.isAndroid = function() {
	return $.browser.android ? true : false;
};

Fai.isIpad = function() {
	return $.browser.ipad ? true : false;
};

Fai.isIphone = function() {
	return $.browser.iphone ? true : false;
};


/**
 * 屏幕类型定义
 * type的定义在JAVA的fai.web.Request中定义，要求一模一样
 */
Fai.BrowserType = {
	"UNKNOWN" : 0,
	"SPIDER" : 1,
	"CHROME" : 2,
	"FIREFOX" : 3,			// Mozilla
	"MSIE8" : 4,
	"MSIE7" : 5,
	"MSIE6" : 6,
	"MSIE9" : 7,
	"SAFARI" : 8,
	"MSIE10" : 9,
	"MSIE11" : 10,
	"OPERA" : 11,
	"APPLE_WEBKIT" : 12
};

Fai.getBrowserType = function(){
	if (Fai.isIE6()) {
		return Fai.BrowserType.MSIE6;
	} else if (Fai.isIE7()) {
		return Fai.BrowserType.MSIE7;
	} else if (Fai.isIE8()) {
		return Fai.BrowserType.MSIE8;
	} else if (Fai.isIE9()) {
		return Fai.BrowserType.MSIE9;
	} else if (Fai.isIE10()) {
		return Fai.BrowserType.MSIE10;
	} else if (Fai.isIE11()) {
		return Fai.BrowserType.MSIE11;
	} else if (Fai.isMozilla()) {
		return Fai.BrowserType.FIREFOX;
	} else if (Fai.isOpera()) {
		return Fai.BrowserType.OPERA;
	} else if (Fai.isChrome()) {
		return Fai.BrowserType.CHROME;
	} else if (Fai.isSafari()) {
		return Fai.BrowserType.SAFARI;
	} else if (Fai.isAppleWebKit()) {
		return Fai.BrowserType.APPLE_WEBKIT;
	} else {
		return Fai.BrowserType.UNKNOWN;
	}
};


/**
 * 返回屏幕宽高
 */
Fai.Screen = function(){
	return {
		"width" : window.screen.width,
		"height" : window.screen.height
	};
};

/**
 * 屏幕类型定义
 * type的定义在JAVA的fai.web.Request中定义，要求一模一样
 */
Fai.ScreenType = {
	"OTHER" : 0,				// 其他分辨率
	"W1920H1080" : 1,			// 1920*1080
	"W1680H1050" : 2,			// 1680*1050
	"W1600H1200" : 3,			// 1600*1200
	"W1600H1024" : 4,			// 1600*1024
	"W1600H900" : 5,			// 1600*900
	"W1440H900" : 6,			// 1440*900
	"W1366H768" : 7,			// 1366*768
	"W1360H768" : 8,			// 1360*768
	"W1280H1024" : 9,			// 1280*1024
	"W1280H960" : 10,			// 1280*960
	"W1280H800" : 11,			// 1280*800
	"W1280H768" : 12,			// 1280*768
	"W1280H720" : 13,			// 1280*720
	"W1280H600" : 14,			// 1280*600
	"W1152H864" : 15,			// 1152*864
	"W1024H768" : 16,			// 1024*768
	"W800H600" : 17				// 800*600
};

Fai.getScreenType = function(width, height){
	if (width == 1920 && height == 1080) {
		return Fai.ScreenType.W1920H1080;		// 1920*1080
	} else if (width == 1680 && height == 1050) {
		return Fai.ScreenType.W1680H1050;		// 1680*1050
	} else if (width == 1600 && height == 1200) {
		return Fai.ScreenType.W1600H1200;		// 1600*1200
	} else if (width == 1600 && height == 1024) {
		return Fai.ScreenType.W1600H1024;		// 1600*1024
	} else if (width == 1600 && height == 900) {
		return Fai.ScreenType.W1600H900;		// 1600*900
	} else if (width == 1440 && height == 900) {
		return Fai.ScreenType.W1440H900;		// 1440*900
	} else if (width == 1366 && height == 768) {
		return Fai.ScreenType.W1366H768;		// 1366*768
	} else if (width == 1360 && height == 768) {
		return Fai.ScreenType.W1360H768;		// 1360*768
	} else if (width == 1280 && height == 1024) {
		return Fai.ScreenType.W1280H1024;		// 1280*1024
	} else if (width == 1280 && height == 960) {
		return Fai.ScreenType.W1280H960;		// 1280*960
	} else if (width == 1280 && height == 800) {
		return Fai.ScreenType.W1280H800;		// 1280*800
	} else if (width == 1280 && height == 768) {
		return Fai.ScreenType.W1280H768;		// 1280*768
	} else if (width == 1280 && height == 720) {
		return Fai.ScreenType.W1280H720;		// 1280*720
	} else if (width == 1280 && height == 600) {
		return Fai.ScreenType.W1280H600;		// 1280*600
	} else if (width == 1152 && height == 864) {
		return Fai.ScreenType.W1152H864;		// 1152*864
	} else if (width == 1024 && height == 768) {
		return Fai.ScreenType.W1024H768;		// 1024*768
	} else if (width == 800 && height == 600) {
		return Fai.ScreenType.W800H600;			// 800*600
	} else {
		return Fai.ScreenType.OTHER;
	}
};


Fai.getCssInt = function(ctrl, css) {
	if(ctrl.css(css))
	{
		var tmp = parseInt(ctrl.css(css).replace('px', ''));
		if (isNaN(tmp)){
			return 0;
		}
		return tmp;
	}else
	{
		return 0;
	}
};
Fai.getEventX = function(e) {
	e = e || window.event;
	return e.pageX || e.clientX + document.body.scrollLeft;
};
Fai.getEventY = function(e) {
	e = e|| window.event;
	return e.pageY || e.clientY + document.body.scrollTop;
};
Fai.inRect = function(point, rect) {
	if (point.x > rect.left && point.x < (rect.left + rect.width) &&
		point.y > rect.top && point.y < (rect.top + rect.height)) {
		return true;
	}
	return false;
};
Fai.addUrlParams = function (url, param){
	if (Fai.isNull(param)){
		return url;
	}
	if (url.indexOf('?') < 0){
		return url + '?' + param;
	}
	return url + '&' + param;
};
//向数组中添加元素，不重复
Fai.addArrElementsNoRepeat = function (arr, elements){
	if(arr.length > 0){
		var repeat = 0;
		$.each(arr, function(i, n){
			if(arr[i] == elements){
				repeat ++;
			}
		});
		if(repeat == 0){arr[arr.length] = elements;}
	}else{
		arr[arr.length] = elements;
	}
	return arr;
};
Fai.getUrlRoot = function(url) {
	var pos = url.indexOf('://');
	if (pos < 0) {
		return url;
	}
	pos = url.indexOf('/', pos + 3);
	if (pos < 0) {
		return '/';
	}
	return url.substring(pos);
};
Fai.getUrlParam = function (url, name){
	/*
	//老版本  过度优化  小小的url没有提速的必要。且也不是根据传进来的url取parameter的
	if (typeof Fai._urlParams == 'undefined' || !Fai._urlParams){
		Fai._urlParams  = new Object() ;
		var paramStr = document.location.search.substr(1).split('&') ;
		for (var i = 0; i < paramStr.length; ++i){
			var param = paramStr[i].split('=');
			var paramName = decodeURIComponent(param[0]);
			var paramValue = decodeURIComponent(param[1]);
			Fai._urlParams[paramName] = paramValue;
		}
	}
	return Fai._urlParams[name];
	*/
	var paramStrings = url.substring(url.indexOf("?")+1,url.length).split("&");
	var value;
	$.each( paramStrings, function( index, str ){
		var tmpKey = decodeURIComponent( str.substring( 0, str.indexOf("=") ) );
		if( tmpKey === name ){
			value = decodeURIComponent( str.substring( str.indexOf("=")+1 , str.length ) );
			return false;
		}
	});
	return value;
};
// 进行html编码
// .replace(/ /g, "&nbsp;").replace(/\b&nbsp;+/g, " ")用于替换空格 再查找单个或者连续的空格，把单个或连续串中第一个替换为原来的“ ”。
Fai.encodeHtml = function(html){
	return html && html.replace ? (html.replace(/&/g, "&amp;").replace(/ /g, "&nbsp;").replace(/\b&nbsp;+/g, " ").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\\/g, "&#92;").replace(/\'/g, "&#39;").replace(/\"/g, "&quot;").replace(/\n/g, "<br/>").replace(/\r/g, "")) : html;
};
// 进行html解码
Fai.decodeHtml = function(html){
	return html && html.replace ? (html.replace(/&nbsp;/gi, " ").replace(/&lt;/gi, "<").replace(/&gt;/g, ">")
		.replace(/&#92;/gi, "\\").replace(/&#39;/gi, "\'").replace(/&quot;/gi, "\"").replace(/\<br\/\>/gi, "\n").replace(/&amp;/gi, "&")) : html;
};
// 把字符串转换为写在html标签中javascript的字符串，例如<div onclick="alert('xxx')">
Fai.encodeHtmlJs = function(html){
	return html && html.replace ? (html.replace(/\\/g, "\\\\").replace(/\'/g, "\\x27").replace(/\"/g, "\\x22").replace(/\n/g, "\\n").replace(/</g,"\\x3c").replace(/>/g,"\\x3e")) : html;
};
// 把字符串转换为写在html中属性值，例如<div title="xxx">
Fai.encodeHtmlAttr = function(html){
	return html && html.replace ? (html.replace(/\"/g, "&#x22;").replace(/\'/g, "&#x27;").replace(/</g, "&#x3c;").replace(/>/g, "&#x3e;").replace(/&/g, "&#x26;")).replace(/\\/g, "&#5c;") : html;
};
// 进行url编码
Fai.encodeUrl = function (url){
	/*
	return url && url.replace ?
		url.replace(/%/ig, "%25").replace(/\+/ig, "%2B").replace(/&/ig, "%26").replace(/#/ig, "%23") 
		: url;
    str = str.replace(/,/g, '%2C');
    str = str.replace(/\//g, '%2F');
    str = str.replace(/\?/g, '%3F');
    str = str.replace(/:/g, '%3A');
    str = str.replace(/@/g, '%40');
    str = str.replace(/&/g, '%26');
    str = str.replace(/=/g, '%3D');
    str = str.replace(/\+/g, '%2B');
    str = str.replace(/\$/g, '%24');
    str = str.replace(/#/g, '%23');
    return str;
	*/
	return typeof url === "undefined" ? "" : encodeURIComponent(url);
};
// 进行url解码
Fai.decodeUrl = function (url){
	var backUrl = "";
	try{
		// 如果decodeURIComponent失败, 会爆Uncaught URIError
		backUrl = (typeof url === "undefined" ? "" : decodeURIComponent(url));
	}catch(e){
		backUrl = "";
	}
	return backUrl;
};

/*\uxxxx格式字符串和中文的相互转换
使用方式：Fai.toUN.on("中文")    和   Fai.toUN.un("\\u0022\\u8BF7\\u8F93\\u0022");
*/
Fai.toUN = {
    on: function(str) {
        var a = [],
        i = 0;
        for (; i < str.length;) a[i] = ("00" + str.charCodeAt(i++).toString(16)).slice( - 4);
        return "\\u" + a.join("\\u")
    },
    un: function(str) {
        return unescape(str.replace(/\\/g, "%"))
    }
};

// 提取文件名，suffix表示是否返回后缀名
Fai.parseFileName = function (path, suffix){
	
	var pos = path.lastIndexOf("/");
	if (pos < 0){
		pos = path.lastIndexOf("\\");
	}
	if (pos >= 0){
		path =path.substring(pos + 1);
	}
	if (suffix){
		return path;
	}else{
		var doPos = path.lastIndexOf(".");
		if(doPos >= 0 ){
			return path.substring(0, doPos);
		}else{
			return path;
		}
	}
};

// 格式化字符串
Fai.format = function (){
	var s = arguments[0];
	for (var i = 0; i < arguments.length - 1; i++) {       
		var reg = new RegExp("\\{" + i + "\\}", "gm");             
		s = s.replace(reg, arguments[i + 1]);
	}
	return s;
};

Fai.checkValid = function(value, label, errCtrlId, minlen, maxlen){
	var msg;
	if (!value && minlen > 0){
		msg = Fai.format("您还未输入{0}", label);
	}
	else if (value.length < minlen){
		msg = Fai.format("{0}不能少于{1}个字", label, minlen);
	}
	else if (value.length > maxlen){
		msg = Fai.format("{0}不能多于{1}个字，请裁减后重试。", label, maxlen);
	}
	Fai.showErr(errCtrlId, msg);
	return !msg;
};

Fai.showErr = function(errCtrlId, msg){
	var errCtrl = $("#" + errCtrlId);
	if (msg){
		errCtrl.show();
		errCtrl.text(msg);
	}else{
		errCtrl.hide();
	}
};

Fai.showMsg = function(msg){
	var msgCtrl = $("#msg");
	if (msg && msg.length > 0){
		msgCtrl.show();
		msgCtrl.text(msg);
	}else{
		msgCtrl.hide();
		msgCtrl.text("");
	}
};

Fai.checkVal = function(lableName, value, minlen, maxlen){
	var msg;
	if(value.length < minlen){
		alert(Fai.format("{0}不能少于{1}个字符", lableName, minlen)); 
		msg = false; 
	}
	else if(value.length > maxlen){
		alert(Fai.format("{0}不能多于{1}个字符", lableName, maxlen)); 
		msg = false;
	}
	else{
		msg = true;
	}
	return msg;
};
/*
删除指定对象：
使用方式：Fai.oDel(obj)
*/
Fai.getEl = function(obj){return typeof(obj)=="string"?document.getElementById(obj):obj};
Fai.getBrowserWidth = function (){return document.documentElement.clientWidth;};
Fai.getBrowserHeight = function (){return document.documentElement.clientHeight;};
Fai.delNode = function(obj){if(Fai.getEl(obj)!=null){Fai.getEl(obj).parentNode.removeChild(Fai.getEl(obj))}};

/*删除指定对象的所有子节点*/
Fai.delChildNodes = function (element){
	if (element == null || element.childNodes == null){
		return;
	}
	var length = element.childNodes.length;   
	for ( var i = 0; i < length; ++i) {   
		element.removeChild(element.firstChild);
	}  
};

/*
需要配套Fai.closeFlowMsg()使用
str: 需要显示的话
*/
Fai.showFlowMsg = function (str) {
	//删除div
	Fai.delNode('showFlowMsg');
	//创建临时div
	var newObj = document.createElement("div");
	newObj.id = "showFlowMsg";
	newObj.style.position = "absolute";
	newObj.style.top = "80px";
	newObj.style.left = "400px";
	newObj.style.border = "1px";
	newObj.style.borderTop = "0px";
	newObj.style.borderBottomStyle = "solid";
	newObj.style.borderColor = "#E9F0F4";
	newObj.style.height = "22px";
	newObj.style.lineHeight = "22px";
	newObj.style.width = "auto";
	newObj.style.paddingTop = "1px";
	newObj.style.paddingBottom = "1px";
	newObj.style.paddingLeft = "10px";
	newObj.style.paddingRight = "10px";
	newObj.style.backgroundColor = "#FFFFC4";
	newObj.style.zIndex = "999";
	newObj.style.textAlign = "left";
	newObj.style.fontSize = "12";
	newObj.innerHTML = str;
	
	
	document.body.appendChild(newObj);
	
	Fai.delay(150);

};

/*
关闭
*/
Fai.closeFlowMsg = function(){
	Fai.delNode('showFlowMsg');
};

/*
时间延迟函数，Millis：毫秒
*/
Fai.delay = function (numberMillis){ 
        var now = new Date(); 
        var exitTime = now.getTime()+numberMillis;    
        while(true){ 
                 now = new Date(); 
                if(now.getTime() > exitTime) 
                        return; 
         } 
};


Fai.Msg = {
	CONFIRM: 0,
	SUCCEED: 1,
	FAIL: 2,
	TIP: 3
};

Fai.Msg.box = function (type, msg){
	//删除div
	Fai.delNode('msgBox');
	alert("ok");
};

Fai.Debug = {};

Fai.Debug.alert = function (obj) {
	if (typeof(obj) != "object"){
		alert(obj);
		return;
	}
	var props = "";
	for(var p in obj){
		if (typeof(obj[p]) == "function"){
			props += p + "=function\t";
		}else{
			props += p + "=" + obj[p] + "\t";
		}
	}
	alert(props);
};

Fai.Debug.msg = function(msg) {
	if (Fai.isNull(Fai.Debug.cnt)) {
		Fai.Debug.cnt = 0;
	}
	++Fai.Debug.cnt;
	var html = "";
	html = "<div id='dbgMsg' style=\"position:absolute;  top:30px; left: 45%; margin:0px auto; width:auto;  height:auto; z-index:9999;\"></div>";
	if(Fai.top.$("#dbgMsg").length ==0) {
		Fai.top.$(html).appendTo("body");
	}
	var tips = "";
	tips += "<div class=\"tips\">"
			+ 	"<div class=\"msg\">" + Fai.Debug.cnt + ":" + msg +"</div>"
			+"</div>";
	Fai.top.$("#dbgMsg").find(".tips").remove();
	Fai.top.$(tips).appendTo("#dbgMsg");
};

Fai.Debug.track = function(msg) {
	if (Fai.isNull(Fai.Debug.outtrack) || !Fai.Debug.outtrack) {
		return;
	}

	var now = (new Date()).getTime();
	if (Fai.isNull(Fai.Debug.last)) {
		Fai.Debug.last = now;
	}
	var diff = (now - Fai.Debug.last);
	Fai.Debug.last = now;

	if (msg == '' || diff <= 0) {
		return;
	}
	var html = "";
	html = "<div id='dbgMsg' style=\"position:absolute;  top:30px; left: 45%; margin:0px auto; width:auto;  height:auto; z-index:9999;\"></div>";
	var dbgMsg = Fai.top.$("#dbgMsg");
	if(dbgMsg.length ==0) {
		dbgMsg = Fai.top.$(html);
		dbgMsg.appendTo("body");
	}
	Fai.top.$("<div class='tips'><div class='msg' style='clear:both;'>" + msg + " : " + diff + "</div></div>").appendTo(dbgMsg);
};


/**
 * Fai.Cookie在新增的方法内容中暂停使用，使用$.cookie()代替
 * Fai.Cookie将在未来版本中删除
 */
Fai.Cookie = {};

Fai.Cookie.set = function(name, value){
     var argv = arguments;
     var argc = arguments.length;
     var expires = (argc > 2) ? argv[2] : null;
     var path = (argc > 3) ? argv[3] : '/';
     var domain = (argc > 4) ? argv[4] : null;
     var secure = (argc > 5) ? argv[5] : false;
     document.cookie = name + "=" + escape (value) +
       ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
       ((path == null) ? "" : ("; path=" + path)) +
       ((domain == null) ? "" : ("; domain=" + domain)) +
       ((secure == true) ? "; secure" : "");
};

Fai.Cookie.get = function(name){
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    var j = 0;
    while(i < clen){
        j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return Fai.Cookie.getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if(i == 0)
            break;
    }
    return null;
};

Fai.Cookie.clear = function(name) {
  if(Fai.Cookie.get(name)){
    var expdate = new Date(); 
    expdate.setTime(expdate.getTime() - (86400 * 1000 * 1)); 
    Fai.Cookie.set(name, "", expdate); 
  }
};

Fai.Cookie.clearCloseClient = function(name) {
  if(Fai.Cookie.get(name)){
    Fai.Cookie.set(name, "", null); 
  }
};

Fai.Cookie.getCookieVal = function(offset){
   var endstr = document.cookie.indexOf(";", offset);
   if(endstr == -1){
       endstr = document.cookie.length;
   }
   return unescape(document.cookie.substring(offset, endstr));
};

Fai.Conn = {};

// 跨域访问，返回一段js代码。（在ie6下，如果是用<a/>还调用，必须是href="javascript:xxx()"的方式，而不能onclick，否则返回的js没有作用）
Fai.Conn.requestJs = function(id, url, options){ 
    oScript = document.getElementById(id);
	var target = null;
	if (typeof options == 'object' && options.target) {
		target = document.getElementsByName(options.target)[0]; 
	} else {
		target = document.getElementsByTagName("head")[0]; 
	}
    if (oScript) { 
        target.removeChild(oScript); 
    } 
	if (typeof options == 'object' && options.callback){
		url = Fai.addUrlParams(url, "_callback=" + options.callback);
	}
	if (typeof options == 'object' && options.refresh){
		url = Fai.addUrlParams(url, "_random=" + Math.random());
	}
    oScript = document.createElement("script"); 
    oScript.setAttribute("src", url); 
    oScript.setAttribute("id", id); 
    oScript.setAttribute("type","text/javascript"); 
    oScript.setAttribute("language","javascript"); 
    target.appendChild(oScript); 
    return oScript; 
};

Fai.IFrame = {};

// 让iframe的高度跟随里面内容的高度而变化。
// 使用方法：<iframe onload="javascript:Fai.IFrame.autoHeight('id')">
// 设置defaultHeight可以避免初始化时iframe出现从小变大的。
// 在ie8下，如果是动态写iframe的html内容，iframe中的body是不会跟着里面的div变高，此时就要传main div id进来。
Fai.IFrame.autoHeight = function(id, mainDiv){
	var iframe = $("#"+id);
	var iframeDoc = iframe[0].contentWindow.document;
	if (iframeDoc){
		// firefox下要给body加上一个div
		var div = iframeDoc.createElement("div");
		iframeDoc.body.appendChild(div);
		div.style.clear = "both";
		div.style.margin = "0px";
		div.style.padding = "0px";
		div.style.fontSize = "1px";
	}
	if (mainDiv){
		Fai.IFrame.doAutoHeight(id, mainDiv);
	} else {
		Fai.IFrame.doAutoHeight(id);
	}
	if (mainDiv){
		setInterval("Fai.IFrame.doAutoHeight('" + id + "','" + mainDiv + "')", 1000);
	}else{
		setInterval("Fai.IFrame.doAutoHeight('" + id + "')", 1000);
	}
};

Fai.IFrame.doAutoHeight = function(id, mainDiv){
	try {
		var iframe = $("#"+id);
		if (iframe.length < 0){
			return;
		}
		var height = 0;
		if (mainDiv) {
			height = iframe[0].contentWindow.$("#" + mainDiv).height();
		} else {
			if (Fai.isIE6()) {
				height = iframe[0].contentWindow.document.body.scrollHeight;
			} else {
				// chrome等浏览器对高度的定义不一样，所以这里使用jquery.height方法
				height = iframe[0].contentWindow.$("body").height();
			}
		}
		if (height != iframe.height()) {
			iframe.height(height);
		}
	}catch(exp){
	}
};

Fai.ptInRect = function (pt, rect){
//	alert(pt.x + " " + pt.y + "\n" + rect.left + " " + rect.top + " " + (rect.left + rect.width) + " " + (rect.top + rect.height));
	if (pt.x >= rect.left && pt.x <= rect.left + rect.width &&
		pt.y >= rect.top && pt.y <= rect.top + rect.height){
		return true;
	}
	return false;
};


Fai.Img = {};

Fai.Img = {
	MODE_SCALE_FILL: 1,				// 根据区域能够填满的最大值等比例缩放。图片100x50，区域50x50，结果50x25。
	MODE_SCALE_WIDTH: 2,			// 根据区域宽度等比例缩放，结果高度将不受区域高度限制，即可能撑大高度。图片100x50，区域50x10，结果50x25。
	MODE_SCALE_HEIGHT: 3,			// 根据区域高度等比例缩放，结果宽度将不受区域宽度限制，即可能撑大宽度。图片100x50，区域50x50，结果100x50。
	MODE_SCALE_DEFLATE_WIDTH: 4,	// 根据区域宽度等比例缩小，不放大，结果高度将不受区域高度限制。图片100x50，区域50x10，结果50x25；图片100x50，区域200x100，结果100x50。
	MODE_SCALE_DEFLATE_HEIGHT: 5,	// 根据区域高度等比例缩小，不放大，结果宽度将不受区域高度限制。图片100x50，区域50x50，结果100x50；图片100x50，区域200x100，结果100x50。
	MODE_SCALE_DEFLATE_FILL: 6,		// 根据区域能够填满的最大值等比例缩小，不放大。图片100x50，区域50x50，结果50x25。
	MODE_SCALE_DEFLATE_MAX: 7 		// 根据区域等比例缩小，不放大，结果的宽度和高度不能同时超过区域限制。图片200x100，区域100x100，结果200x100；图片100x200，区域100x100，结果100x200。
};

// 使用此函数时，不要在img标签中先设置大小，会使得调整img大小时失败；先隐藏图片，避免出现图片从原始图片变为目标图片的过程
// 	<img src="xx.jpg" style="display:none;" onload="Fai.Img.optimize(this, {width:100, height:50, mode:Fai.Img.MODE_SCALE_FILL});"/>
Fai.Img.optimize = function(img, option){
	// ie下对于display:none的img不会加载
	// 这里要用临时图片，是因为当动态改变图片src时，由于图片的大小已经被设置，因此再次获取会失败
	var imgTmp = new Image();   
	// 这里还不能先置空，否则将会引起对''文件的一次访问
	//	imgTmp.src = '';
	imgTmp.src = img.src;
	var imgWidth = imgTmp.width;
	var imgHeight = imgTmp.height;
	if (Fai.isNull(imgWidth) || imgWidth == 0 || Fai.isNull(imgHeight) || imgHeight == 0) {
		// chrome似乎对临时图片的加载会有延迟，立即取大小会失败
		imgWidth = img.width;
		imgHeight = img.height;
	}
//	alert(imgTmp.width + ":" + imgTmp.height + "\n" + img.width + ":" + img.height);
	var size = Fai.Img.calcSize(imgWidth, imgHeight, option.width, option.height, option.mode);
	img.width = size.width;
	img.height = size.height;
	if(option.display == 1)
	{
		img.style.display = 'inline';
	}else if(option.display == 2)
	{
		img.style.display = 'none';
	}else if(option.display == 3){
		img.style.display = 'inline-block';
	}else{
		img.style.display = 'block';
	}
	return {width:img.width, height:img.height};
};

Fai.Img.calcSize = function(width, height, maxWidth, maxHeight, mode){
    var size = {width:width, height:height};      
    if (mode == Fai.Img.MODE_SCALE_FILL){
		var rateWidth = width / maxWidth;      
		var rateHeight = height / maxHeight;      
			  
		if (rateWidth > rateHeight){      
			size.width =  maxWidth;      
			size.height = height / rateWidth;      
		}else{      
			size.width = width / rateHeight;      
			size.height = maxHeight;      
		}      
    }
	else if (mode == Fai.Img.MODE_SCALE_WIDTH){
		var rateWidth = width / maxWidth;      
		size.width =  maxWidth;      
		size.height = height / rateWidth;      
	}
	else if (mode == Fai.Img.MODE_SCALE_HEIGHT){
		var rateHeight = height / maxHeight;      
		size.width = width / rateHeight;      
		size.height = maxHeight;      
	}
	else if (mode == Fai.Img.MODE_SCALE_DEFLATE_WIDTH){
		var rateWidth = width / maxWidth; 
		if (rateWidth > 1){
			size.width =  maxWidth;      
			size.height = height / rateWidth;      
		}
	}
	else if (mode == Fai.Img.MODE_SCALE_DEFLATE_HEIGHT){
		var rateHeight = height / maxHeight;      
		if (rateHeight > 1){
			size.width = width / rateHeight;      
			size.height = maxHeight;      
		}
	}
    else if (mode == Fai.Img.MODE_SCALE_DEFLATE_FILL){
		var rateWidth = width / maxWidth;
		var rateHeight = height / maxHeight;
		
		if (rateWidth > rateHeight){   
			if (rateWidth > 1) {
				size.width =  maxWidth;      
				size.height = height / rateWidth;
			}
		}else{
			if (rateHeight > 1)	{
				size.width = width / rateHeight;      
				size.height = maxHeight;      
			}
		}
	} else if (mode == Fai.Img.MODE_SCALE_DEFLATE_MAX) {
		if (width > maxWidth && height > maxHeight) {
			var rateWidth = width / maxWidth;
			var rateHeight = height / maxHeight;

			if (rateWidth < rateHeight) {
				size.width = maxWidth;
				size.height = height / rateWidth;
			} else {
				size.width = width / rateHeight;
				size.height = maxHeight;
			}
		}
    }
	size.width = Math.floor(size.width);
	size.height = Math.floor(size.height);
	if (size.width == 0) {
		size.width = 1;
	}
	if (size.height == 0) {
		size.height = 1;
	}
    return size;
};

/*
灰色透明背景，正在运行的图标
提交的时候使用Fai.ing(str, autoClose):str为要显示的语句，如果为空则显示"正在处理"
如果autoClose == true，则在2秒钟后自动close便签，否则不自动关闭。
得到结果后使用Fai.removeIng()
提供autoTime参数，用于自定义提示自动关闭的等待时间————Front 2015-03-17
*/
Fai.ing = function(str, autoClose, autoTime) {
	var msg = (str==null||str=="")?"正在处理...":str;
	var windowWidth = Fai.top.document.body.clientWidth;
	var windowHeight = Fai.top.document.body.clientHeight;
	var html = "";
	var ingStyle = "position:absolute; top:50px; left: 50%; margin:0px auto; width:auto;  height:auto; z-index:9999;";
	var animateStyle = "transition: opacity ease .6s; -moz-transition: opacity ease .6s; -webkit-transition: opacity ease .6s; -o-transition: opacity ease .6s; opacity: 0; -webkit-opacity: 0; -moz-opacity: 0; -khtml-opacity: 0; filter:alpha(opacity=0);";
	html = "<div id='ing' style='" + ingStyle + animateStyle + "'></div>";
	if(Fai.top.$("#ing").length ==0) {
		Fai.top.$(html).appendTo("body");
	}
	var ing = Fai.top.$("#ing");
	var bodyTop = Fai.top.$(document).scrollTop();
	if (Fai.isIE() && bodyTop == 0) {
		bodyTop = Fai.top.$("html").scrollTop();
	}
	if (bodyTop > 0) {
		ing.css('top', ((bodyTop) + 50) + 'px');
	}
	
	var id = parseInt(Math.random()*10000);
	var tips = "";
	tips += "<div id=\""+ id +"\" class=\"tips\">"
			+ 	"<div class=\"msg\">"+ msg +"</div><div class='close' onclick=\"Fai.top.Fai.removeIng(false, "+ id +");\"></div>"
			+"</div>";
	ing.find(".tips").remove();
	Fai.top.$(tips).appendTo(ing);
	
	// 居中处理
	var ingWidth = Fai.top.$(ing).width();
	Fai.top.$(ing).css("left", (Fai.top.document.documentElement.clientWidth - ingWidth) / 2 );

	if(autoClose){
		Fai.top.Fai.removeIng(autoClose, id, autoTime);
	}

	/* 渐变出现 */
	var isIe = Fai.isIE6() || Fai.isIE7() || Fai.isIE8();
	if (isIe) {
		Fai.top.$("#ing").animate({ opacity: 1, filter:"alpha(opacity=100)"}, 300);
	} else {
		Fai.top.$("#ing").css({ opacity: 1 });
	}
	
	Fai.top.$("#ing").find(".close").bind("mouseenter", function () {
		$(this).addClass("close_hover");
	}).bind("mouseleave", function () {
		$(this).removeClass("close_hover");
	});
};



/*购物车用的*/
Fai.ing2 = function(str, autoClose, autoTime) {
	var msg = (str==null||str=="")?"正在处理...":str;
	var windowWidth = Fai.top.document.body.clientWidth;
	var windowHeight = Fai.top.document.body.clientHeight;
	var html = "";
	var ingStyle = " margin:180px auto; width:500px;  height:auto; z-index:9999;";
	var animateStyle = "transition: opacity ease .6s; -moz-transition: opacity ease .6s; -webkit-transition: opacity ease .6s; -o-transition: opacity ease .6s; opacity: 0; -webkit-opacity: 0; -moz-opacity: 0; -khtml-opacity: 0; filter:alpha(opacity=0);";
	html = "<div id='ing2' style='" + ingStyle + animateStyle + "'></div>";
	if(Fai.top.$("#ing2").length ==0) {
		Fai.top.$(html).appendTo("body");
	}
	var ing = Fai.top.$("#ing2");
	var bodyTop = Fai.top.$('body').scrollTop();
	if (Fai.isIE() && bodyTop == 0) {
		bodyTop = Fai.top.$("html").scrollTop();
	}

	if (bodyTop > 0) {
		ing.css('top', (bodyTop) + 50 + 'px');
	}else{
		ing.css('top', (bodyTop) + 200 + 'px');
	}
	
	var id = parseInt(Math.random()*10000);
	var tips = "";
	tips += "<div id=\""+ id +"\" class=\"tips2\">"
			+ 	"<div class=\"msg2\">"+ msg +"</div><div class='close' onclick=\"Fai.top.Fai.removeIng2(false, "+ id +");Fai.top.Fai.removeBg();\"></div>"
			+"</div>";
	ing.find(".tips2").remove();
	Fai.top.$(tips).appendTo(ing);
	
	// 居中处理
	var ingWidth = Fai.top.$(ing).width();
	Fai.top.$(ing).css("left", (Fai.top.document.documentElement.clientWidth - ingWidth) / 2 );

	if(autoClose){
		Fai.top.Fai.removeIng(autoClose, id, autoTime);
	}

	/* 渐变出现 */
	var isIe = Fai.isIE6() || Fai.isIE7() || Fai.isIE8();
	if (isIe) {
		Fai.top.$("#ing2").animate({ opacity: 1, filter:"alpha(opacity=100)"}, 300);
	} else {
		Fai.top.$("#ing2").css({ opacity: 1 });
	}
	
	Fai.top.$("#ing2").find(".close").bind("mouseenter", function () {
		$(this).addClass("close_hover");
	}).bind("mouseleave", function () {
		$(this).removeClass("close_hover");
	});
};

Fai.removeAllIng = function() {
	Fai.top.$("#ing").remove();
};

Fai.removeIng = function(autoClose, id, autoTime){
	if(autoClose){
		if(typeof id != 'undefined' && Fai.top.$("#" + id).length > 0){
			Fai.top.window.setTimeout(function(){$("#" + id).fadeOut(1000); }, (autoTime ? autoTime : 3000));
			Fai.top.window.setTimeout(function(){$("#" + id).remove(); }, (autoTime ? autoTime + 1500 : 4500));
		}else{
			Fai.top.$(".tips").fadeOut(1000);
			Fai.top.window.setTimeout(function(){$("#ing").remove(); }, (autoTime ? autoTime : 3000));
		}
	}else{
		if(typeof id != 'undefined' && Fai.top.$("#" + id).length > 0){
			Fai.top.$("#" + id).fadeOut(500);
			Fai.top.window.setTimeout(function(){$("#" + id).remove(); }, 1000);
		}
		else{
			Fai.top.$(".tips").fadeOut(500);
			Fai.top.window.setTimeout(function(){$("#ing").remove(); }, 1000);
		}
	}
	Fai.top.$("#ing").css("opacity", 0);
};

Fai.removeIng2 = function(autoClose, id, autoTime){
	if(autoClose){
		if(typeof id != 'undefined' && Fai.top.$("#" + id).length > 0){
			Fai.top.window.setTimeout(function(){$("#" + id).fadeOut(1000); }, (autoTime ? autoTime : 3000));
			Fai.top.window.setTimeout(function(){$("#" + id).remove(); }, (autoTime ? autoTime + 1500 : 4500));
		}else{
			Fai.top.$(".tips").fadeOut(1000);
			Fai.top.window.setTimeout(function(){$("#ing2").remove(); }, (autoTime ? autoTime : 3000));
		}
	}else{
		if(typeof id != 'undefined' && Fai.top.$("#" + id).length > 0){
			Fai.top.$("#" + id).fadeOut(500);
			Fai.top.window.setTimeout(function(){$("#" + id).remove(); }, 1000);
		}
		else{
			Fai.top.$(".tips").fadeOut(500);
			Fai.top.window.setTimeout(function(){$("#ing2").remove(); }, 1000);
		}
	}
	Fai.top.$("#ing2").css("opacity", 0);
};



/*
灰色透明背景
*/
Fai.bg = function(id, opacity) {
	var html = "";
	var opacityHtml = "";
	if(opacity){
		opacityHtml = "filter: alpha(opacity=" + opacity*100 + "); opacity:"+ opacity +";";
	}

	if (Fai.isIE6()) {
		var scrollTop = Fai.top.$("html").scrollTop();
		Fai.top.$("html").data("scrollTop", scrollTop);
		Fai.top.$("html").scrollTop(0);
		var scrollTop = Fai.top.$("body").scrollTop();
		Fai.top.$("body").data("scrollTop", scrollTop);
		Fai.top.$("body").scrollTop(0);
		Fai.top.$("html").data("overflow-x", Fai.top.$("html").css("overflow-x"));
		Fai.top.$("html").data("overflow-y", Fai.top.$("html").css("overflow-y"));
		Fai.top.$("html").css("overflow-x", "hidden");
		Fai.top.$("html").css("overflow-y", "hidden");
		Fai.top.$("body").data("overflow-x", Fai.top.$("body").css("overflow-x"));
		Fai.top.$("body").data("overflow-y", Fai.top.$("body").css("overflow-y"));
		Fai.top.$("body").css("overflow-x", "hidden");
		Fai.top.$("body").css("overflow-y", "hidden");
	}
	// 处理网站置灰的情况
	if (Fai.isIE6() || Fai.isIE7() || Fai.isIE8()) {
		if (Fai.top.$("html").css("filter")) {
			Fai.top.$("html").data("filter", Fai.top.$("html").css("filter"));
			Fai.top.$("html").css("filter", "none");
		}
	}
	
	html = "<div id=\"popupBg"+ id +"\" class=\"popupBg\" style='"+ opacityHtml +"' >"+
		($.browser.msie && $.browser.version == 6.0 ?
			  '<iframe id="fixSelectIframe' + id + '" wmode="transparent" style="filter: alpha(opacity=0);opacity: 0;" class="popupBg" style="z-index:-111" src="javascript:"></iframe>' 	
			  :
			  '')
	+"</div>";
	
	Fai.top.$(html).appendTo("body");
	Fai.stopInterval(null);
};

Fai.removeBg = function(id){
	if(id){
		Fai.top.$("#popupBg" + id).remove();
	}else{
		Fai.top.$(".popupBg").remove();
	}
	if (Fai.isIE6()) {
		Fai.top.$("html").css("overflow-x", Fai.top.$("html").data("overflow-x"));
		Fai.top.$("html").css("overflow-y", Fai.top.$("html").data("overflow-y"));
		Fai.top.$("body").css("overflow-x", Fai.top.$("body").data("overflow-x"));
		Fai.top.$("body").css("overflow-y", Fai.top.$("body").data("overflow-y"));
		Fai.top.$("html").scrollTop(Fai.top.$("html").data("scrollTop"));
		Fai.top.$("body").scrollTop(Fai.top.$("body").data("scrollTop"));
	}
	// 处理网站置灰的情况
	if (Fai.isIE6() || Fai.isIE7() || Fai.isIE8()) {
		if (Fai.top.$("html").data("filter")) {
			Fai.top.$("html").css("filter", Fai.top.$("html").data("filter"));
		}
	}

	Fai.startInterval(null);
};
Fai.removeBg = function(id){
	if(id){
		Fai.top.$("#popupBg" + id).remove();
	}else{
		Fai.top.$(".popupBg").remove();
	}
	if (Fai.isIE6()) {
		Fai.top.$("html").css("overflow-x", Fai.top.$("html").data("overflow-x"));
		Fai.top.$("html").css("overflow-y", Fai.top.$("html").data("overflow-y"));
		Fai.top.$("body").css("overflow-x", Fai.top.$("body").data("overflow-x"));
		Fai.top.$("body").css("overflow-y", Fai.top.$("body").data("overflow-y"));
		Fai.top.$("html").scrollTop(Fai.top.$("html").data("scrollTop"));
		Fai.top.$("body").scrollTop(Fai.top.$("body").data("scrollTop"));
	}
	// 处理网站置灰的情况
	if (Fai.isIE6() || Fai.isIE7() || Fai.isIE8()) {
		if (Fai.top.$("html").data("filter")) {
			Fai.top.$("html").css("filter", Fai.top.$("html").data("filter"));
		}
	}

	Fai.startInterval(null);
};
/**
* @function 灰色透明背景（在当前页面）
* @param {int} id -- 弹出层id
* @param {float} opacity -- 弹出层透明度
* @param {object} options -- 弹出层扩展
**/
Fai.bodyBg = function(id, opacity, options) {
	var html = "",
		opacityHtml = "",
		opt = options || {},
		extClass = opt.extClass || "";
	
	if(opacity){
		opacityHtml = "filter: alpha(opacity=" + opacity*100 + "); opacity:"+ opacity +";";
	}

	if (Fai.isIE6()) {
		$("body").data("height", $("body").css("height"));
		$("body").css("height", "100%");
	}

	html = "<div id=\"popupBg"+ id +"\" class=\"popupBg "+ extClass +"\" style='"+ opacityHtml +"' >"+ 
		($.browser.msie && $.browser.version == 6.0 ?
			  '<iframe id="fixSelectIframe' + id + '" wmode="transparent" style="filter: alpha(opacity=0);opacity: 0;" class="popupBg" style="z-index:-111" src="javascript:"></iframe>' 	
			  :
			  '')
	+"</div>";
	$(html).appendTo("body");
};

Fai.removeBodyBg = function(id){
	if (Fai.isIE6()) {
		$("body").css("height", $("body").data("height"));
	}

	if(id) {
		$("#popupBg" + id).remove();
	}else{
		$(".popupBg").remove();
	}
};


/*
弹出框（在top显示）
title				弹出框的标题
width				内容区宽度（数字/字符串）
height				内容区高度（数字/字符串）
opacity				背景遮盖层的透明度，默认0.3
displayBg			是否加背景遮盖层，默认true
frameSrcUrl			内容区iframe的src地址
frameScrolling		iframe是否有scrolling(yes/no/auto)，默认auto
bannerDisplay		是否显示标题栏和边框，默认true
closeBtnClass		关闭按钮样式
framePadding		是否去掉内容区的padding
bgClose				是否点击背景关闭，默认false
divId				以div的html来作为内容
divContent			以html来作为内容

closeFunc			关闭popup window时执行的函数，可以通过Fai.closePopupWindow(popupID, closeArgs)来传递closeFunc的回调参数，即调用：closeFunc(closeArgs)
msg                 关闭popup window时提示的语句（点击右上角那个×）
helpLink			帮助按钮的link url
waitingPHide		是否隐藏loading面板（前后端分离后，交由加载的文件控制）
*/
Fai.popupWindow = function(options){
	//settings走默认设置
	var settings = {
		title: '',
		width: 500,
		height: 300,
		frameSrcUrl: 'about:_blank',
		frameScrolling: 'auto',
		bannerDisplay: true,
		framePadding: true,
		opacity: '0.3',
		displayBg: true,
		bgClose: false,
		closeBtnClass: '',
		waitingPHide: true
	};
	settings = $.extend(settings, options);

	var contentWidth = parseInt(settings.width),
		contentHeight = parseInt(settings.height);

	var browserWidth = Fai.top.document.documentElement.clientWidth;
	if (!$.browser.msie) {
		browserWidth = Fai.top.document.body.clientWidth;
	}
	var browserHeight = Fai.top.document.documentElement.clientHeight;

	var leftMar = (browserWidth - contentWidth)/2;
	if( settings.leftMar != null ){
		leftMar = parseInt(settings.leftMar);
	}
	var topDiff = 80;
	if( !settings.bannerDisplay ){
		topDiff = 0;
	}
	var topMar = (browserHeight - contentHeight - topDiff)/2;
	if( settings.topMar != null ){
		topMar = parseInt(settings.topMar);
	}

	var bgDisplay = '',
		bannerStyle = '',
		trans = "";
	if( !settings.bannerDisplay ){
		bannerStyle = "display:none;";
		bgDisplay ="background:none;";
		if (!settings.closeBtnClass) {
			settings.closeBtnClass = "formX_old";	// 没有边框时使用另一个样式（如幻灯片）
		}
	}
	if( !settings.framePadding ){
		bgDisplay += "padding:0;";
		trans = "allowtransparency=\"true\"";
	}
	
	var id = parseInt(Math.random()*10000),
		displayModeContent = "<iframe " + trans + " id='popupWindowIframe"+ id +"' class='popupWindowIframe' src='' frameborder='0' scrolling='"+ settings.frameScrolling +"' style='width:100%;height:100%;'></iframe>",
		iframeMode = true;
	if(settings.divId != null)	{
		iframeMode = false;
		displayModeContent = $(settings.divId).html();
	}
	if(settings.divContent != null)	{
		iframeMode = false;
		displayModeContent = settings.divContent;
	}

	//加背景
	if(settings.displayBg) {
		Fai.bg(id, settings.opacity);
	}

	var html = "";
	//加弹出窗口
	var scrollTop = Fai.top.$("body").scrollTop();
	if (scrollTop == 0) {
		scrollTop = Fai.top.$("html").scrollTop();
	}
	
	var winStyle = "left:"+ leftMar +"px; top:"+ (topMar + scrollTop) +"px;";
	// ie6/7 popupWin的width无法自动，所以先写死再动态修正
	if( Fai.isIE6() || Fai.isIE7() ){
		winStyle += 'width:' + contentWidth + 'px;';
	}
	var formMSGStyle = "position:relative;width:" + contentWidth + 'px;height:' + contentHeight + 'px;',
		fixFlashIframe = "";
	if ($.browser.msie) {
		fixFlashIframe = '<iframe id="fixFlashIframe' + id + '" style="position:absolute;z-index:-1;left:0;top:0;" frameborder="0" width="100%" height="100%" src="javascript:"></iframe>';
	}

	//fixFlashIframe防止在IE下Flash遮挡弹出层
	var html = [
			"<div id=\"popupWindow"+ id +"\" class=\"formDialog\" style=\"" + winStyle + "\">",
			fixFlashIframe,
				"<div class=\"formTL\" style='"+ bannerStyle +"'><div class=\"formTR\"><div class=\"formTC\">" + settings.title + "</div></div></div>",
					"<div class=\"formBL\" style='"+ bgDisplay +"'>",
						"<div class=\"formBR\" style='"+ bgDisplay +"'>",
							"<div class=\"formBC\" id=\"formBC" + id + "\" style=\"height:auto;"+ bgDisplay +"\">",
								"<div class=\"formMSG\" style=\"" + formMSGStyle + "\">",
									displayModeContent,
								"</div>",
								"<table cellpadding='0' cellspacing='0' class='formBtns'>",
									"<tr><td align='center' style='padding:15px 0px;'></td></tr>",
								"</table>",
							"</div>",
						"<div id=\"waitingP"+ id +"\" class=\"waitingP\" style=\"height:auto;\"></div>",
					"</div>",
				"</div>",
				"<a href=\"javascript:;\" class=\"formX " + settings.closeBtnClass + "\" hidefocus='true' onclick='return false;'></a>",
			"</div>"];

	var popupWin = Fai.top.$(html.join('')).appendTo("body");

	// ie6/7 popupWin的width在一开始时先用了contentWidth，这里重新修正
	if (Fai.isIE6() || Fai.isIE7()) {
		var formBL = popupWin.find('.formBL');
		var formBLPadding = Fai.getCssInt(formBL, 'padding-left') + Fai.getCssInt(formBL, 'padding-right') + Fai.getCssInt(formBL, 'border-left-width') + Fai.getCssInt(formBL, 'border-right-width');
		var formBR = popupWin.find('.formBR');
		var formBRPadding = Fai.getCssInt(formBR, 'padding-left') + Fai.getCssInt(formBR, 'padding-right') + Fai.getCssInt(formBR, 'border-left-width') + Fai.getCssInt(formBR, 'border-right-width');
		var formBC = popupWin.find('.formBC');
		var formBCPadding = Fai.getCssInt(formBC, 'padding-left') + Fai.getCssInt(formBC, 'padding-right') + Fai.getCssInt(formBC, 'border-left-width') + Fai.getCssInt(formBC, 'border-right-width');
		popupWin.css('width', (contentWidth + formBLPadding + formBRPadding + formBCPadding) + 'px');
	}

	// fix height
	var btnsHeight = 40;
	var offHeight = 20;
	if (!settings.bannerDisplay) {
		btnsHeight = 0;
	}
	if (popupWin.height() + btnsHeight > (browserHeight - offHeight)) {
		var diffHeight = popupWin.height() + btnsHeight - popupWin.find('.formMSG').height();	// 40预留给button区
		popupWin.find('.formMSG').css('height', (browserHeight - offHeight - diffHeight) + 'px');
		popupWin.css('top', (10 + scrollTop) + 'px');
	}

	/*if( Fai.isIE6() ){
		$('#fixFlashIframe' + id).attr('height',popupWin.height()+'px');
	}*/
	//重置加载层宽高度
	if (iframeMode) {
		Fai.top.$("#waitingP" + id).height(Fai.top.$("#formBC" + id).height())
			.width(Fai.top.$("#formBC" + id).width());
	} else {
		if( settings.waitingPHide ){
			Fai.top.$("#waitingP" + id).hide();
		}
	}
	
	if(settings.divInit != null){
		settings.divInit(id);
	}
	Fai.top.$("#popupWindow"+id).ready(function() {
		if (iframeMode) {
			var frameSrcUrlArgs = "popupID=" + id;
			Fai.top.$("#popupWindowIframe" + id).attr("src", Fai.addUrlParams(settings.frameSrcUrl, frameSrcUrlArgs)).load(function(){
				if( settings.waitingPHide ){
					Fai.top.$("#waitingP"+id).hide();
				}
			});
		}
		popupWin.draggable({
			start	: function(){
				// 拖动不选中
				Fai.top.$("body").disableSelection();
				// chrome,ie10以上 top选色板focusout失效。
				Fai.top.$("#colorpanel").remove();
                Fai.top.$(".faiColorPicker").remove();
			},
			handle	: '.formTL',
			stop	: function(){
				// 拖动不选中
				Fai.top.$("body").enableSelection();
			}
		});
		
		popupWin.find(".formX").bind("click", function(){
			if (Fai.isNull(options.msg)){
				Fai.closePopupWindow(id);
			} else {
				Fai.closePopupWindow(id, undefined, options.msg);
			}	
			return false;
		});
		popupWin.find(".formTL").disableSelection();
		
		// 如果开启了点击背景关闭
		if(settings.bgClose){
			Fai.top.$("#popupBg" + id).bind("click", function(){
				if (Fai.isNull(options.msg)){
					Fai.closePopupWindow(id);
				} else {
					Fai.closePopupWindow(id, undefined, options.msg);
				}
				return false;
			});
		}
		
	});
	
	if (Fai.isNull(Fai.top._popupOptions)) {
		Fai.top._popupOptions = {};
	}
	if (Fai.isNull(Fai.top._popupOptions["popup" + id])) {
		Fai.top._popupOptions["popup" + id] = {};
	}
	if(!Fai.isNull(options.callArgs)){
		Fai.top._popupOptions["popup" + id].callArgs = options.callArgs;
	}
	Fai.top._popupOptions["popup" + id].options = options;
	Fai.top._popupOptions["popup" + id].change = false;
	return id;
};

Fai.setPopupWindowChange = function(id, change) {
	if (Fai.isNull(Fai.top._popupOptions)){
		return;
	}
	if (Fai.isNull(Fai.top._popupOptions["popup" + id])) {
		return;
	}
	Fai.top._popupOptions["popup" + id].change = change;
};

Fai.closePopupWindow = function(id, closeArgs, msg){
	if(id) {
		try{
			if (Fai.isNull(Fai.top._popupOptions["popup" + id])) {
				return;
			}
			var popupOption = Fai.top._popupOptions["popup" + id];
			if(popupOption.change){
				if(typeof(msg)=='undefined'){
					if(!window.confirm("您的修改尚未保存，确定要离开吗？")){
						return;
					}
				}else{
					if(!window.confirm(msg)){
						return;
					}
				}
			}
			if(popupOption.refresh){
				Fai.top.location.reload();
				return;
			}
			// remove first
			Fai.top.Fai.removeAllIng(false);
			var options = popupOption.options;
			if (!Fai.isNull(options.closeFunc)) {
				if (closeArgs) {
					options.closeFunc(closeArgs);
				} else {
					options.closeFunc(Fai.top._popupOptions["popup" + id].closeArgs);
				}
			}		
			Fai.top._popupOptions["popup" + id] = {};
			//animate start
			var ppwObj = Fai.top.$("#popupWindow"+id);
			if(options.animate){
				Fai.top.Fai.closePopupWindowAnimate(ppwObj, options.animateTarget, options.animateOnClose);
			}
			//animate finish
			
			// for ie9 bug
			Fai.top.setTimeout("Fai.closePopupWindow_Internal('" + id + "')");
		}catch(err){
		}
	}else{
		Fai.removeBg();
		Fai.top.$(".formDialog").remove(); 
	}
};

Fai.closePopupWindow_Internal = function(id) {
	if (typeof id == 'undefined') {
		//在关闭窗口之前，先把swfuplaod销毁了
		if($.browser.msie && $.browser.version == 10){
			var popupWindowIframe = Fai.top.$(".formDialog").find(".popupWindowIframe")[0];
			if(popupWindowIframe){
				popupWindowIframeWindow = popupWindowIframe.contentWindow;
				
				if(popupWindowIframeWindow){
					try{
						if( popupWindowIframeWindow.swfObj ){
							popupWindowIframeWindow.swfObj.destroy();
						}
						if( popupWindowIframeWindow.editor ){
							if(popupWindowIframeWindow.editor.swfObj){
								popupWindowIframeWindow.editor.swfObj.destroy();
							}
						}
					}catch( e ){
						
					}
				}
			}
		}
		Fai.top.$(".popupBg").remove();
		//Fai.top.$("body").focus();	// 由于这里导致IE9下打开两次以前popupWindow之后。iframe里面的input无法focus进去，先注释掉
		Fai.top.$(".formDialog").remove(); 
	} else {
		//fix ie10 下图片上传的BUG
		//在关闭窗口之前，先把swfuplaod销毁了
		if($.browser.msie && $.browser.version == 10){
			var popupWindowIframe = Fai.top.document.getElementById("popupWindowIframe" + id);
			if(popupWindowIframe){
				popupWindowIframeWindow = popupWindowIframe.contentWindow;
				
				if(popupWindowIframeWindow){
					try{
						if( popupWindowIframeWindow.swfObj ){
							popupWindowIframeWindow.swfObj.destroy();
						}
						if( popupWindowIframeWindow.editor ){
							if(popupWindowIframeWindow.editor.swfObj){
								popupWindowIframeWindow.editor.swfObj.destroy();
							}
						}
					}catch( e ){
						
					}
				}
			}
			
		}

		Fai.top.Fai.removeBg(id);
		//Fai.top.$("body").focus();	// 由于这里导致IE9下打开两次以前popupWindow之后。iframe里面的input无法focus进去，先注释掉
		Fai.top.$("#popupWindowIframe" + id).remove();	// 这里尝试先remove iframe再后面remove div，看焦点会不会丢失
		Fai.top.$("#popupWindow"+id).remove();
		
	}
};

Fai.closePopupWindowAnimate = function (ppwObj, target, onclose) {
	var animateDiv = $('<div>');
	Fai.top.$('body').append(animateDiv);
	animateDiv.css({
		border: '1px solid #ff4400',
		position: 'absolute',
		'z-index': '9999',
		top: ppwObj.offset().top,
		left: ppwObj.offset().left,
		height: ppwObj.height() + 'px',
		width: ppwObj.width() + 'px'
	});
	var guide = Fai.top.$('body').find(target);
	animateDiv.animate({
		top: guide.offset().top + 'px',
		left: guide.offset().left + 'px',
		width: guide.width() + 'px',
		height: guide.height() + 'px'
	},'slow',function(){
		if (typeof onclose == 'function') {
			onclose();
		}
		animateDiv.remove();
	})
};

/*
click 				点击触发事件
msg 				点击关闭时弹出消息
*/
Fai.addPopupWindowBtn = function(id, btnOptions) {
	var win = Fai.top.$("#popupWindow" + id);
	win.find(".formBtns").show();
	var btnId = "popup" + id + btnOptions.id;
	var btns = win.find(".formBtns td");
	var btn = btns.find("#" + btnId);
	if (btn.length > 0){
		btn.remove();
	}
	if( btnOptions.click != 'help' ){
		if (typeof btnOptions["extClass"] != "undefined") {
			var extClass = btnOptions["extClass"];
			Fai.top.$("<input id='" + btnId + "' type='button' value='" + btnOptions.text + "' class='abutton faiButton' extClass='" + extClass + "'></input>").appendTo(btns);
		} else {
			Fai.top.$("<input id='" + btnId + "' type='button' value='" + btnOptions.text + "' class='abutton faiButton'></input>").appendTo(btns);
		}
	}
	btn = btns.find("#" + btnId);	
	if (typeof btn.faiButton == 'function') {
		btn.faiButton();
	}
	if(btnOptions.callback && Object.prototype.toString.call(btnOptions.callback) === "[object Function]"){
		btn.click(function() {
			btnOptions.callback();
			if (Fai.isNull(btnOptions.msg)){
				Fai.top.Fai.closePopupWindow(id);
			} else {
				Fai.top.Fai.closePopupWindow(id, undefined, btnOptions.msg);
			}
		});
	}
	
	if (btnOptions.click == 'close') {
		btn.click(function() {
			if (Fai.isNull(btnOptions.msg)){
				Fai.top.Fai.closePopupWindow(id);
			} else {
				Fai.top.Fai.closePopupWindow(id, undefined, btnOptions.msg);
			}
		});
	} else if (btnOptions.click == 'help') {
		if( win.find('a.formH').length == 0 ){
			win.append( "<a class='formH' href='" + btnOptions.helpLink + "' target='_blank' title='" + btnOptions.text + "'></a>" );
		}
	} else {
		btn.click(btnOptions.click);
	}
	
	if (btnOptions.disable) {
		btn.attr('disabled', true);
		btn.faiButton("disable");
	}
	
	// 捕捉弹窗输入Enter键，符合要求则触发保存
	// 性能问题：每次关闭弹窗，keydown函数会自动跟随窗体销毁
	$(document).keydown(function(e){
		if(e.keyCode == 13){
			var saveBtn = win.find("#popup"+ id +"save"),
				focusElem;
			
			if(saveBtn.length > 0 && !saveBtn.prop("disabled")){
				var focusElem = $(":focus");
				if(focusElem.is("input[type='text']") || focusElem.is("textarea")){
					return;
				}
				saveBtn.trigger("click");
			}
		}
	});
};

Fai.enablePopupWindowBtn = function(id, btnId, enable) {
	var win = Fai.top.$("#popupWindow" + id);
	btnId = "popup" + id + btnId;
	var btn = win.find("#" + btnId);
	if (enable){
		btn.removeAttr('disabled');
		btn.faiButton("enable");
	} else {
		btn.attr('disabled', true);
		btn.faiButton("disable");
	}
};

/*
弹出框（在当前页面内显示）
title				弹出框的标题
content				内容
width				内容区宽度（数字/字符串）
height				内容区高度（数字/字符串）
opacity				背景遮盖层的透明度，默认0.3
displayBg			是否加背景遮盖层，默认true
bannerDisplay		是否显示标题栏和边框，默认true
window_extClass 弹窗扩展class

closeFunc			关闭popup window时执行的函数
*/
Fai.popupBodyWindow = function(options){
	
	var settings = {
		title: '',
		width: 500,
		height: 300,
		bannerDisplay: true,
		opacity: '0.3',
		displayBg: true,
		window_extClass:'',
		bg_extClass : '' // 遮罩层自定义类
	};
	settings = $.extend(settings, options);

	var contentWidth = parseInt(settings.width);
	var contentHeight = parseInt(settings.height);
	var scrollTop = $("body").scrollTop();
	if (scrollTop == 0) {
		scrollTop = $("html").scrollTop();
	}
	var browserWidth = document.documentElement.clientWidth;
	if (!$.browser.msie) {
		browserWidth = document.body.clientWidth;
	}
	var browserHeight = document.documentElement.clientHeight;

	var bannerStyle = "";
	var bgDisplay = '';
	if (!settings.bannerDisplay)	{
		bannerStyle = "display:none;";
		bgDisplay ="background:none;";
	}

	var leftMar = 20;
	if(settings.leftMar != null) {
		leftMar = settings.leftMar;
	} else {
		leftMar = (browserWidth - contentWidth) / 2;
	}

	var topMar = 20;
	if(settings.topMar != null) {
		topMar = settings.topMar;
	} else {
		topMar = (browserHeight - contentHeight - 80) / 2;
	}

	var content			= '';
	if(settings.content != null) {
		content = settings.content;
	}
	
	var id = parseInt(Math.random()*10000);

	//加背景
	if(settings.displayBg) {
		Fai.bodyBg(id, settings.opacity, {"extClass":settings.bg_extClass});
	}
	
	var winStyle = "left:"+ leftMar +"px; top:"+ (topMar + scrollTop) +"px;";
	// ie6/7 popupWin的width无法自动，所以先写死再动态修正
	if (Fai.isIE6() || Fai.isIE7()) {
		winStyle += 'width:' + contentWidth + 'px;';
	}
	var formMSGStyle = "position:relative;width:" + contentWidth + 'px;height:' + contentHeight + 'px;';

	var html = [
			"<div id=\"popupWindow"+ id +"\" class=\"formDialog "+ settings.window_extClass +"\" style=\"" + winStyle + "\">",
				"<div class=\"formTL\" style='"+ bannerStyle +"'>",
					"<div class=\"formTR\">",
						"<div class=\"formTC\">" + settings.title + "</div>",
					"</div>",
				"</div>",
				"<div class=\"formBL\" style='"+ bgDisplay +"'>",
					"<div class=\"formBR\" style='"+ bgDisplay +"'>",
						"<div class=\"formBC\" id=\"formBC\" style=\"height:auto;"+ bgDisplay +"\">",
							"<div class=\"formMSG\" style=\"" + formMSGStyle + "\">",
								content,
							"</div>",
							"<table cellpadding='0' cellspacing='0' class='formBtns'>",
								"<tr><td align='center' class='formBtnsContent'></td></tr>",
							"</table>",
						"</div>",
					"</div>",
				"</div>",
				"<a class=\"formX\" href='javascript:;' hidefocus='true' onclick='return false;'></a>",
			"</div>"];
	$(html.join('')).appendTo("body");

	var popupWin = $("#popupWindow"+id);

	// ie6/7 popupWin的width在一开始时先用了contentWidth，这里重新修正
	if (Fai.isIE6() || Fai.isIE7()) {
		var formBL = popupWin.find('.formBL');
		var formBLPadding = Fai.getCssInt(formBL, 'padding-left') + Fai.getCssInt(formBL, 'padding-right') + Fai.getCssInt(formBL, 'border-left-width') + Fai.getCssInt(formBL, 'border-right-width');
		var formBR = popupWin.find('.formBR');
		var formBRPadding = Fai.getCssInt(formBR, 'padding-left') + Fai.getCssInt(formBR, 'padding-right') + Fai.getCssInt(formBR, 'border-left-width') + Fai.getCssInt(formBR, 'border-right-width');
		var formBC = popupWin.find('.formBC');
		var formBCPadding = Fai.getCssInt(formBC, 'padding-left') + Fai.getCssInt(formBC, 'padding-right') + Fai.getCssInt(formBC, 'border-left-width') + Fai.getCssInt(formBC, 'border-right-width');
		//注：内层popupWin manage.src.css	 .formDialog .formMSG 存在margin属性 
		var formMSG = popupWin.find('.formMSG');
		var formMSGMargin = Fai.getCssInt(formMSG, 'margin-left') + Fai.getCssInt(formMSG, 'margin-right') + Fai.getCssInt(formMSG, 'border-left-width') + Fai.getCssInt(formMSG, 'border-right-width');
		popupWin.css('width', (contentWidth + formBLPadding + formBRPadding + formBCPadding + formMSGMargin) + 'px');
	}
	popupWin.ready(function() {
		$(".formDialog").draggable({handle:'.formTL'});
		$(".formTL").disableSelection();
		$('.formX').click(function(){
			if( settings.beforeClose ){
				settings.beforeClose();
			}
			Fai.closePopupBodyWindow(id);
			//对急速建站弹出来的特殊处理
			Fai.top.$('#popupBgTitle'+id).remove();
		});
	});
	popupWin.data('settings', settings);

	return id;
};

Fai.closePopupBodyWindow = function(id){
	if(id)
	{
		Fai.removeBodyBg(id);
		var popup = $("#popupWindow"+id);
		var settings = popup.data('settings');
		if (settings && typeof settings.closeFunc == 'function') {
			settings.closeFunc();
		}
		popup.remove(); 
		$("body").focus();
	}else{
		Fai.removeBodyBg();
		$(".formDialog").remove(); 
	}
};

Fai.addPopupBodyWindowBtn = function(id, btnOptions) {
	var win = $("#popupWindow" + id);
	win.find(".formBtns").show();
	var btnId = "popup" + id + btnOptions.id;
	var btns = win.find(".formBtns td");
	var btn = btns.find("#" + btnId);
	if (btn.length > 0){
		btn.remove();
	}
	
	if( win.find(".popupButtons").length != 1 ){
		$("<span class='popupButtons'></span>").appendTo(btns);
	}
	if( win.find(".popupCheckboxs").length === 1 ){
		//存在checkBox   button向右飘
		$(win.find(".popupButtons")[0]).css("margin-right", "10px").css("float", "right").css("margin-top","-3px");
		if (Fai.isIE6()){
			$(win.find(".popupButtons")[0]).css("margin-top","-20px");
		}
	}
	
	var buttonHtml = "";
	if (typeof btnOptions["extClass"] != "undefined") {
		var extClass = " " + btnOptions["extClass"];
		buttonHtml = "<input id='" + btnId + "' type='button' value='" + btnOptions.text + "' class='abutton faiButton' extClass='" + extClass + "'></input>";
	} else {
		buttonHtml = "<input id='" + btnId + "' type='button' value='" + btnOptions.text + "' class='abutton faiButton'></input>";
	}
	
	$(buttonHtml).appendTo( $(btns).find(".popupButtons") );
	
	btn = btns.find("#" + btnId);
	if (typeof btn.faiButton == 'function') {
		btn.faiButton();
	}
	
	if (btnOptions.click == 'close') {
		btn.click(function() {
			Fai.closePopupBodyWindow(id);
		});
	} else {
		btn.click(btnOptions.click);
	}
	if (btnOptions.disable) {
		btn.attr('disabled', true);
		btn.faiButton("disable");
	}
};

Fai.addPopupBodyWindowCheckBox = function(id, options){
	var win = $("#popupWindow" + id);
	win.find(".formBtns").show();
	var btnId = "popup" + id + options.id;
	var btns = win.find(".formBtns td");
	var btn = btns.find("#" + btnId);
	if (btn.length > 0){
		btn.remove();
	}
	
	var checkboxHtml = "<input id='" + btnId + "' type='checkbox' /><label for='" + btnId + "'>"+options.text+"</label>";
	if( win.find(".popupCheckboxs").length != 1 ){
		btns.removeAttr("align").css("line-height", "22px");
		$(btns.find(".popupButtons")[0]).css("margin-right", "10px").css("float", "right");
		$("<span class='popupCheckboxs'>"+checkboxHtml+"</span>").appendTo(btns);
	}else{
		$(checkboxHtml).appendTo( $(win.find(".popupCheckboxs")[0]) );
	}

	if( options.init === "checked" ){
		$("#"+btnId).attr("checked","checked");
	}
	
	btn = btns.find("#" + btnId);
	
	btn.click(options.click);
	if (options.disable) {
		btn.attr('disabled', true);
	}
};

Fai.enablePopupBodyWindowBtn = function(id, btnId, enable) {
	var win = $("#popupWindow" + id);
	btnId = "popup" + id + btnId;
	var btn = win.find("#" + btnId);
	if (enable){
		btn.removeAttr('disabled');
		btn.faiButton("enable");
	} else {
		btn.attr('disabled', true);
		btn.faiButton("disable");
	}
};


/*
ajax提交成功返回的结果的处理
尚未登录，则返回success:false,login:true,loginUrl:url
jumpUrl:处理成功后跳转的页面，如果为null或者""，则不跳转
successMsg/failureMsg: 如果successMsg或failureMsg为空，则使用ajax返回的结果
jumpMode: 跳转页面的模式（0--当前页面跳转(默认)	1--top页面跳转 2--parent页面跳转, 3--不跳转， 4--指定位置刷新（用eval执行传过来的语句)），5--当前页面跳转(地址不去锚，暂时用于Mobi端)
alertMode: 弹出对话框的模式(0--默认的alert模式 1--便签模式Fai.ing("") 2--便签模式且不自动关闭tips Fai.ing("", false),3-不弹出)
*/
Fai.successHandle = function(resultData, successMsg, failureMsg, jumpUrl, jumpMode, alertMode){
	Fai.top.$("#ing").find(".tips").remove();
	var result = jQuery.parseJSON(resultData);
	var output = "";
	if(result.success)
	{
		if(result.msg){
			output = result.msg;
		}
		if(successMsg != ""){
		   output = successMsg;
		}
		if (output && output != ""){
			if(alertMode == 0){
				Fai.top.Fai.removeIng(true);
				alert(output);
			}else if(alertMode == 1){
				Fai.ing(output, true);
			}else if(alertMode == 2){
				Fai.ing(output, false);
			}else if(alertMode == 3){
				
			}else{
				Fai.top.Fai.removeIng(true);
				alert(output);
			}
		}
		if(jumpUrl!=""){
			if (jumpMode == 1){
				jumpUrl = jumpUrl.replace(/#.*/, '');// remove #
				if (Fai.top.location.href == jumpUrl) {
					Fai.top.location.reload();
				} else {
					Fai.top.location.href = jumpUrl;
				}
			} else if (jumpMode == 2) {
				jumpUrl = jumpUrl.replace(/#.*/, '');// remove #
				if (parent.location.href == jumpUrl) {
					parent.location.reload();
				} else {
					parent.location.href = jumpUrl;
				}
			} else if (jumpMode == 3) {
				return result.success; 
			} else if (jumpMode == 4){
				Fai.fkEval(jumpUrl);
			} else if(jumpMode == 5){
				if (Fai.top.location.href == jumpUrl) {
					Fai.top.location.reload();
				} else {
					Fai.top.location.href = jumpUrl;
				}
			} else {
				jumpUrl = jumpUrl.replace(/#.*/, '');// remove #
				if (document.location.href == jumpUrl) {
					document.location.reload()
				} else {
					document.location.href = jumpUrl;
				}
			}
		}
	}
	else {
		/*
		if(result.login){
			//弹出登录框
			var loginUrl = "/loginForm.jsp";
			if (result.loginUrl){
				loginUrl = result.loginUrl;
			}
			
			loginUrl = Fai.addUrlParams(loginUrl, "f=" + Fai.encodeUrl(Fai.top.location.href));
			Fai.popupWindow({title:"请重新登录", frameSrcUrl:loginUrl + '&ram='+Math.random(), width:'380', height:'195'});
			return;
		}
		*/
		if(result.msg){
			output = result.msg;
		}
		if(output == ""){
			output = failureMsg;
		}
		if(output == ""){
			output = "系统错误";
		}
		if(alertMode == 0){
			alert(output);
		}else if(alertMode == 1 || alertMode == 2){
			Fai.ing(output, false);
		}else{
			alert(output);
		}
	}
	return result.success;
};

/*
检查当前页面是否内嵌页面，如果不是，则跳转到指定的url，并带上当前的url作为url参数
*/
Fai.checkEmbed = function(url, item) {
	if (Fai.top.location.href == document.location.href) {
		var local = document.location.href;
		local = local.replace(/http:\/\/[^\/]+/, '');
		Fai.top.location.href = Fai.addUrlParams(url, 'url=' + Fai.encodeUrl(local) + '&item=' + Fai.encodeUrl(item));
	}
};

Fai.disable = function(id, disable) {
	if (disable) {
		$('#' + id).attr('disabled', true);
	} else {
		$('#' + id).removeAttr('disabled');
	}
};



//jquery 下拉菜单
var timeout         = 500;
var closetimer		= 0;
var ddmenuitem      = 0;

Fai.dropdownForm_open = function()
{
	Fai.dropdownForm_canceltimer();
	Fai.dropdownForm_close();
	ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');
};

Fai.dropdownForm_close = function()
{
	if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');
};

Fai.dropdownForm_timer = function()
{
	closetimer = window.setTimeout(Fai.dropdownForm_close, timeout);
};

Fai.dropdownForm_canceltimer = function()
{
	if(closetimer)
	{	window.clearTimeout(closetimer);
		closetimer = null;}
};

$(function()
{	
	try{
		$('.dropdownForm > div').bind('click', Fai.dropdownForm_open);
		$('.dropdownForm > div').bind('mouseover', Fai.dropdownForm_open);
		$('.dropdownForm > div').bind('mouseout',  Fai.dropdownForm_timer);
		
		Number.prototype.toFixed = function(s){
			var changenum=(parseInt(   Math.round(this * Math.pow( 10, s ) + Math.pow(10, -(s+2)) ) )/ Math.pow( 10, s )).toString();
			var index=changenum.indexOf(".");
			if(index<0&&s>0){
				changenum=changenum+".";
			for(var i=0;i<s;i++){
				changenum=changenum+"0";
			}
			
			}else {
				index=changenum.length-index;
				for(var i=0;i<(s-index)+1;i++){
					changenum=changenum+"0";
				}
			}
			return  changenum;
		}
	}catch(exp){
	}
});

/*
使用方法：
方法1：Fai.preloadImg(banner_1.jpg")
方法2：
var imgsrc=new Array(   
"/images/banner_1.jpg",   
"/images/banner_34.jpg",   
"/images/banner_35.jpg"
}
Fai.preloadImg(imgsrc);
*/
Fai.preloadImg = function(){   
  var Arrimg=new Array();   
  if(typeof(arguments[0])=="string")Arrimg[0]=arguments[0];   
  if(typeof(arguments[0])=="object"){   
      for(var i=0;i<arguments[0].length;i++){   
          Arrimg[i]=arguments[0][i]   
        }   
    }   
  var img=new Array()   
  for(var i=0;i<Arrimg.length;i++){   
    img[i]=new Image();   
    img[i].src=Arrimg[i];   
  }   
};

//document.onclick = Fai.dropdownForm_close;

// 检查html中所有的img，如果有faiSrc属性，则把属性值设置到src
Fai.delayLoadImg = function(countPer) {
	if (typeof countPer == 'undefined' || countPer <= 0) {
		countPer = 10;
	}
	setTimeout("Fai.doDelayLoadImg(" + countPer + ")", 200);
};
Fai.doDelayLoadImg = function(countPer){
	// 每次只加载10张新图
	var count = 0;
	$("img").each(function(){
		var faiSrc = $(this).attr('faiSrc');
		if (!Fai.isNull(faiSrc) && faiSrc != ''){
			if (faiSrc != $(this).attr('src')){
				++ count;
				$(this).show();
				$(this).attr('src', faiSrc);
				if (count >= countPer) {
					return false;
				}
			}
		}
	});
	if (count >= countPer) {
		setTimeout("Fai.doDelayLoadImg(" + countPer + ")", 200);
	}
};


//
Fai.editableDiv = function(divID){
        //获得当前点击的对象
	var td=$("#" + divID);
	//取出当前td的文本内容保存起来
	var tdWidth = td.width();
	var oldText=td.text();
	//建立一个文本框，设置文本框的值为保存的值   
	var input=$("<input type='text' value='"+oldText+"'/>"); 
	//将当前td对象内容设置为input
	td.html(input);
	//设置文本框的点击事件失效
	input.click(function(){
		return false;
	});
	//设置文本框的样式
	//input.css("border-width","1"); 
	input.css("font-size","12px");
	input.css("text-align","left");
	//设置文本框宽度等于td的宽度
	input.width(tdWidth-10);
	//当文本框得到焦点时触发全选事件  
	input.trigger("focus").trigger("select"); 
	input.focus();
	//当文本框失去焦点时重新变为文本
	input.blur(function(){
		var input_blur=$(this);
		//保存当前文本框的内容
		var newText=input_blur.val();
		if(newText==""){
			td.html("<span>默认栏目名称</span>");
		}else{
			td.html("<span>" + newText + "</span>"); 
		}
		if(newText != oldText)
		{
			$("#saveButton").attr("disabled", false);
		}
	}); 
	//响应键盘事件
	input.keyup(function(event){
		// 获取键值
		var keyEvent=event || window.event;
		var key=keyEvent.keyCode;
		//获得当前对象
		var input_blur=$(this);
		switch(key)
		{
			case 13://按下回车键，保存当前文本框的内容
				var newText=input_blur.val(); 
				if(newText==""){
					td.html("<span>默认栏目名称</span>");
				}else{
					td.html("<span>" + newText + "</span>");
				}
				if(newText != oldText)
				{
					$("#saveButton").attr("disabled", false);
				}
			break;
			
			case 27://按下esc键，取消修改，把文本框变成文本
				td.html("<span>" + oldText + "</span>"); 
			break;
		}
	});
};

Fai.containsChinese = function(str){ 
	var re = /[\u4e00-\u9fa5]+/; 
	return re.test(str);
};

Fai.refreshClass = function(e){
	e.children().each(function() {
		$(this).attr("class", $(this).attr("class"));
		Fai.refreshClass($(this));
	});
};

// 注册时间处理函数（这些函数会在Fai.bg调用是被停止处理）
Fai.addInterval = function(id, func, interval) {
	if (Fai.isNull(Fai.intervalFunc)){
		Fai.intervalFunc = new Array();
	}
	// 检查是否有重复 20121217新增：有重复的，先删掉旧的，再添加新的
	for (var i = 0; i < Fai.intervalFunc.length; ++i){
		if (Fai.intervalFunc[i].id == id){
			Fai.intervalFunc.splice(i, 1);
			break;
		}
	}
	Fai.intervalFunc.push({id:id, func:func, interval:interval, type: 1});
};

// 注册时间处理函数（这些函数会在Fai.bg调用是被停止处理）
Fai.addTimeout = function(id, func, interval) {
	if (Fai.isNull(Fai.intervalFunc)){
		Fai.intervalFunc = new Array();
	}
	// 检查是否有重复 20121217新增：有重复的，先删掉旧的，再添加新的
	for (var i = 0; i < Fai.intervalFunc.length; ++i){
		if (Fai.intervalFunc[i].id == id){
			Fai.intervalFunc.splice(i, 1);
			break;
		}
	}
	Fai.intervalFunc.push({id:id, func:func, interval:interval, type: 0});
};

// 启动时间处理函数，id为null表示启动所有
Fai.startInterval = function(id) {
	if (Fai.isNull(Fai.intervalFunc)){
		return;
	}
	for (var i = 0; i < Fai.intervalFunc.length; ++i){
		var x = Fai.intervalFunc[i];
		if (id == null || x.id == id){
			if (x.timer){
				clearInterval(x.timer);
			}
			if (x.type == 1) {
				x.timer = setInterval(x.func, x.interval)
			} else {
				x.timer = setTimeout(x.func, x.interval)
			}
		}
	}
};

// 停止时间处理函数，id为null表示停止所有
Fai.stopInterval = function(id) {
	if (Fai.isNull(Fai.intervalFunc)){
		return;
	}
	for (var i = 0; i < Fai.intervalFunc.length; ++i){
		var x = Fai.intervalFunc[i];
		if (id == null || x.id == id){
			if (x.timer){
				clearInterval(x.timer);
			}
		}
	}
};


// 相同的opacity不再设置，避免占用cpu
jQuery.extend( jQuery.fx.step, {
	opacity: function( fx ) {
		var o = jQuery.style(fx.elem, "opacity");
		if (o == null || o == '' || o != fx.now) {
			jQuery.style(fx.elem, "opacity", fx.now);
		}
	}
});
// failinear是限制了效果的修改次数，以便降低CPU的使用，即不管效果的时间多长，都只修改10次
jQuery.extend( jQuery.easing,
{
	// t: current time, b: begInnIng value, c: change In value, d: duration
	faicount: 10,
	failinear: function (x, t, b, c, d) {
//		alert('x=' + x + ' t=' + t + ' b=' + b + ' c=' + c + ' d=' + d);
		// 只修改jQuery.easing.faicount次
		var interval = Math.abs(c - b) / jQuery.easing.faicount;
		if (interval == 0){
			return c;
		}
		x = parseInt(x / interval) * interval;
		return jQuery.easing['linear'](x, t, b, c, d);
	},
	easeOutQuart: function (x, t, b, c, d) {  
		// 先快速后慢速
	    return -c * ((t=t/d-1)*t*t*t - 1) + b;
	}
});

// 设置缺省的easing为failinear
Fai.easingFaiLinear = function() {
	jQuery.extend( jQuery.easing,
	{
		swing: function (x, t, b, c, d) {
			return jQuery.easing['failinear'](x, t, b, c, d);
		}
	});
};

Fai.getDivHeight = function(obj)
{
	var padding = Fai.getCssInt(obj, 'padding-top') + Fai.getCssInt (obj, 'padding-bottom');
	var margin = Fai.getCssInt(obj, 'margin-top') + Fai.getCssInt (obj, 'margin-bottom');
	var border = Fai.getCssInt(obj, 'border-top-width') + Fai.getCssInt (obj, 'border-bottom-width');
	var height = obj.height();
	return height + border + margin + padding;
};

Fai.getDivWidth = function(obj)
{
	var padding = Fai.getCssInt(obj, 'padding-left') + Fai.getCssInt (obj, 'padding-right');
	var margin = Fai.getCssInt(obj, 'margin-left') + Fai.getCssInt (obj, 'margin-right');
	var border = Fai.getCssInt(obj, 'border-left-width') + Fai.getCssInt (obj, 'border-right-width');
	var width = obj.width();
	return width + border + margin + padding;
};

Fai.getFrameHeight = function(obj)
{
	var padding = Fai.getCssInt(obj, 'padding-top') + Fai.getCssInt (obj, 'padding-bottom');
	var margin = Fai.getCssInt(obj, 'margin-top') + Fai.getCssInt (obj, 'margin-bottom');
	var border = Fai.getCssInt(obj, 'border-top-width') + Fai.getCssInt (obj, 'border-bottom-width');
	return border + margin + padding;
};

Fai.getFrameWidth = function(obj)
{
	var padding = Fai.getCssInt(obj, 'padding-left') + Fai.getCssInt (obj, 'padding-right');
	var margin = Fai.getCssInt(obj, 'margin-left') + Fai.getCssInt (obj, 'margin-right');
	var border = Fai.getCssInt(obj, 'border-left-width') + Fai.getCssInt (obj, 'border-right-width');
	return border + margin + padding;
};

/*
option.host(jQuery object): 控件对象,
option.data(objects Array): [ {}, {}, {}...]
				object.href 每一项的href		(*)
				object.html 每一项的html内容	(*)
option.cls  每一项的class						(*)
option.mode: 0:显示在host的左下角(默认); 1:显示在host的右上角
option.closeMode 0:mouseleave close(默认); 1:blur close
option.fixpos: true:修正菜单在屏幕边界时的位置(默认)
option.navSysClass: 系统样式
Fai.showMenu( { host:$('#element'), data:[ {html:'导出全部订单', href:'javascript:alert(0)'},{html:'导出当前订单', href:'javascript:alert(0)'} ] } );
其余的参数均可取默认 class可以为空  但不推荐为空
*/
Fai.showMenu = function(option) {

	var id = option.id;
	if (Fai.isNull(id)){
		id = '';
	}
	var host = option.host;
	var mode = 0;
	if (!Fai.isNull(option.mode)){
		mode = option.mode;
	}
	if (Fai.isNull(option.fixpos)){
		option.fixpos = true;
	}
	//参考菜单对象，不能为空，用于设置二级菜单横向时的位置
	var rulerObj = option.rulerObj;
	
	var navSysClass = option.navSysClass;
	if (Fai.isNull(navSysClass)){
		navSysClass = '';
	}
	// 关闭模式
	var closeMode = 0;
	if (!Fai.isNull(option.closeMode)){
		closeMode = option.closeMode;
	}
	var left = 0;
	var top = 0;
	if (mode == 1){
		left = host.offset().left + host.width();
		top = host.offset().top;
	} else {
		left = host.offset().left;
		top = host.offset().top + host.height();
	}
	var menu = $("#g_menu" + id);
	if (menu.length != 0){
		if(closeMode == 0){
			menu.attr("_mouseIn", 1);
			return menu;
		} else {
			menu.attr("_mouseIn", 0);
			Fai.hideMenu();
			return null;
		}
	}
	
	// 删除所有菜单
	$(".g_menu").each(function(){
		$(this).remove();
	});

	var data = option.data;
	if (option.data == null || option.data == ''){
		return null;
	}
	//console.log(option.cls);
	menu = $("<div id='g_menu" + id + "' tabindex='0' hidefocus='true' class='g_menu " + option.cls + " " + (option.clsIndex?option.cls + "Index" + option.clsIndex:'') + " "+ navSysClass +"' style='display:block;outline:none;'></div>");
	menu.appendTo($("body"));
	var content = $("<div class='content contentLayer1'></div>");
	content.appendTo(menu);
	Fai.addMenuItem(data, content, option);

	// 修正菜单超过底端时的表现
	if (option.fixpos) {
		if (top + menu.height() + 20 > $(document).height()) {
			top = host.offset().top - menu.height();
		}
	}
	menu.css("left", left - Fai.getCssInt(content, 'border-left-width') + "px");
	menu.css("top", top + "px");
		
	if(closeMode == 0){
		// 过100ms后才隐藏，避免鼠标在host和menu之间移动会隐藏
		menu.mouseleave(function(){
			menu.attr("_mouseIn", 0);
			setTimeout("Fai.hideMenu()", 100);
		});
		menu.mouseover(function(){
			menu.attr("_mouseIn", 1);
		});
		menu.click(function(){
			menu.attr("_mouseIn", 0);
			Fai.hideMenu();
		});
		host.mouseleave(function(){
			menu.attr("_mouseIn", 0);
			setTimeout("Fai.hideMenu()", 100);
		});
		host.mouseover(function(){
			menu.attr("_mouseIn", 1);
		});
	}else{
		host.mousedown(function(){
			menu.attr("_mouseIn", 2);
		});
		menu.bind('blur',function(){
			if (menu.attr("_mouseIn") != 2) {
				menu.attr("_mouseIn", 0);
				setTimeout("Fai.hideMenu()", 100);
			}
		});
		menu.focus();
	}

	if (typeof g_bindMenuMousewheel == 'undefined'){
		g_bindMenuMousewheel = 1;
		$('body').bind('mousewheel',function(){
			$("#g_menu").remove();
		});
	}
	menu.attr("_mouseIn", 1);
	menu.slideDown(200);
	Fai.calcMenuSize(menu, option);
	
	// 修正横向二级菜单位置及超出竖排问题 一定要放在calMenuSize修正之后
	var tmpItemsObj = $("#g_menu" + id + ">div.content>table>tbody>tr>td.center>table.item");
	var tmpMenuObj = $("#g_menu" + id);
	var tmpMenuObjLeftRightOuterWidth = 		// 二级菜单左右两边的空白位及border
		(tmpMenuObj.outerWidth() - tmpMenuObj.width()) +
		(tmpMenuObj.find(".content").outerWidth() - tmpMenuObj.find(".content").width()) +
		(tmpMenuObj.find(".content .middle").outerWidth() - tmpMenuObj.find(".content .middle").width()) + 
		(tmpMenuObj.find(".content .middle .left").outerWidth() - tmpMenuObj.find(".content .middle .right").outerWidth()) +
		(tmpMenuObj.find(".content .middle .center").outerWidth() - tmpMenuObj.find(".content .middle .center").width());
		
	var tmpItemObjClear = tmpItemsObj.first().css("clear");
	var tmpItemObjWidth = tmpItemsObj.first().outerWidth();
	var length = tmpItemsObj.length;
	if(tmpItemObjClear == "none"){
		if(length > 1 && tmpItemObjWidth > 0){
			var tmpItemsWidth = tmpItemObjWidth*length;
			var clientWidth = document.documentElement.clientWidth;
			var tmpMenuObjLeft = tmpMenuObj.offset().left;
			var tmpMenuObjRight = tmpMenuObj.offset().right;
			var tmpMenuObjWidth = tmpMenuObj.width();
			var tmpHostLeft = host.offset().left;
			
			var tmpRulerObjWidth = rulerObj.outerWidth();
			var tmpRulerObjLeft = rulerObj.offset().left;
			var tmpRulerObjRightLeft = tmpRulerObjLeft + tmpRulerObjWidth;
			
			//如果在右半部份的菜单
			if(tmpHostLeft > clientWidth/2){
				if(tmpItemsWidth < clientWidth && tmpItemsWidth > clientWidth/2){
					var newMenuObjLeft = tmpRulerObjRightLeft - tmpItemsWidth;
					tmpMenuObj.offset({left:newMenuObjLeft - tmpMenuObjLeftRightOuterWidth}); // 去掉左右外框补白的宽度
					tmpMenuObj.find(".content>.middle").width("100%");
				}
				if(tmpItemsWidth < clientWidth && tmpItemsWidth < clientWidth/2 && (tmpRulerObjRightLeft - tmpMenuObjLeft) < tmpItemsWidth ){
					var newMenuObjLeft = tmpRulerObjRightLeft - tmpItemsWidth;
					tmpMenuObj.offset({left:newMenuObjLeft - tmpMenuObjLeftRightOuterWidth}); // 去掉左右外框补白的宽度
					tmpMenuObj.find(".content>.middle").width("100%");
				}
				
				if(tmpItemsWidth > clientWidth){	//二级菜单长度大过显示的长度
					if(clientWidth < tmpMenuObjWidth){		//拖动屏缩小到浏览器宽度大于内容页宽度
						tmpMenuObj.offset({left:0});
						tmpMenuObj.find(".content>.middle").width("100%");
					}else{
						tmpMenuObj.offset({left:tmpRulerObjLeft});
						tmpMenuObj.find(".content>.middle").width("100%");
					}
				}
			}else{	//左半
				if(tmpItemsWidth < clientWidth && (clientWidth - tmpHostLeft) < tmpItemsWidth){
					var newMenuObjLeft = clientWidth - tmpItemsWidth;
					tmpMenuObj.offset({left:newMenuObjLeft - tmpMenuObjLeftRightOuterWidth}); // 去掉左右外框补白的宽度
					tmpMenuObj.find(".content>.middle").width("100%");
				}
				if(tmpItemsWidth < clientWidth && (tmpRulerObjRightLeft - tmpMenuObjLeft) < tmpItemsWidth){
					var newMenuObjLeft = tmpRulerObjRightLeft - tmpItemsWidth;
					tmpMenuObj.offset({left:newMenuObjLeft - tmpMenuObjLeftRightOuterWidth}); // 去掉左右外框补白的宽度
					tmpMenuObj.find(".content>.middle").width("100%");
				}
				if(tmpItemsWidth > clientWidth){	//二级菜单长度大过显示的长度
					if(clientWidth < tmpMenuObjWidth){		//拖动屏缩小到浏览器宽度大于内容页宽度
						tmpMenuObj.offset({left:0});
						tmpMenuObj.find(".content>.middle").width("100%");
					}else{
						tmpMenuObj.offset({left:tmpRulerObjLeft});
						tmpMenuObj.find(".content>.middle").width("100%");
					}
				}
			}
			
		}
	}
	return menu;
};

Fai.addMenuItem = function(data, menu, option) {
	if (data.length <= 0){
		return;
	}
	var contentHtml = ["<table class='top' cellpadding='0' cellspacing='0'><tr><td class='left'></td><td class='center'></td><td class='right'></td></tr></table>",
		"<table class='middle' cellpadding='0' cellspacing='0'><tr><td class='left'></td><td class='center'></td><td class='right'></td></tr></table>",
		"<table class='bottom' cellpadding='0' cellspacing='0'><tr><td class='left'></td><td class='center'></td><td class='right'></td></tr></table>"];
	var content = $(contentHtml.join(''));
	content.appendTo(menu);
	content = content.parent().find(".middle .center");
	for (var i = 0; i < data.length; ++i){
		var item = data[i];
		var sub = item.sub;
		var href = item.href;
		var onclick = item.onclick;
		var target = item.target;
		var disable = item.disable;
		var cls = "";
		if (!href && !onclick){
			// 通常有子菜单的项就是两者都不存在
			href = "";
			onclick = "";
		} else {
			if (!href){
				href = " href='javascript:;'";
			} else {
				if( disable ) {
					href = "";
				} else {
					href = " href=\"" + href + "\" style=\"cursor:pointer;\"";	
				}
			}
			if (!onclick){
				onclick = "";
			} else {
				onclick = " onclick=\"" + onclick + "\"";
			}
			if (!target){
				target = "";
			} else {
				target = " target='" + target + "'";
			}
		}
		var id = parseInt(Math.random() * 100000);
		var html = [];
		var itemIndex = i+1;		// 用于item的索引，用于设置样式
		html.push("<table class='item itemIndex" + itemIndex + "' itemId='");
		html.push(id);
		html.push("' cellpadding='0' cellspacing='0'><tr><td class='itemLeft'></td><td class='itemCenter'><a hidefocus='true' ");
		html.push(href);
		html.push(onclick);
		html.push(target);
		if (item.title) {
			html.push(" title='" + item.title + "'");
		}
		html.push(">" + item.html + "</a></td><td class='itemRight'></td></tr></table>");
		var div = $(html.join(''));
		if (sub){
			div.addClass("itemPopup");
		}
		if (content.find(' > .subMenu').length >= 1){
			div.insertBefore(content.find(' > .subMenu').first());
		} else {
			div.appendTo(content);
		}
		if (sub){
			if (sub.length == 0){
				//sub.push({html:"----"});
			}
			var subMenu = $("<div class='subMenu' itemId='" + id + "'><div class='content contentLayer2'></div></div>");
			subMenu.appendTo(content);
			var subContent = subMenu.find(' > .content');
			Fai.addMenuItem(sub, subContent, option);
			subMenu.mouseleave(function(){
				$(this).attr("_mouseIn", 0);
				//setTimeout("Fai.hideSubMenu()", 100);
				setTimeout(function(){
					Fai.hideSubMenu();
				}, 100);
				
				//鼠标移出三级菜单，修正g_menu透明区域位置
				if( option.navBar == true  && Fai.isIE() ){
					var $g_menu = $("#g_menu" + option.id);
					var $contentLayer1 = $g_menu.find(".contentLayer1");
					var g_menuHeight = $contentLayer1.outerHeight(true);
					var g_menuWidth = $contentLayer1.outerWidth(true);
					var contentLayer1_contentWidth = $contentLayer1.children(".middle").first().outerWidth(true);
					$g_menu.css({'width':g_menuWidth+'px', 'height':g_menuHeight+'px'});
					$contentLayer1.css({'width':contentLayer1_contentWidth+'px'});
				}
				
			});
			subMenu.mouseover(function(){
				$(this).attr("_mouseIn", 1);
			});
			subMenu.click(function(){
				$(this).attr("_mouseIn", 0);
				Fai.hideSubMenu();
			});
		}
		div.hover(function(){
			var div2 = $(this);
			var subMenu2 = null;
			$(this).parent().find(' > .subMenu').each(function(j, k){
				if ($(this).attr("itemId") == div2.attr("itemId")){
					subMenu2 = $(this);
				}
			});
			if (subMenu2 != null && subMenu2.length == 1){
				//进入二级菜单（有下一级菜单的）
				if (subMenu2.css("display") == "none"){
					if (subMenu2.attr("_hadShow") != 1) {
						var left = div2.position().left + div2.width();
						var top = div2.position().top;
						// 修正菜单超过底端时的表现
						if (option.fixpos) {
							var diffY = div2.offset().top + subMenu2.height() + 20 - $(document).height();
							if (diffY > 0) {
								top = top - diffY;
							}
						}
						subMenu2.css("left", left + "px");
						subMenu2.css("top", top + "px");
						subMenu2.slideDown(200);
						Fai.calcMenuSize(subMenu2, option);
						subMenu2.attr("_hadShow", 1);
					} else {
						subMenu2.slideDown(200);
					}					
				}
				subMenu2.attr("_mouseIn", 1);
				//修正g_menu透明区域位置
				if( option.navBar == true && Fai.isIE() ) {
					var $g_menu = $("#g_menu" + option.id);
					var $contentLayer1 = $g_menu.find(".contentLayer1");
					var $contentLayer2 = subMenu2.find(".contentLayer2");
					var contentLayer1_height = $contentLayer1.outerHeight(true);
					var contentLayer1_width = $contentLayer1.outerWidth(true);
					var contentLayer1_contentWidth = $contentLayer1.children(".middle").first().outerWidth(true);
					var contentLayer2_height = $contentLayer2.outerHeight(true);
					var contentLayer2_width = $contentLayer2.outerWidth(true);
					var subMenu2_top = subMenu2.position().top;
					var fixNavSubMenuHeight = (contentLayer2_height + subMenu2_top) - contentLayer1_height;
					var g_menuHeight = fixNavSubMenuHeight > 0 ? (contentLayer1_height + fixNavSubMenuHeight) : contentLayer1_height;
					var g_menuWidth = contentLayer1_width + contentLayer2_width;
					$g_menu.css({'width':g_menuWidth+'px', 'height':g_menuHeight+'px'});
					$contentLayer1.css({'width':contentLayer1_contentWidth+'px'});
				}
				//有子菜单的父级
			}else{
				
					if( $(this).parents(".subMenu").length <= 0 ){
						//鼠标进入二级菜单（无下一级菜单）,修正g_menu透明区域位置
						if( option.navBar == true && Fai.isIE() ){
							var $g_menu = $("#g_menu" + option.id);
							var $contentLayer1 = $g_menu.find(".contentLayer1");
							var contentLayer1_contentWidth = $contentLayer1.children(".middle").first().outerWidth(true);
							var g_menuHeight = $contentLayer1.outerHeight(true);
							var g_menuWidth = $contentLayer1.outerWidth(true);
							$g_menu.css({'width':g_menuWidth+'px', 'height':g_menuHeight+'px'});
							$contentLayer1.css({'width':contentLayer1_contentWidth+'px'});
						}
					}
			}
			div2.addClass('itemHover');
			div2.addClass('itemHoverIndex' + (div2.index()+1));
			$(".g_menu").attr("_mouseIn", 1);
		},function(){
			var div2 = $(this);
			var subMenu2 = null;
			$(this).parent().find(' > .subMenu').each(function(){
				if ($(this).attr("itemId") == div2.attr("itemId")){
					subMenu2 = $(this);
				}
			});
			if (subMenu2 != null && subMenu2.length == 1){
				subMenu2.attr("_mouseIn", 0);
				//setTimeout("Fai.hideSubMenu()", 100);
				setTimeout(function(){
					Fai.hideSubMenu();
				}, 100);
			} else {
				div2.removeClass('itemHover');
				div2.removeClass('itemHoverIndex' + (div2.index()+1));
			}
		}).click( function(){
			$(".g_menu").attr("_mouseIn", 0);
			setTimeout("Fai.hideMenu()", 100);
		});

		if (option.closeMode == 1){
			div.mousedown(function(){
				$(".g_menu").attr("_mouseIn", 2);
			});
		}
	}
};

// 把菜单项的宽度置为最宽的那一项
Fai.calcMenuSize = function(menu, option) {
	menu.find(' > .content').each(function(){
		var middle = $(' > .middle', this);
		// items
		var maxWidth = 0;
		if (!Fai.isNull(option.minWidth)){
			maxWidth = option.minWidth - Fai.getCssInt(middle.find('.left').first(), 'width') - Fai.getCssInt(middle.find('.right').first(), 'width');
		}
		var maxOuterWidth = maxWidth;
		var items = middle.find(' > tbody > tr > .center > .item');
		items.each(function(){
			if ($(this).width() > maxWidth) {
				maxOuterWidth = $(this).outerWidth();
				maxWidth = $(this).width();
			}
		});
		items.width(maxOuterWidth);
		items.find(" > tbody > tr > .itemCenter").each(function(){
			var itemCenter = $(this);
			var itemLeft = itemCenter.parent().find(" > .itemLeft");
			var itemRight = itemCenter.parent().find(" > .itemRight");
			itemCenter.css("width", (maxWidth - itemLeft.outerWidth() - itemRight.outerWidth() - itemCenter.outerWidth() + itemCenter.width() ) + "px");
			var aa = itemCenter.find("a");
			aa.css("width", (itemCenter.width() - aa.outerWidth() + aa.width()) + "px");
		});
		$(' > .top', this).width(middle.width());
		$(' > .bottom', this).width(middle.width());
	});
};

Fai.hideSubMenu = function() {
	$(".g_menu .subMenu").each(function(){
		var menu = $(this);
		if (menu.length != 1) {
			return;
		}
		if (menu.attr("_mouseIn") == 1){
			return;
		}
		menu.css("display", "none");
		menu.parent().find(' > .item').each(function(){
			if ($(this).attr("itemId") == menu.attr("itemId")) {
				$(this).removeClass('itemHover');
			}
		});
	});
};

Fai.hideMenu = function() {
	$(".g_menu").each(function(){
		var menu = $(this);
		if (menu.length != 1) {
			return;
		}
		if (menu.attr("_mouseIn") == 1){
			return;
		}
		menu.remove();
	});
};

// 计算一个控件宽度，把控件设置为width
Fai.calcCtrlWidth = function(width, item) {
	padding = Fai.getCssInt(item, 'padding-left') + Fai.getCssInt (item, 'padding-right');
	margin = Fai.getCssInt(item, 'margin-left') + Fai.getCssInt (item, 'margin-right');
	border = Fai.getCssInt(item, 'border-left-width') + Fai.getCssInt (item, 'border-right-width');
	item.width(width - padding - margin - border);
};

// 计算一个控件高度，把控件设置为height
Fai.calcCtrlHeight = function(height, item) {
	padding = Fai.getCssInt(item, 'padding-top') + Fai.getCssInt (item, 'padding-bottom');
	margin = Fai.getCssInt(item, 'margin-top') + Fai.getCssInt (item, 'margin-bottom');
	border = Fai.getCssInt(item, 'border-top-width') + Fai.getCssInt (item, 'border-bottom-width');
	item.height(height - padding - margin - border);
};

// 计算网格（左中右）的大小。center的宽度由width减去left&right，left/right的高度等于center的高度
Fai.calcGridSize = function(width, grid, left, center, right) {
	if (width > 0){
		// calc grid
		var padding = Fai.getCssInt(grid, 'padding-left') + Fai.getCssInt (grid, 'padding-right');
		var margin = Fai.getCssInt(grid, 'margin-left') + Fai.getCssInt (grid, 'margin-right');
		var border = Fai.getCssInt(grid, 'border-left-width') + Fai.getCssInt (grid, 'border-right-width');
		// 不允许grid被撑大
		grid.css('overflow-x', 'hidden');
		grid.width(width - padding - border - margin);
	}
	// calc center
	var leftWidth = 0;
	if (left.css('display') != 'none'){
		padding = Fai.getCssInt(left, 'padding-left') + Fai.getCssInt (left, 'padding-right');
		margin = Fai.getCssInt(left, 'margin-left') + Fai.getCssInt (left, 'margin-right');
		border = Fai.getCssInt(left, 'border-left-width') + Fai.getCssInt (left, 'border-right-width');
		leftWidth = left.width() + padding + margin + border;
	}
	var rightWidth = 0;
	if (right.css('display') != 'none'){
		padding = Fai.getCssInt(right, 'padding-left') + Fai.getCssInt (right, 'padding-right');
		margin = Fai.getCssInt(right, 'margin-left') + Fai.getCssInt (right, 'margin-right');
		border = Fai.getCssInt(right, 'border-left-width') + Fai.getCssInt (right, 'border-right-width');
		rightWidth = right.width() + padding + margin + border;
	}
	Fai.calcCtrlWidth(grid.width() - leftWidth - rightWidth, center);

	padding = Fai.getCssInt(center, 'padding-top') + Fai.getCssInt (center, 'padding-bottom');
	margin = Fai.getCssInt(center, 'margin-top') + Fai.getCssInt (center, 'margin-bottom');
	border = Fai.getCssInt(center, 'border-top-width') + Fai.getCssInt (center, 'border-bottom-width');
	var height = center.height() + padding + margin + border;

	// 通过重置grid的height（grid要设置overflow:hidden），修正ie6在grid设置了padding情况下会自动加额外padding的bug
	// 貌似又没问题了，不需要设置
	//grid.height(height);

	// calc left
	Fai.calcCtrlHeight(height, left);
	// calc right
	Fai.calcCtrlHeight(height, right);
};

Fai.removeBgStyle = function(obj)
{
	//var obj = $("#" + id);
	if(obj.attr("style"))
	{
		style = obj.attr("style").toLowerCase();
		if(style.indexOf("background-image") > -1)		{style = style.replace(/background-image[^;]*/gi, '');}
		if(style.indexOf("background-repeat") > -1)		{style = style.replace(/background-repeat[^;]*/gi, '');}
		if(style.indexOf("background-position") > -1)	{style = style.replace(/background-position[^;]*/gi, '');}
		if(style.indexOf("background-color") > -1)		{style = style.replace(/background-color[^;]*/gi, '');}
		if(style.indexOf("background") > -1)			{style = style.replace(/background[^;]*/gi, '');}
		if(style == "" || style == null){obj.removeAttr("style");}
			else{obj.attr("style", style);}
	}
};

Fai.showTip = function(options){
	//options
	//	1.tid (*) -- jQuery selector
	//	2.content html (*)
	//	3.closeSwitch -- true/false
	//	4.autoTimeout -- it is close timeout
	//	5.showMode -- left,right,top,bottom (*)
	//  6.beforeClose -- function
	//  7.cls -- extend class name
	var frameContent = new Array();
	if(!options.content) {
		options.content = "";
	}
	frameContent.push("<div class='tip-content'>");
	if(options.closeSwitch) {
		frameContent.push("<div class='tip-content'>");
		frameContent.push("<a class='tip-btnClose'></a>");
	} else {
		frameContent.push("<div class='tip-content'>");
	}
	frameContent.push(options.content);
	frameContent.push("</div>");
	var finalContent = frameContent.join('');
	
	var defaultOptions = {
		content: finalContent,
		className: 'tip-yellowsimple',
		showTimeout: 1,
		hideTimeout: 0,
		alignTo: 'target',
		alignX: 'center',
		alignY: 'top',
		offsetY: 5,
		showOn: 'none',
		hideAniDuration: 0,
		id: 'tip-yellowsimple' + parseInt(Math.random()*10000)
	}
	
	if(options.id){
		$.extend( defaultOptions, {id:options.id} );
	}
	
	if(options.showMode){
		if(options.showMode == "left"){
			$.extend( defaultOptions, {alignX: 'left', alignY: 'center', offsetY: 0, offsetX: 5} );
		}else if(options.showMode == "right"){
			$.extend( defaultOptions, {alignX: 'right', alignY: 'center', offsetY: 0, offsetX: 5} );
		}else if(options.showMode == "top"){
			$.extend( defaultOptions, {alignX: 'center', alignY: 'top', offsetY: 0, offsetX: 5} );
		}else if(options.showMode == "bottom"){
			$.extend( defaultOptions, {alignX: 'center', alignY: 'bottom', offsetY: 0, offsetX: 5} );
		}
	}
	
	if(options.data){
		$.extend( defaultOptions, options.data );
	}
	if(options.appendToId){ // 插入位置
		$.extend( defaultOptions, { appendToId: options.appendToId } );
	}
	if (options.autoLocation){	// 动态改变位置
		$.extend( defaultOptions, { autoLocation: options.autoLocation } );
	}
	if (options.cusStyle){	// 自定义属性
		$.extend( defaultOptions, { cusStyle: options.cusStyle } );
	}
	
	var faiTarget = $(options.tid);
	faiTarget.poshytip('destroy');
	faiTarget.poshytip(defaultOptions);	
	faiTarget.poshytip('show');
	if (options.cls) {
		$('#' + defaultOptions.id).addClass(options.cls);
	}
	
	$('#' + defaultOptions.id).find('.tip-btnClose').live('click',function(){
		if(options.beforeClose){
			options.beforeClose();
		}
		Fai.closeTip(options.tid);
	})
	if(options.autoTimeout){
		window.setTimeout( 
			function(){
				if(options.beforeClose){
					options.beforeClose();
				}
				Fai.closeTip(options.tid);
			}, options.autoTimeout );
	}
	
};

Fai.closeTip = function( tid ){
	if (typeof $(tid).poshytip == "function"){
		$(tid).poshytip('destroy');
	}
};

Fai.refreshTip = function( tid ){
	$(tid).poshytip('hide');
	$(tid).poshytip('show');
};

Fai.removeCss = function(ctrl, css) {
	var reg = new RegExp(css + '[^;]*;', "gi");
	var style = ctrl.attr("style").replace(reg, '');
	if(style == "" || style == null){
		ctrl.removeAttr("style");
	} else {
		ctrl.attr("style", style);
	}
};

Fai.rgb2hex = function(rgb) { 
	if (rgb.charAt(0) == '#') 
		return rgb; 
	var n = Number(rgb);
	var ds = rgb.split(/\D+/); 
	var decimal = Number(ds[1]) * 65536 + Number(ds[2]) * 256 + Number(ds[3]); 
	var s = decimal.toString(16); 
	while (s.length < 6) 
		s = "0" + s; 
	return "#" + s; 
};

Fai.int2hex = function(rgb) { 
	var hex = rgb.toString(16);
	while (hex.length < 6) 
		hex = "0" + hex; 
	return "#" + hex;
};

Fai.setCtrlStyleCss = function(sid, id, cls, key, value) {
	var style = $('#' + sid);
	var list = new Array();
	if (style.length == 1) {
		// ie6下会把没换行的也自动加了换行，例如#id .xxx{\n\tDISPLAY: none\n}
		var html = style.html();
		html = html.replace(/{\r\n/g, '{').replace(/\t/g, '').replace(/\r\n}/g, ';}');
		list = html.split('\n');
		style.remove();
	}
	var reg = new RegExp('#' + id + ' +' + fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
	if(id == "" || id == "undefined"){	//没有传ID的情况
		reg = new RegExp(fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
	}
	
	for (var i = list.length - 1; i >= 0; --i) {
		var line = list[i];
		if (line.length == 0 || /^\s$/.test(line) || reg.test(line)) {
			list.splice(i, 1);
		}
	}
	
	if(id == "" || id == "undefined"){	//没有传ID的情况
		list.push(cls + '{' + key + ':' + value + ';}');
	}else{
		list.push('#' + id + ' ' + cls + '{' + key + ':' + value + ';}');
	}
	
	$('head').append('<style type="text/css" id="' + sid + '">' + list.join('\n') + '</style>');
};

Fai.setCtrlStyleCssList = function(sid, id, css, options) {	
	var style = $('#' + sid);
	var list = new Array();
	if (style.length == 1) {
		// ie6下会把没换行的也自动加了换行，例如#id .xxx{\n\tDISPLAY: none\n}
		var html = style.html();
		html = html.replace(/{\r\n/g, '{').replace(/\t/g, '').replace(/\r\n}/g, ';}');
		list = html.split('\n');
		style.remove();
	}
	
	for (var i = list.length - 1; i >= 0; --i) {
		var line = list[i];
		for (var n = 0; n < css.length; ++n){
			var cls = css[n].cls;
			var key = css[n].key;
			var reg = new RegExp('#' + id + ' +' + fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
			
			if(id == "" || id == "undefined"){	//没有传ID的情况
				reg = new RegExp(fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
			}
			
			if (line.length == 0 || /^\s$/.test(line) || reg.test(line)) {
				list.splice(i, 1);
				break;
			}
		}
	}
	for (var n = 0; n < css.length; ++n){
		if(id == "" || id == "undefined"){	//没有传ID的情况
			list.push(css[n].cls + '{' + css[n].key + ':' + css[n].value + ';}');
		}else{
			list.push('#' + id + ' ' + css[n].cls + '{' + css[n].key + ':' + css[n].value + ';}');
		}
		
	}
	
	//默认减减组成新的列表是倒序，覆盖插入的样式是插后面
	if(options && options.rev){
		list.reverse();
	}
	
	$('head').append('<style type="text/css" id="' + sid + '">' + list.join('\n') + '</style>');
};

Fai.getCtrlStyleCss = function(sid, id, cls, key) {
	var style = $('#' + sid);
	if (style.length == 0) {
		return '';
	}
	var list = style.html().split('\n');
	var reg = new RegExp('#' + id + ' +' + fixRegSpecialCharacter(cls) + ' *{ *' + key + '[^;]*;', "gi");
	if(id == "" || id == "undefined"){	//没有传ID的情况
		reg = new RegExp(fixRegSpecialCharacter(cls) + ' *{ *' + key + '[^;]*;', "gi");
	}
	for (var i = list.length - 1; i >= 0; --i) {
		var line = list[i];
		var matchs = line.match(reg);
		if (matchs && matchs.length >= 2) {
			return matchs[1];
		}
	}
	return '';
};

Fai.removeCtrlStyleCss = function(sid, id, cls, key) {
	var style = $('#' + sid);
	var list = new Array();
	if (style.length == 1) {
		// ie6下会把没换行的也自动加了换行，例如#id .xxx{\n\tDISPLAY: none\n}
		var html = style.html();
		html = html.replace(/{\r\n/g, '{').replace(/\t/g, '').replace(/\r\n}/g, ';}');
		list = html.split('\n');
		style.remove();
	}
	var reg = new RegExp('#' + id + ' +' + fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
	if(id == "" || id == "undefined"){	//没有传ID的情况
		reg = new RegExp(fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
	}
	for (var i = list.length - 1; i >= 0; --i) {
		var line = list[i];
		if (line.length == 0 || /^\s$/.test(line) || reg.test(line)) {
			list.splice(i, 1);
		}
	}
	$('head').append('<style type="text/css" id="' + sid + '">' + list.join('\n') + '</style>');
	
};

Fai.removeCtrlStyleCssList = function(sid, id, css) {
	var style = $('#' + sid);
	var list = new Array();
	if (style.length == 1) {
		// ie6下会把没换行的也自动加了换行，例如#id .xxx{\n\tDISPLAY: none\n}
		var html = style.html();
		html = html.replace(/{\r\n/g, '{').replace(/\t/g, '').replace(/\r\n}/g, ';}');
		list = html.split('\n');
		style.remove();
	}
	for (var i = list.length - 1; i >= 0; --i) {
		var line = list[i];
		for (var n = 0; n < css.length; ++n){
			var cls = css[n].cls;
			var key = css[n].key;
			var reg = new RegExp('#' + id + ' +' + fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
			if(id == "" || id == "undefined"){	//没有传ID的情况
				reg = new RegExp(fixRegSpecialCharacter(cls) + ' *{ *' + key + '\s*:[^;]*;', "gi");
			}
			if (line.length == 0 || /^\s$/.test(line) || reg.test(line)) {
				list.splice(i, 1);
				break;
			}
		}
	}
	$('head').append('<style type="text/css" id="' + sid + '">' + list.join('\n') + '</style>');
};

//	正则匹配中，特殊字符 * . ?  $ ^ [ ] ( ) { } | \ /
//	针对以上特殊字符，进行相应转义处理
function fixRegSpecialCharacter(cls){
	var specialCharacter = ["\\",".","?","$","*","^","[","]","{","}","|","(",")","/"];

	for(var i = 0, len = specialCharacter.length; i < len; i++){
		cls = cls.replace(specialCharacter[i], "\\"+specialCharacter[i]);
	}

	return cls;
}


Fai.addCtrlStyle = function(sid, styles) {
	var style = $('#' + sid);
	var list = new Array();
	if (style.length == 1) {
		// ie6下会把没换行的也自动加了换行，例如#id .xxx{\n\tDISPLAY: none\n}
		var html = style.html();
		html = html.replace(/{\r\n/g, '{').replace(/\t/g, '').replace(/\r\n}/g, ';}');
		list = html.split('\n');
		style.remove();
	}
	list.push(styles);
	
	$('head').append('<style type="text/css" id="' + sid + '">' + list.join('\n') + '</style>');
};

Fai.addBookmark = function(title,url) {
	/*
	try {
		if (window.sidebar) { 			//ff27.0.1 为{}，没有addPanel方法
			window.sidebar.addPanel(title, url,""); 
		} else if( document.all ) {		//ie11 为undefined   window.external为{}  但是可以调用AddFavorite
			window.external.AddFavorite( url, title);
		}else{
			alert("收藏网站失败，请使用Ctrl+D进行添加1");
		}
	} catch (e) {
		alert("收藏网站失败，请使用Ctrl+D进行添加2");
	}
	*/
	try {
		try{
			window.sidebar.addPanel(title, url, "");
		} catch (e){
			window.external.AddFavorite( url, title);
		}
	} catch (e1) {
		//if( Fai.isChrome() ){
			alert("收藏网站失败，请使用Ctrl+D进行添加。");
		//}
	}
};

Fai.setHomePage = function(url) {
	if( typeof(url) == 'undefined' ){ url = location.protocol + '//' + location.host; }
	var comm2use = false; // 非管理，非网站使用时
	if( typeof LS == 'undefined' || typeof LS.setHomePageSuccess == 'undefined' ){
		comm2use = true;
	}
	if( $.browser.msie ) {
		try{
			document.body.style.behavior = "url(#default#homepage)";
			document.body.setHomePage(url);
		} catch (e) {
			var str_shpns = '您的浏览器暂时不支持自动设为首页，请手动添加。';
			if( !comm2use ){ str_shpe = LS.setHomePageNotSupport; }
			alert( str_shpns );
		}
	}else if( $.browser.mozilla ) {
		try{
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
			Components.classes['@mozilla.org/preferences-service;1'].getService(Components. interfaces.nsIPrefBranch).setCharPref('browser.startup.homepage',url);
			var str_shps = '添加首页成功。';
			if( !comm2use ){ str_shps = LS.setHomePageSuccess; }
			alert( str_shps );
		} catch (e) {
			var str_shpe = '设置失败，请手动添加。';
			if( !comm2use ){ str_shpe = LS.setHomePageError; }
			alert( str_shpe );
		}
		//alert( "该操作被浏览器拒绝，如果想启用该功能，请在地址栏内输入 about:config,然后将项 signed.applets.codebase_principal_support 值该为true" );
	}else{
		var str_shpns = '您的浏览器暂时不支持自动设为首页，请手动添加。';
		if( !comm2use ){ str_shpns = LS.setHomePageNotSupport; }
		alert( str_shpns );
	}
};

/*
**单个TextArea 限制长度
**@Time 2011-08-11
**
**用法：
**1、加载时执行，传入TextArea的ID和最大字符值
*/

Fai.singleTextAreaAddMaxLength = function(objId,maxLength){
	if(typeof objId != "undefined" && typeof maxLength != "undefined"){
		var textAreaObj = $("#" + objId);		//获取textArea对象
		textAreaObj.attr("maxlength",maxLength);
		textAreaObj.bind('keydown keyup change blur',function(){
			textAreaObjVal = textAreaObj.val();
			textAreaObjLength = textAreaObjVal.length;
			if(textAreaObjLength > maxLength){
				var tmp = textAreaObjVal.substr(0,maxLength);
				textAreaObj.val(tmp);
			}
		});
	}
};


/**
 * 文件单位转换(B)转成(KB/MB)
 */
Fai.parseFileSize = function(bit){
	
	if(typeof bit != "undefined" && typeof bit == "number"){
		var newFileSize;
		if(bit < 1024){
			newFileSize = bit + "B";
		}else if(bit < 1024*1024){
			var tmpSize = bit/1024;
			//alert(tmpSize);
			newFileSize = tmpSize.toFixed(2) + "KB";
		}else{
			var tmpSize = bit/(1024*1024);
			newFileSize = tmpSize.toFixed(2) + "MB";
		}
		
		return newFileSize;
	}else{
		return "-";
	}
};



/**
 * compareObj
 * function: compareObj
 * 
 * @param Fai.compareObj(firstVal, secondVal, desc);
 */
Fai.compareObj = function(firstVal, secondVal, desc) {
	if (firstVal === '') {
		if (secondVal === '') {
			return 0;
		}
		return 1;
	}
	if (secondVal === '') {
		return -1;
	}
	if (!isNaN(firstVal)) { // isNumber
		if (!isNaN(secondVal)) { // isNumber
			firstVal = Math.floor(firstVal);
			secondVal = Math.floor(secondVal);
		} else {
			//a is num , b not num
			if(desc){
				return 1;
			} else {
				return -1;
			}
		}
	} else {
		if (!isNaN(secondVal)) {
			//a not num, b is num
			if(desc){
				return -1;
			} else {
				return 1;
			}
		}
	}

	if(firstVal > secondVal){
		if(desc){
			return -1;
		}else{
			return 1;
		}
	} else if (firstVal == secondVal) {
		return 0;
	} else {
		if(desc){
			return 1;
		}else{
			return -1;
		}
	}
};

//2014.11.26
//获取浏览器滚动条的宽度
//不同操作系统,不同浏览器下,滚动条的宽度是不相同的.
Fai.getScrollWidth = function(){
	var w;
	var tmp = document.createElement("div");

		tmp.style.cssText = "overflow:scroll; width:100px; height:100px; background:transparent;";
		document.body.appendChild(tmp);
		w = tmp.offsetWidth - tmp.clientWidth; //得到滚动条宽度
		document.body.removeChild(tmp);

	return w;
};

/**
 * 浮点数精度计算
 * 使用场景：对数值比较敏感的应用
 * @param type 0:加  1:减  2:乘  3:除
 * 问题例子：
 *  2.2 + 2.1 = 4.300000000000001 
 *	2.2 - 1.9 = 0.30000000000000027 
 *	2.2 * 2.2 = 4.840000000000001 
 *	2.1 / 0.3 = 7.000000000000001
 */
Fai.calculate = function(num1, num2, type){
	var result = 0, baseNum1 = 0, baseNum2 = 0;
	try { 
		baseNum1 = num1.toString().split(".")[1].length; 
	} catch (e) { 
		baseNum1 = 0; 
	} 
	try { 
		baseNum2 = num2.toString().split(".")[1].length; 
	} catch (e) { 
		baseNum2 = 0; 
	}
	 
	switch(type){
		case 0: //加
			var baseNum = Math.pow(10, Math.max(baseNum1, baseNum2)); 
			result = (num1 * baseNum + num2 * baseNum) / baseNum; 
			break;
		case 1: //减
			var precision;//精度
			var baseNum = Math.pow(10, Math.max(baseNum1, baseNum2)); 
			precision = (baseNum1 >= baseNum2) ? baseNum1 : baseNum2; 
			result = ((num1 * baseNum - num2 * baseNum) / baseNum).toFixed(precision);
			break;
		case 2: //乘
			result =  _numMulti(num1, num2); 
			break;
		case 3: //除
			//不使用精确乘操作 2.1/3 会存在问题
			result = _numMulti((Number(num1.toString().replace(".", "")) / Number(num2.toString().replace(".", ""))) , Math.pow(10, baseNum2 - baseNum1)); 
			break;
	}
	return result;

	function _numMulti(one, two){
		var tmp = 0;
		try { 
			tmp += one.toString().split(".")[1].length; 
		} catch (e) { 
		} 
		try { 
			tmp += two.toString().split(".")[1].length; 
		} catch (e) { 
		}
		return 	Number(one.toString().replace(".", "")) * Number(two.toString().replace(".", "")) / Math.pow(10, tmp);
	}
};

/***************************** End compareObj *****************************/

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//jQuery Utils
//////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Create a cookie with the given key and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 */

/**
 * Get the value of a cookie with the given key.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 */
 
;(function($) {
	$.cookie = function (key, value, options) {
	
		// key and value given, set cookie...
		if (arguments.length > 1 && (value === null || typeof value !== "object")) {
			options = $.extend({}, options);
	
			if (value === null) {
				options.expires = -1;
			}
	
			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}
	
			return (document.cookie = [
				encodeURIComponent(key), '=',
				options.raw ? String(value) : encodeURIComponent(String(value)),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path ? '; path=' + options.path : '',
				options.domain ? '; domain=' + options.domain : '',
				options.secure ? '; secure' : ''
			].join(''));
		}
	
		// key and possibly options given, get cookie...
		options = value || {};
		var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
	};

})(jQuery);

///*
// * jQuery JSON Plugin
// * version: 2.1 (2009-08-14)
// *
// */
// 
//;(function($) {
//    /** jQuery.toJSON( json-serializble )
//        Converts the given argument into a JSON respresentation.
//
//        If an object has a "toJSON" function, that will be used to get the representation.
//        Non-integer/string keys are skipped in the object, as are keys that point to a function.
//
//        json-serializble:
//            The *thing* to be converted.
//     **/
//    $.toJSON = function(o)
//    {
////        if (typeof(JSON) == 'object' && JSON.stringify)
// //           return JSON.stringify(o);
//        
//        var type = typeof(o);
//    
//        if (o === null)
//            return "null";
//    
//        if (type == "undefined")
//            return undefined;
//        
//        if (type == "number" || type == "boolean")
//            return o + "";
//    
//        if (type == "string")
//            return $.quoteString(o);
//    
//        if (type == 'object')
//        {
//            if (typeof o.toJSON == "function") 
//                return $.toJSON( o.toJSON() );
//            
//            if (o.constructor === Date)
//            {
//                var month = o.getUTCMonth() + 1;
//                if (month < 10) month = '0' + month;
//
//                var day = o.getUTCDate();
//                if (day < 10) day = '0' + day;
//
//                var year = o.getUTCFullYear();
//                
//                var hours = o.getUTCHours();
//                if (hours < 10) hours = '0' + hours;
//                
//                var minutes = o.getUTCMinutes();
//                if (minutes < 10) minutes = '0' + minutes;
//                
//                var seconds = o.getUTCSeconds();
//                if (seconds < 10) seconds = '0' + seconds;
//                
//                var milli = o.getUTCMilliseconds();
//                if (milli < 100) milli = '0' + milli;
//                if (milli < 10) milli = '0' + milli;
//
//                return '"' + year + '-' + month + '-' + day + 'T' +
//                             hours + ':' + minutes + ':' + seconds + 
//                             '.' + milli + 'Z"'; 
//            }
//
//            if (o.constructor === Array) 
//            {
//                var ret = [];
//                for (var i = 0; i < o.length; i++)
//                    ret.push( $.toJSON(o[i]) || "null" );
//
//                return "[" + ret.join(",") + "]";
//            }
//        
//            var pairs = [];
//            for (var k in o) {
//                var name;
//                var type = typeof k;
//
//                if (type == "number")
//                    name = '"' + k + '"';
//                else if (type == "string")
//                    name = $.quoteString(k);
//                else
//                    continue;  //skip non-string or number keys
//            
//                if (typeof o[k] == "function") 
//                    continue;  //skip pairs where the value is a function.
//            
//                var val = $.toJSON(o[k]);
//            
//                pairs.push(name + ":" + val);
//            }
//
//            return "{" + pairs.join(", ") + "}";
//        }
//    };
//
//    /** jQuery.evalJSON(src)
//        Evaluates a given piece of json source.
//     **/
//    $.evalJSON = function(src)
//    {
//        if (typeof(JSON) == 'object' && JSON.parse)
//            return JSON.parse(src);
//        return eval("(" + src + ")");
//    };
//    
//    /** jQuery.secureEvalJSON(src)
//        Evals JSON in a way that is *more* secure.
//    **/
//    $.secureEvalJSON = function(src)
//    {
//        if (typeof(JSON) == 'object' && JSON.parse)
//            return JSON.parse(src);
//        
//        var filtered = src;
//        filtered = filtered.replace(/\\["\\\/bfnrtu]/g, '@');
//        filtered = filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
//        filtered = filtered.replace(/(?:^|:|,)(?:\s*\[)+/g, '');
//        
//        if (/^[\],:{}\s]*$/.test(filtered))
//            return eval("(" + src + ")");
//        else
//            throw new SyntaxError("Error parsing JSON, source is not valid.");
//    };
//
//    /** jQuery.quoteString(string)
//        Returns a string-repr of a string, escaping quotes intelligently.  
//        Mostly a support function for toJSON.
//    
//        Examples:
//            >>> jQuery.quoteString("apple")
//            "apple"
//        
//            >>> jQuery.quoteString('"Where are we going?", she asked.')
//            "\"Where are we going?\", she asked."
//     **/
//    $.quoteString = function(string)
//    {
//        if (string.match(_escapeable))
//        {
//            return '"' + string.replace(_escapeable, function (a) 
//            {
//                var c = _meta[a];
//                if (typeof c === 'string') return c;
//                c = a.charCodeAt();
//                return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
//            }) + '"';
//        }
//        return '"' + string + '"';
//    };
//    
//    var _escapeable = /["\\\x00-\x1f\x7f-\x9f]/g;
//    
//    var _meta = {
//        '\b': '\\b',
//        '\t': '\\t',
//        '\n': '\\n',
//        '\f': '\\f',
//        '\r': '\\r',
//        '"' : '\\"',
//        '\\': '\\\\'
//    };
//})(jQuery);


/**
 * jQuery JSON Plugin
 * Time 2011-09-17 version: 2.3
 *
 * Usage:
 * Serializes a javascript object, number, string, or array into JSON. 
 *     $.toJSON()
 * Converts from JSON to Javascript, quickly, and is trivial.
 *     $.evalJSON()
 * Converts from JSON to Javascript, but does so while checking to see if the source is actually JSON, and not with other Javascript statements thrown in. 
 *     $.secureEvalJSON()
 * Places quotes around a string, and intelligently escapes any quote, backslash, or control characters. 
 *     $.quoteString()
 */
(function ($) {
	'use strict';

	var escape = /["\\\x00-\x1f\x7f-\x9f]/g,
		meta = {
			'\b': '\\b',
			'\t': '\\t',
			'\n': '\\n',
			'\f': '\\f',
			'\r': '\\r',
			'"' : '\\"',
			'\\': '\\\\'
		},
		hasOwn = Object.prototype.hasOwnProperty;

	/**
	 * jQuery.toJSON
	 * Converts the given argument into a JSON representation.
	 *
	 * @param o {Mixed} The json-serializable *thing* to be converted
	 *
	 * If an object has a toJSON prototype, that will be used to get the representation.
	 * Non-integer/string keys are skipped in the object, as are keys that point to a
	 * function.
	 *
	 */
	//$.toJSON = typeof JSON === 'object' && JSON.stringify ? JSON.stringify : function (o) {
	$.toJSON = function (o) {
		if (o === null) {
			return 'null';
		}

		var pairs, k, name, val,
			type = $.type(o);

		if (type === 'undefined') {
			return undefined;
		}

		// Also covers instantiated Number and Boolean objects,
		// which are typeof 'object' but thanks to $.type, we
		// catch them here. I don't know whether it is right
		// or wrong that instantiated primitives are not
		// exported to JSON as an {"object":..}.
		// We choose this path because that's what the browsers did.
		if (type === 'number' || type === 'boolean') {
			return String(o);
		}
		if (type === 'string') {
			return $.quoteString(o);
		}
		if (typeof o.toJSON === 'function') {
			return $.toJSON(o.toJSON());
		}
		if (type === 'date') {
			var month = o.getUTCMonth() + 1,
				day = o.getUTCDate(),
				year = o.getUTCFullYear(),
				hours = o.getUTCHours(),
				minutes = o.getUTCMinutes(),
				seconds = o.getUTCSeconds(),
				milli = o.getUTCMilliseconds();

			if (month < 10) {
				month = '0' + month;
			}
			if (day < 10) {
				day = '0' + day;
			}
			if (hours < 10) {
				hours = '0' + hours;
			}
			if (minutes < 10) {
				minutes = '0' + minutes;
			}
			if (seconds < 10) {
				seconds = '0' + seconds;
			}
			if (milli < 100) {
				milli = '0' + milli;
			}
			if (milli < 10) {
				milli = '0' + milli;
			}
			return '"' + year + '-' + month + '-' + day + 'T' +
				hours + ':' + minutes + ':' + seconds +
				'.' + milli + 'Z"';
		}

		pairs = [];

		if ($.isArray(o)) {
			for (k = 0; k < o.length; k++) {
				pairs.push($.toJSON(o[k]) || 'null');
			}
			return '[' + pairs.join(',') + ']';
		}

		// Any other object (plain object, RegExp, ..)
		// Need to do typeof instead of $.type, because we also
		// want to catch non-plain objects.
		if (typeof o === 'object') {
			for (k in o) {
				// Only include own properties,
				// Filter out inherited prototypes
				if (hasOwn.call(o, k)) {
					// Keys must be numerical or string. Skip others
					type = typeof k;
					if (type === 'number') {
						name = '"' + k + '"';
					} else if (type === 'string') {
						name = $.quoteString(k);
					} else {
						continue;
					}
					type = typeof o[k];

					// Invalid values like these return undefined
					// from toJSON, however those object members
					// shouldn't be included in the JSON string at all.
					if (type !== 'function' && type !== 'undefined') {
						val = $.toJSON(o[k]);
						pairs.push(name + ':' + val);
					}
				}
			}
			return '{' + pairs.join(',') + '}';
		}
	};

	/**
	 * jQuery.evalJSON
	 * Evaluates a given json string.
	 *
	 * @param str {String}
	 */
	$.evalJSON = typeof JSON === 'object' && JSON.parse ? JSON.parse : function (str) {
		/*jshint evil: true */
		return Fai.fkEval('(' + str + ')');
	};

	/**
	 * jQuery.secureEvalJSON
	 * Evals JSON in a way that is *more* secure.
	 *
	 * @param str {String}
	 */
	$.secureEvalJSON = typeof JSON === 'object' && JSON.parse ? JSON.parse : function (str) {
		var filtered =
			str
			.replace(/\\["\\\/bfnrtu]/g, '@')
			.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
			.replace(/(?:^|:|,)(?:\s*\[)+/g, '');

		if (/^[\],:{}\s]*$/.test(filtered)) {
			/*jshint evil: true */
			return Fai.fkEval('(' + str + ')');
		}
		throw new SyntaxError('Error parsing JSON, source is not valid.');
	};

	/**
	 * jQuery.quoteString
	 * Returns a string-repr of a string, escaping quotes intelligently.
	 * Mostly a support function for toJSON.
	 * Examples:
	 * >>> jQuery.quoteString('apple')
	 * "apple"
	 *
	 * >>> jQuery.quoteString('"Where are we going?", she asked.')
	 * "\"Where are we going?\", she asked."
	 */
	$.quoteString = function (str) {
		if (str.match(escape)) {
			return '"' + str.replace(escape, function (a) {
				var c = meta[a];
				if (typeof c === 'string') {
					return c;
				}
				c = a.charCodeAt();
				return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
			}) + '"';
		}
		return '"' + str + '"';
	};

}(jQuery));



/**
 * jquery.dateFormat
 * v: 1.0
 */
(function (jQuery) {
		
		var daysInWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var shortMonthsInYear = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var longMonthsInYear = ["January", "February", "March", "April", "May", "June", 
														"July", "August", "September", "October", "November", "December"];
		var shortMonthsToNumber = [];
		shortMonthsToNumber["Jan"] = "01";
		shortMonthsToNumber["Feb"] = "02";
		shortMonthsToNumber["Mar"] = "03";
		shortMonthsToNumber["Apr"] = "04";
		shortMonthsToNumber["May"] = "05";
		shortMonthsToNumber["Jun"] = "06";
		shortMonthsToNumber["Jul"] = "07";
		shortMonthsToNumber["Aug"] = "08";
		shortMonthsToNumber["Sep"] = "09";
		shortMonthsToNumber["Oct"] = "10";
		shortMonthsToNumber["Nov"] = "11";
		shortMonthsToNumber["Dec"] = "12";
	
    jQuery.format = (function () {
        function strDay(value) {
 						return daysInWeek[parseInt(value, 10)] || value;
        }

        function strMonth(value) {
						var monthArrayIndex = parseInt(value, 10) - 1;
 						return shortMonthsInYear[monthArrayIndex] || value;
        }

        function strLongMonth(value) {
					var monthArrayIndex = parseInt(value, 10) - 1;
					return longMonthsInYear[monthArrayIndex] || value;					
        }

        var parseMonth = function (value) {
					return shortMonthsToNumber[value] || value;
        };

        var parseTime = function (value) {
                var retValue = value;
                var millis = "";
                if (retValue.indexOf(".") !== -1) {
                    var delimited = retValue.split('.');
                    retValue = delimited[0];
                    millis = delimited[1];
                }

                var values3 = retValue.split(":");

                if (values3.length === 3) {
                    hour = values3[0];
                    minute = values3[1];
                    second = values3[2];

                    return {
                        time: retValue,
                        hour: hour,
                        minute: minute,
                        second: second,
                        millis: millis
                    };
                } else {
                    return {
                        time: "",
                        hour: "",
                        minute: "",
                        second: "",
                        millis: ""
                    };
                }
            };

        return {
            date: function (value, format) {
                /* 
					value = new java.util.Date()
                 	2009-12-18 10:54:50.546 
				*/
                try {
                    var date = null;
                    var year = null;
                    var month = null;
                    var dayOfMonth = null;
                    var dayOfWeek = null;
                    var time = null;
                    if (typeof value.getFullYear === "function") {
                        year = value.getFullYear();
                        month = value.getMonth() + 1;
                        dayOfMonth = value.getDate();
                        dayOfWeek = value.getDay();
                        time = parseTime(value.toTimeString());
										} else if (value.search(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.?\d{0,3}[-+]?\d{2}:?\d{2}/) != -1) { /* 2009-04-19T16:11:05+02:00 */											
                        var values = value.split(/[T\+-]/);
                        year = values[0];
                        month = values[1];
                        dayOfMonth = values[2];
                        time = parseTime(values[3].split(".")[0]);
                        date = new Date(year, month - 1, dayOfMonth);
                        dayOfWeek = date.getDay();
                    } else {
                        var values = value.split(" ");
                        switch (values.length) {
                        case 6:
                            /* Wed Jan 13 10:43:41 CET 2010 */
                            year = values[5];
                            month = parseMonth(values[1]);
                            dayOfMonth = values[2];
                            time = parseTime(values[3]);
                            date = new Date(year, month - 1, dayOfMonth);
                            dayOfWeek = date.getDay();
                            break;
                        case 2:
                            /* 2009-12-18 10:54:50.546 */
                            var values2 = values[0].split("-");
                            year = values2[0];
                            month = values2[1];
                            dayOfMonth = values2[2];
                            time = parseTime(values[1]);
                            date = new Date(year, month - 1, dayOfMonth);
                            dayOfWeek = date.getDay();
                            break;
                        case 7:
                            /* Tue Mar 01 2011 12:01:42 GMT-0800 (PST) */
                        case 9:
                            /*added by Larry, for Fri Apr 08 2011 00:00:00 GMT+0800 (China Standard Time) */
                        case 10:
                            /* added by Larry, for Fri Apr 08 2011 00:00:00 GMT+0200 (W. Europe Daylight Time) */
                            year = values[3];
                            month = parseMonth(values[1]);
                            dayOfMonth = values[2];
                            time = parseTime(values[4]);
                            date = new Date(year, month - 1, dayOfMonth);
                            dayOfWeek = date.getDay();
                            break;
                        default:
                            return value;
                        }
                    }

                    var pattern = "";
                    var retValue = "";
                    /*
						Issue 1 - variable scope issue in format.date 
                    	Thanks jakemonO
					*/
                    for (var i = 0; i < format.length; i++) {
                        var currentPattern = format.charAt(i);
                        pattern += currentPattern;
                        switch (pattern) {
                        case "ddd":
                            retValue += strDay(dayOfWeek);
                            pattern = "";
                            break;
                        case "dd":
                            if (format.charAt(i + 1) == "d") {
                                break;
                            }
                            if (String(dayOfMonth).length === 1) {
                                dayOfMonth = '0' + dayOfMonth;
                            }
                            retValue += dayOfMonth;
                            pattern = "";
                            break;
                        case "MMMM":
                            retValue += strLongMonth(month);
                            pattern = "";
                            break;
                        case "MMM":
                            if (format.charAt(i + 1) === "M") {
                                break;
                            }
                            retValue += strMonth(month);
                            pattern = "";
                            break;
                        case "MM":
                            if (format.charAt(i + 1) == "M") {
                                break;
                            }
                            if (String(month).length === 1) {
                                month = '0' + month;
                            }
                            retValue += month;
                            pattern = "";
                            break;
                        case "yyyy":
                            retValue += year;
                            pattern = "";
                            break;
                        case "yy":
                            if (format.charAt(i + 1) == "y" &&
                           	format.charAt(i + 2) == "y") {
                            	break;
                      	    }
                            retValue += String(year).slice(-2);
                            pattern = "";
                            break;
                        case "HH":
                            retValue += time.hour;
                            pattern = "";
                            break;
                        case "hh":
                            /* time.hour is "00" as string == is used instead of === */
                            var hour = (time.hour == 0 ? 12 : time.hour < 13 ? time.hour : time.hour - 12);
                            hour = String(hour).length == 1 ? '0'+hour : hour;
                            retValue += hour;
                            pattern = "";
                            break;
												case "h":
												    if (format.charAt(i + 1) == "h") {
												        break;
												    }
												    var hour = (time.hour == 0 ? 12 : time.hour < 13 ? time.hour : time.hour - 12);                           
												    retValue += hour;
												    pattern = "";
												    break;
                        case "mm":
                            retValue += time.minute;
                            pattern = "";
                            break;
                        case "ss":
                            /* ensure only seconds are added to the return string */
                            retValue += time.second.substring(0, 2);
                            pattern = "";
                            break;
                        case "SSS":
                            retValue += time.millis.substring(0, 3);
                            pattern = "";
                            break;
                        case "a":
                            retValue += time.hour >= 12 ? "PM" : "AM";
                            pattern = "";
                            break;
                        case " ":
                            retValue += currentPattern;
                            pattern = "";
                            break;
                        case "/":
                            retValue += currentPattern;
                            pattern = "";
                            break;
                        case ":":
                            retValue += currentPattern;
                            pattern = "";
                            break;
                        default:
                            if (pattern.length === 2 && pattern.indexOf("y") !== 0 && pattern != "SS") {
                                retValue += pattern.substring(0, 1);
                                pattern = pattern.substring(1, 2);
                            } else if ((pattern.length === 3 && pattern.indexOf("yyy") === -1)) {
                                pattern = "";
                            }
                        }
                    }
                    return retValue;
                } catch (e) {
                    return value;
                }
            }
        };
    }());
}(jQuery));

/*
 * jQuery Lazy loading images
 *
 * Version:  1.7.0
 * e.g:
 *    <img class="lazy" src="img/grey.gif" data-original="img/example.jpg"  width="640" heigh="480">
 *    $("img.lazy").lazyload();
 */
(function($, window) {
    
    $window = $(window);
    
    $.fn.lazyload = function(options) {
        var settings = {
            threshold       : 0,
            failure_limit   : 0,
            event           : "scroll",
            effect          : "show",
            container       : window,
            data_attribute  : "original",
            skip_invisible  : true,
            appear          : null,
            load            : null,
			lazyRemoveclass	: ""
        };
		
        if(options) {
            /* Maintain BC for a couple of version. */
            if (undefined !== options.failurelimit) {
                options.failure_limit = options.failurelimit; 
                delete options.failurelimit;
            }
            if (undefined !== options.effectspeed) {
                options.effect_speed = options.effectspeed; 
                delete options.effectspeed;
            }
            
            $.extend(settings, options);
        }        

        /* Fire one scroll event per scroll. Not one scroll event per image. */
        var elements = this;
		if (0 == settings.event.indexOf("scroll")) {
			//$(settings.container).unbind(settings.event).bind(settings.event, function(event) {
			$(settings.container).off("scroll.lazy");
			$(settings.container).on("scroll.lazy", function(event) {
                var counter = 0;
                elements.each(function() {
                    $this = $(this);
					if (settings.skip_invisible && !$this.is(":visible")) return;
					/*
					//暂时先注释，因没有考虑到mobi这边情况，导致图片一直loading
					//already load no check
					if (!$($this).hasClass("loadingPlaceholderBackground")) return;
					*/
					if ($.abovethetop(this, settings) ||
                        $.leftofbegin(this, settings)) {
                            /* Nothing. */
                    } else if (!$.belowthefold(this, settings) &&
                        !$.rightoffold(this, settings)) {
                            $this.trigger("appear");
                    } else {
						if (++counter > settings.failure_limit) {
							//alert( $this.attr("data-original") )
                           // return false;
                           /*if($this.attr("data-original"))
							 return false;*/
                        }
                    }
                });
            });
        }
                
        this.each(function() {
            var self = this;
            var $self = $(self);
            
            self.loaded = false;
            
            /* When appear is triggered load original image. */
            $self.one("appear", function() {
                if (!this.loaded) {
                    if (settings.appear) {
                        var elements_left = elements.length;
                        settings.appear.call(self, elements_left, settings);
                    }
                    $("<img />")
                        .bind("load", function() {
                            $self
                                .hide()
								.removeClass( settings.lazyRemoveclass )
                                .attr("src", $self.data(settings.data_attribute))
                                [settings.effect](settings.effect_speed);
                            self.loaded = true;
                            
                            /* Remove image from array so it is not looped next time. */
                            var temp = $.grep(elements, function(element) {
                                return !element.loaded;
                            });
                            elements = $(temp);
                            
                            if (settings.load) {
                                var elements_left = elements.length;
                                settings.load.call(self, elements_left, settings);
                            }
                        })
                        .attr("src", $self.data(settings.data_attribute)).removeClass( settings.lazyRemoveclass );
                };                
            });

            /* When wanted event is triggered load original image */
            /* by triggering appear.                              */
            if (0 != settings.event.indexOf("scroll")) {
				$self.bind(settings.event, function(event) {
                    if (!self.loaded) {
						$self.trigger("appear");
                    }
                });
            }
        });
        
        /* Check if something appears when window is resized. */
        $window.bind("resize", function(event) {
            $(settings.container).trigger(settings.event);
        });
        
        /* Force initial check if images should appear. */
        $(settings.container).trigger(settings.event);
        
        return this;

    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

    $.belowthefold = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $window.height() + $window.scrollTop();
        } else {
            var fold = $(settings.container).offset().top + $(settings.container).height();
        }
        return fold <= $(element).offset().top - settings.threshold;
    };
    
    $.rightoffold = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $window.width() + $window.scrollLeft();
        } else {
            var fold = $(settings.container).offset().left + $(settings.container).width();
        }
        return fold <= $(element).offset().left - settings.threshold;
    };
        
    $.abovethetop = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $window.scrollTop();
        } else {
            var fold = $(settings.container).offset().top;
        }
        return fold >= $(element).offset().top + settings.threshold  + $(element).height();
    };
    
    $.leftofbegin = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $window.scrollLeft();
        } else {
            var fold = $(settings.container).offset().left;
        }
        return fold >= $(element).offset().left + settings.threshold + $(element).width();
    };

    $.inviewport = function(element, settings) {
         return !$.rightofscreen(element, settings) && !$.leftofscreen(element, settings) && 
                !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
     };

    /* Custom selectors for your convenience.   */
    /* Use as $("img:below-the-fold").something() */

    $.extend($.expr[':'], {
        "below-the-fold" : function(a) { return $.belowthefold(a, {threshold : 0, container: window}) },
        "above-the-top"  : function(a) { return !$.belowthefold(a, {threshold : 0, container: window}) },
        "right-of-screen": function(a) { return $.rightoffold(a, {threshold : 0, container: window}) },
        "left-of-screen" : function(a) { return !$.rightoffold(a, {threshold : 0, container: window}) },
        "in-viewport"    : function(a) { return !$.inviewport(a, {threshold : 0, container: window}) },
        /* Maintain BC for couple of versions. */
        "above-the-fold" : function(a) { return !$.belowthefold(a, {threshold : 0, container: window}) },
        "right-of-fold"  : function(a) { return $.rightoffold(a, {threshold : 0, container: window}) },
        "left-of-fold"   : function(a) { return !$.rightoffold(a, {threshold : 0, container: window}) }
    });
    
})(jQuery, window);

/**
 * Leyewen jQuery flag
 * use Binary to represent 32bit true/false
 * Time: 20120313 version: 1.02
 * 
 * key: enter key(number only && key>0), options: flag get/set, flag number start from 0~31;
 * @usage:
 * 		get: $.flag(key, optionsNumber);			@get optionsNumber's status from key, return true/false
 *		set: $.flag(key, optionsNumber, value);		@set optionsNumber's status to key, return new key
 * 		set: $.flag(key, options);					@set options(e.g {0:true, 1:false, 5:true}) to key, return new key
 * 		set: $.flag(key, options);					@set options(e.g {"true":[0, 1, 2], "false":[3]}) to key, return new key
**/

(function($){
	$.flag = function(key, options, value){
		if(typeof key != "number"){
			return null;
		}
		
		var settings = {
			key			:	0,
			digit		:	32,
			options		:	'',
			setBoolean	:	''
		};
		
		if(options || options === 0) {
			$.extend(settings, {key:key, options:options, setBoolean:value});
			
			if(typeof options === "number") {
				if(typeof settings.setBoolean != "boolean"){
					if(settings.options>=0 && settings.options<settings.digit) {
						var checkFlag = 0x1 << settings.options;
						return (settings.key&checkFlag) == checkFlag;
					} else {
						return false;
					}
				} else {
					if(settings.options>=0 && settings.options<settings.digit) {
						var checkFlag = 0x1 << settings.options;
						if(settings.setBoolean) {
							key |= checkFlag;
						} else {
							key &= ~checkFlag;
						}
					}
					return key;
				}
			} else if(typeof options === "object") {
				var ot = options["true"];
				var of = options["false"];
				if((typeof ot != "undefined" || typeof of != "undefined") && (ot.length > 0 || of.length > 0)) {
					var newOptions = {};
					for(var i = 0; i < ot.length; i++) {
						newOptions[ot[i]] = true;
					}
					for(var i = 0; i < of.length; i++) {
						newOptions[of[i]] = false;
					}
					$.extend(settings, {options:newOptions});
				}
				
				for(var i = 0; i < settings.digit; i++) {
					var setFlag = settings.options[i];
					var checkFlag = 0x1 << i;
					if(typeof setFlag != "undefined" && typeof setFlag === "boolean") {
						if(setFlag) {
							key |= checkFlag;
						} else {
							key &= ~checkFlag;
						}
					}
				}
				return key;
			}
		} else {
			return null;
		}
	};
})(jQuery);


/*
 * jQuery MD5 Plugin 1.2.1
 * https://github.com/blueimp/jQuery-MD5
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 * 
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/*jslint bitwise: true */
/*global unescape, jQuery */

(function ($) {
    'use strict';

    /*
    * Add integers, wrapping at 2^32. This uses 16-bit operations internally
    * to work around bugs in some JS interpreters.
    */
    function safe_add(x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF),
            msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    /*
    * Bitwise rotate a 32-bit number to the left.
    */
    function bit_rol(num, cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }

    /*
    * These functions implement the four basic operations the algorithm uses.
    */
    function md5_cmn(q, a, b, x, s, t) {
        return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
    }
    function md5_ff(a, b, c, d, x, s, t) {
        return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }
    function md5_gg(a, b, c, d, x, s, t) {
        return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }
    function md5_hh(a, b, c, d, x, s, t) {
        return md5_cmn(b ^ c ^ d, a, b, x, s, t);
    }
    function md5_ii(a, b, c, d, x, s, t) {
        return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
    }

    /*
    * Calculate the MD5 of an array of little-endian words, and a bit length.
    */
    function binl_md5(x, len) {
        /* append padding */
        x[len >> 5] |= 0x80 << ((len) % 32);
        x[(((len + 64) >>> 9) << 4) + 14] = len;

        var i, olda, oldb, oldc, oldd,
            a =  1732584193,
            b = -271733879,
            c = -1732584194,
            d =  271733878;

        for (i = 0; i < x.length; i += 16) {
            olda = a;
            oldb = b;
            oldc = c;
            oldd = d;

            a = md5_ff(a, b, c, d, x[i],       7, -680876936);
            d = md5_ff(d, a, b, c, x[i +  1], 12, -389564586);
            c = md5_ff(c, d, a, b, x[i +  2], 17,  606105819);
            b = md5_ff(b, c, d, a, x[i +  3], 22, -1044525330);
            a = md5_ff(a, b, c, d, x[i +  4],  7, -176418897);
            d = md5_ff(d, a, b, c, x[i +  5], 12,  1200080426);
            c = md5_ff(c, d, a, b, x[i +  6], 17, -1473231341);
            b = md5_ff(b, c, d, a, x[i +  7], 22, -45705983);
            a = md5_ff(a, b, c, d, x[i +  8],  7,  1770035416);
            d = md5_ff(d, a, b, c, x[i +  9], 12, -1958414417);
            c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
            b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
            a = md5_ff(a, b, c, d, x[i + 12],  7,  1804603682);
            d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
            c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
            b = md5_ff(b, c, d, a, x[i + 15], 22,  1236535329);

            a = md5_gg(a, b, c, d, x[i +  1],  5, -165796510);
            d = md5_gg(d, a, b, c, x[i +  6],  9, -1069501632);
            c = md5_gg(c, d, a, b, x[i + 11], 14,  643717713);
            b = md5_gg(b, c, d, a, x[i],      20, -373897302);
            a = md5_gg(a, b, c, d, x[i +  5],  5, -701558691);
            d = md5_gg(d, a, b, c, x[i + 10],  9,  38016083);
            c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
            b = md5_gg(b, c, d, a, x[i +  4], 20, -405537848);
            a = md5_gg(a, b, c, d, x[i +  9],  5,  568446438);
            d = md5_gg(d, a, b, c, x[i + 14],  9, -1019803690);
            c = md5_gg(c, d, a, b, x[i +  3], 14, -187363961);
            b = md5_gg(b, c, d, a, x[i +  8], 20,  1163531501);
            a = md5_gg(a, b, c, d, x[i + 13],  5, -1444681467);
            d = md5_gg(d, a, b, c, x[i +  2],  9, -51403784);
            c = md5_gg(c, d, a, b, x[i +  7], 14,  1735328473);
            b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

            a = md5_hh(a, b, c, d, x[i +  5],  4, -378558);
            d = md5_hh(d, a, b, c, x[i +  8], 11, -2022574463);
            c = md5_hh(c, d, a, b, x[i + 11], 16,  1839030562);
            b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
            a = md5_hh(a, b, c, d, x[i +  1],  4, -1530992060);
            d = md5_hh(d, a, b, c, x[i +  4], 11,  1272893353);
            c = md5_hh(c, d, a, b, x[i +  7], 16, -155497632);
            b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
            a = md5_hh(a, b, c, d, x[i + 13],  4,  681279174);
            d = md5_hh(d, a, b, c, x[i],      11, -358537222);
            c = md5_hh(c, d, a, b, x[i +  3], 16, -722521979);
            b = md5_hh(b, c, d, a, x[i +  6], 23,  76029189);
            a = md5_hh(a, b, c, d, x[i +  9],  4, -640364487);
            d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
            c = md5_hh(c, d, a, b, x[i + 15], 16,  530742520);
            b = md5_hh(b, c, d, a, x[i +  2], 23, -995338651);

            a = md5_ii(a, b, c, d, x[i],       6, -198630844);
            d = md5_ii(d, a, b, c, x[i +  7], 10,  1126891415);
            c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
            b = md5_ii(b, c, d, a, x[i +  5], 21, -57434055);
            a = md5_ii(a, b, c, d, x[i + 12],  6,  1700485571);
            d = md5_ii(d, a, b, c, x[i +  3], 10, -1894986606);
            c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
            b = md5_ii(b, c, d, a, x[i +  1], 21, -2054922799);
            a = md5_ii(a, b, c, d, x[i +  8],  6,  1873313359);
            d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
            c = md5_ii(c, d, a, b, x[i +  6], 15, -1560198380);
            b = md5_ii(b, c, d, a, x[i + 13], 21,  1309151649);
            a = md5_ii(a, b, c, d, x[i +  4],  6, -145523070);
            d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
            c = md5_ii(c, d, a, b, x[i +  2], 15,  718787259);
            b = md5_ii(b, c, d, a, x[i +  9], 21, -343485551);

            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
        }
        return [a, b, c, d];
    }

    /*
    * Convert an array of little-endian words to a string
    */
    function binl2rstr(input) {
        var i,
            output = '';
        for (i = 0; i < input.length * 32; i += 8) {
            output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xFF);
        }
        return output;
    }

    /*
    * Convert a raw string to an array of little-endian words
    * Characters >255 have their high-byte silently ignored.
    */
    function rstr2binl(input) {
        var i,
            output = [];
        output[(input.length >> 2) - 1] = undefined;
        for (i = 0; i < output.length; i += 1) {
            output[i] = 0;
        }
        for (i = 0; i < input.length * 8; i += 8) {
            output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (i % 32);
        }
        return output;
    }

    /*
    * Calculate the MD5 of a raw string
    */
    function rstr_md5(s) {
        return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
    }

    /*
    * Calculate the HMAC-MD5, of a key and some data (raw strings)
    */
    function rstr_hmac_md5(key, data) {
        var i,
            bkey = rstr2binl(key),
            ipad = [],
            opad = [],
            hash;
        ipad[15] = opad[15] = undefined;                        
        if (bkey.length > 16) {
            bkey = binl_md5(bkey, key.length * 8);
        }
        for (i = 0; i < 16; i += 1) {
            ipad[i] = bkey[i] ^ 0x36363636;
            opad[i] = bkey[i] ^ 0x5C5C5C5C;
        }
        hash = binl_md5(ipad.concat(rstr2binl(data)), 512 + data.length * 8);
        return binl2rstr(binl_md5(opad.concat(hash), 512 + 128));
    }

    /*
    * Convert a raw string to a hex string
    */
    function rstr2hex(input) {
        var hex_tab = '0123456789abcdef',
            output = '',
            x,
            i;
        for (i = 0; i < input.length; i += 1) {
            x = input.charCodeAt(i);
            output += hex_tab.charAt((x >>> 4) & 0x0F) +
                hex_tab.charAt(x & 0x0F);
        }
        return output;
    }

    /*
    * Encode a string as utf-8
    */
    function str2rstr_utf8(input) {
        return unescape(encodeURIComponent(input));
    }

    /*
    * Take string arguments and return either raw or hex encoded strings
    */
    function raw_md5(s) {
        return rstr_md5(str2rstr_utf8(s));
    }
    function hex_md5(s) {
        return rstr2hex(raw_md5(s));
    }
    function raw_hmac_md5(k, d) {
        return rstr_hmac_md5(str2rstr_utf8(k), str2rstr_utf8(d));
    }
    function hex_hmac_md5(k, d) {
        return rstr2hex(raw_hmac_md5(k, d));
    }
    
    $.md5 = function (string, key, raw) {
        if (!key) {
            if (!raw) {
                return hex_md5(string);
            } else {
                return raw_md5(string);
            }
        }
        if (!raw) {
            return hex_hmac_md5(key, string);
        } else {
            return raw_hmac_md5(key, string);
        }
    };
    
}(typeof jQuery === 'function' ? jQuery : this));


/**
 * jQuery open Url
 * @author Wen
 * @Open
 * 		String $.openURL(String url, String target, String extendOptions)
 */
(function($){
	$.openURL = function(url, target, extendOptions){
		if(target){
			window.open(url, target, extendOptions);
		}else{
			window.open(url);
		}
	};
})(jQuery);


/**
 * jQuery jump Url
 * @author Wen
 * @Open
 * 		String $.jumpURL(String url)
 */
(function($){
	$.jumpURL = function(url){
		window.location.href = url;
	};
})(jQuery);

/**
 * jQuery hash plugin used for storage hash name-value pairs
 * into browser locaiton's hash string.
 *
 * You can set or save hash name-value
 * @ $.hash("name", "value");
 *
 * You can get hash name's value
 * @ $.hash("name");
 *
 * You can delete hash name-value pairs
 * @ $.hash("name", null);
 * 
 * create by Charles
 */
(function($){
	$.hash = function(name, value) {
		function isString(obj) {
			return typeof obj == "string" || Object.prototype.toString.call(obj) === "[object String]";
		}
		if (!isString(name) || name == "") {
			return;
		}
	
		var clearReg = new RegExp("(&" + name + "=[^&]*)|(\\b" + name + "=[^&]*&)|(\\b" + name + "=[^&]*)", "ig");
		var getReg   = new RegExp("&*\\b" + name + "=[^&]*", "i");
		if (typeof value == "undefined") {
			// get name-value pair's value
			var result = window.location.hash.match(getReg);
			var splitIndex = result?result[0].indexOf("="):-1;
			if( splitIndex != -1 ){
				result = $.trim(result[0].substring(splitIndex+1,result[0].length));
			}else{
				return null;
			}
			//firfox的会默认decode了参数value
			return Fai.isMozilla() ? result : decodeURIComponent( result );
		}
		else if (value === null) {
			// remove a specific name-value pair
			window.location.hash = window.location.hash.replace(clearReg, "");
		}
		else {
			value = value + "";
	
			// clear all relative name-value pairs 
			var temp = window.location.hash.replace(clearReg, "");
	
			// build a new hash value-pair to save it
			temp += ((temp.indexOf("=") != -1) ? "&" : "") + name + "=" + encodeURIComponent(value);
			window.location.hash = temp;
		}
	};
})(jQuery);


/*
 * jQuery Numeric
 * Version 1.3.1
 * Demo: http://www.texotela.co.uk/code/jquery/numeric/
 *
 */
(function($) {
/*
 * Allows only valid characters to be entered into input boxes.
 * Note: fixes value when pasting via Ctrl+V, but not when using the mouse to paste
  *      side-effect: Ctrl+A does not work, though you can still use the mouse to select (or double-click to select all)
 *
 * @name     numeric
 * @param    config      { decimal : "." , negative : true }
 * @param    callback     A function that runs if the number is not valid (fires onblur)
 * @author   Sam Collett (http://www.texotela.co.uk)
 * @example  $(".numeric").numeric();
 * @example  $(".numeric").numeric(","); // use , as separator
 * @example  $(".numeric").numeric({ decimal : "," }); // use , as separator
 * @example  $(".numeric").numeric({ negative : false }); // do not allow negative values
 * @example  $(".numeric").numeric(null, callback); // use default values, pass on the 'callback' function
 *
 */
$.fn.numeric = function(config, callback)
{
	if(typeof config === 'boolean')
	{
		config = { decimal: config };
	}
	config = config || {};
	// if config.negative undefined, set to true (default is to allow negative numbers)
	if(typeof config.negative == "undefined") { config.negative = true; }
	// set decimal point
	var decimal = (config.decimal === false) ? "" : config.decimal || ".";
	// allow negatives
	var negative = (config.negative === true) ? true : false;
	// callback function
	callback = (typeof(callback) == "function" ? callback : function() {});
	// set data and methods
	return this.data("numeric.decimal", decimal).data("numeric.negative", negative).data("numeric.callback", callback).keypress($.fn.numeric.keypress).keyup($.fn.numeric.keyup).blur($.fn.numeric.blur);
};

$.fn.numeric.keypress = function(e)
{
	// get decimal character and determine if negatives are allowed
	var decimal = $.data(this, "numeric.decimal");
	var negative = $.data(this, "numeric.negative");
	// get the key that was pressed
	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	// allow enter/return key (only when in an input box)
	if(key == 13 && this.nodeName.toLowerCase() == "input")
	{
		return true;
	}
	else if(key == 13)
	{
		return false;
	}
	var allow = false;
	// allow Ctrl+A
	if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) { return true; }
	// allow Ctrl+X (cut)
	if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) { return true; }
	// allow Ctrl+C (copy)
	if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) { return true; }
	// allow Ctrl+Z (undo)
	if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) { return true; }
	// allow or deny Ctrl+V (paste), Shift+Ins
	if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */ ||
	  (e.shiftKey && key == 45)) { return true; }
	// if a number was not pressed
	if(key < 48 || key > 57)
	{
	  var value = $(this).val();
		/* '-' only allowed at start and if negative numbers allowed */
		if(value.indexOf("-") !== 0 && negative && key == 45 && (value.length === 0 || parseInt($.fn.getSelectionStart(this), 10) === 0)) { return true; }
		/* only one decimal separator allowed */
		if(decimal && key == decimal.charCodeAt(0) && value.indexOf(decimal) != -1)
		{
			allow = false;
		}
		// check for other keys that have special purposes
		if(
			key != 8 /* backspace */ &&
			key != 9 /* tab */ &&
			key != 13 /* enter */ &&
			key != 35 /* end */ &&
			key != 36 /* home */ &&
			key != 37 /* left */ &&
			key != 39 /* right */ &&
			key != 46 /* del */
		)
		{
			allow = false;
		}
		else
		{
			// for detecting special keys (listed above)
			// IE does not support 'charCode' and ignores them in keypress anyway
			if(typeof e.charCode != "undefined")
			{
				// special keys have 'keyCode' and 'which' the same (e.g. backspace)
				if(e.keyCode == e.which && e.which !== 0)
				{
					allow = true;
					// . and delete share the same code, don't allow . (will be set to true later if it is the decimal point)
					if(e.which == 46) { allow = false; }
				}
				// or keyCode != 0 and 'charCode'/'which' = 0
				else if(e.keyCode !== 0 && e.charCode === 0 && e.which === 0)
				{
					allow = true;
				}
			}
		}
		// if key pressed is the decimal and it is not already in the field
		if(decimal && key == decimal.charCodeAt(0))
		{
			if(value.indexOf(decimal) == -1)
			{
				allow = true;
			}
			else
			{
				allow = false;
			}
		}
	}
	else
	{
		allow = true;
	}
	return allow;
};



$.fn.numeric.keyup = function(e)
{
	var val = $(this).val();
	if(val && val.length > 0)
	{
		// get carat (cursor) position
		var carat = $.fn.getSelectionStart(this);
		var selectionEnd = $.fn.getSelectionEnd(this);
		// get decimal character and determine if negatives are allowed
		var decimal = $.data(this, "numeric.decimal");
		var negative = $.data(this, "numeric.negative");
		
		// prepend a 0 if necessary
		if(decimal !== "" && decimal !== null)
		{
			// find decimal point
			var dot = val.indexOf(decimal);
			// if dot at start, add 0 before
			if(dot === 0)
			{
				this.value = "0" + val;
			}
			// if dot at position 1, check if there is a - symbol before it
			if(dot == 1 && val.charAt(0) == "-")
			{
				this.value = "-0" + val.substring(1);
			}
			val = this.value;
		}
		
		// if pasted in, only allow the following characters
		var validChars = [0,1,2,3,4,5,6,7,8,9,'-',decimal];
		// get length of the value (to loop through)
		var length = val.length;
		// loop backwards (to prevent going out of bounds)
		for(var i = length - 1; i >= 0; i--)
		{
			var ch = val.charAt(i);
			// remove '-' if it is in the wrong place
			if(i !== 0 && ch == "-")
			{
				val = val.substring(0, i) + val.substring(i + 1);
			}
			// remove character if it is at the start, a '-' and negatives aren't allowed
			else if(i === 0 && !negative && ch == "-")
			{
				val = val.substring(1);
			}
			var validChar = false;
			// loop through validChars
			for(var j = 0; j < validChars.length; j++)
			{
				// if it is valid, break out the loop
				if(ch == validChars[j])
				{
					validChar = true;
					break;
				}
			}
			// if not a valid character, or a space, remove
			if(!validChar || ch == " ")
			{
				val = val.substring(0, i) + val.substring(i + 1);
			}
		}
		// remove extra decimal characters
		var firstDecimal = val.indexOf(decimal);
		if(firstDecimal > 0)
		{
			for(var k = length - 1; k > firstDecimal; k--)
			{
				var chch = val.charAt(k);
				// remove decimal character
				if(chch == decimal)
				{
					val = val.substring(0, k) + val.substring(k + 1);
				}
			}
		}
		// set the value and prevent the cursor moving to the end
		this.value = val;
		$.fn.setSelection(this, [carat, selectionEnd]);
	}
};

$.fn.numeric.blur = function()
{
	var decimal = $.data(this, "numeric.decimal");
	var callback = $.data(this, "numeric.callback");
	var val = this.value;
	if(val !== "")
	{
		var re = new RegExp("^\\d+$|^\\d*" + decimal + "\\d+$");
		if(!re.exec(val))
		{
			callback.apply(this);
		}
	}
};

$.fn.removeNumeric = function()
{
	return this.data("numeric.decimal", null).data("numeric.negative", null).data("numeric.callback", null).unbind("keypress", $.fn.numeric.keypress).unbind("blur", $.fn.numeric.blur);
};

// Based on code from http://javascript.nwbox.com/cursor_position/ (Diego Perini <dperini@nwbox.com>)
$.fn.getSelectionStart = function(o)
{
	if (o.createTextRange)
	{
		var r = document.selection.createRange().duplicate();
		r.moveEnd('character', o.value.length);
		if (r.text === '') { return o.value.length; }
		return o.value.lastIndexOf(r.text);
	} else { return o.selectionStart; }
};

// Based on code from http://javascript.nwbox.com/cursor_position/ (Diego Perini <dperini@nwbox.com>)
$.fn.getSelectionEnd = function(o)
{
	if (o.createTextRange) {
		var r = document.selection.createRange().duplicate()
		r.moveStart('character', -o.value.length)
		return r.text.length
	} else return o.selectionEnd
}

// set the selection, o is the object (input), p is the position ([start, end] or just start)
$.fn.setSelection = function(o, p)
{
	// if p is number, start and end are the same
	if(typeof p == "number") { p = [p, p]; }
	// only set if p is an array of length 2
	if(p && p.constructor == Array && p.length == 2)
	{
		if (o.createTextRange)
		{
			var r = o.createTextRange();
			r.collapse(true);
			r.moveStart('character', p[0]);
			r.moveEnd('character', p[1]);
			r.select();
		}
		else if(o.setSelectionRange)
		{
			o.focus();
			o.setSelectionRange(p[0], p[1]);
		}
	}
};

})(jQuery);

/*
 * jQuery Browser Plugin v0.0.2
 * https://github.com/gabceb/jquery-browser-plugin
 * 重写jQuery默认的$.browser方法，以支持更多的浏览器及新版浏览器
 *
 * Date: 2013-07-29T17:23:27-07:00
 */

(function( jQuery, window, undefined ) {
	"use strict";
	
	var matched, browser;
	
	jQuery.uaMatch = function( ua ) {
		ua = ua.toLowerCase();
		
		var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
			/(chrome)[ \/]([\w.]+)/.exec( ua ) ||
			/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
			/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
			/(msie) ([\w.]+)/.exec( ua ) ||
			ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
			ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
			[];
		
		var platform_match = /(ipad)/.exec( ua ) ||
			/(iphone)/.exec( ua ) ||
			/(android)/.exec( ua ) ||
			[];
		
		return {
			browser: match[ 1 ] || "",
			version: match[ 2 ] || "0",
			platform: platform_match[0] || ""
		};
	};
	
	matched = jQuery.uaMatch( window.navigator.userAgent );
	browser = {};
	
	if ( matched.browser ) {
		browser[ matched.browser ] = true;
		browser.version = matched.version;
	}
	
	if ( matched.platform) {
		browser[ matched.platform ] = true
	}
	
	// Chrome and Opera 15+ are Webkit, but Webkit is also Safari.
	if ( browser.chrome || browser.opr) {
		browser.webkit = true;
	} else if ( browser.webkit ) {
		browser.safari = true;
	}
	
	// IE11 has a new token so we will assign it msie to avoid breaking changes
	if (browser.rv) {
		browser.msie = true;
	}
	
	// Opera 15+ are identified as opr
	if (browser.opr) {
		browser.opera = true;
	}
	
	jQuery.browser = browser;

})( jQuery, window );

/*
 * jquery.removecss.js
 * Remove multiple properties from an element in your DOM.
 *
 * @param {Array|Object|String} css
 */
(function($){
	$.fn.removeCss = function(css) {
		var properties = [];
		var type = $.type(css);
		if (type === 'array') {
			properties = css;	
		} else if (type === 'object') {
			for (var rule in css) {
				properties.push(rule);
			}
		} else if (type === 'string') {
			properties = css.replace(/,$/, '').split(',');
		}
		
		return this.each(function() {
			var thisObj = $(this);
			$.map(properties, function(prop) {
				thisObj.css(prop, '');
			});
		});
	}
})(jQuery);


/**
 * jQuery livequery
 * Version: 1.1.1
 * Requires jQuery 1.3+
 * Docs: http://docs.jquery.com/Plugins/livequery
 */

(function($) {
	
	$.extend($.fn, {
		livequery: function(type, fn, fn2) {
			var self = this, q;
	
			// Handle different call patterns
			if ($.isFunction(type))
				fn2 = fn, fn = type, type = undefined;
	
			// See if Live Query already exists
			$.each( $.livequery.queries, function(i, query) {
				if ( self.selector == query.selector && self.context == query.context &&
					type == query.type && (!fn || fn.$lqguid == query.fn.$lqguid) && (!fn2 || fn2.$lqguid == query.fn2.$lqguid) )
						// Found the query, exit the each loop
						return (q = query) && false;
			});
	
			// Create new Live Query if it wasn't found
			q = q || new $.livequery(this.selector, this.context, type, fn, fn2);
	
			// Make sure it is running
			q.stopped = false;
	
			// Run it immediately for the first time
			q.run();
	
			// Contnue the chain
			return this;
		},
	
		expire: function(type, fn, fn2) {
			var self = this;
	
			// Handle different call patterns
			if ($.isFunction(type))
				fn2 = fn, fn = type, type = undefined;
	
			// Find the Live Query based on arguments and stop it
			$.each( $.livequery.queries, function(i, query) {
				if ( self.selector == query.selector && self.context == query.context &&
					(!type || type == query.type) && (!fn || fn.$lqguid == query.fn.$lqguid) && (!fn2 || fn2.$lqguid == query.fn2.$lqguid) && !this.stopped )
						$.livequery.stop(query.id);
			});
	
			// Continue the chain
			return this;
		}
	});
	
	$.livequery = function(selector, context, type, fn, fn2) {
		this.selector = selector;
		this.context  = context;
		this.type     = type;
		this.fn       = fn;
		this.fn2      = fn2;
		this.elements = [];
		this.stopped  = false;
	
		// The id is the index of the Live Query in $.livequery.queries
		this.id = $.livequery.queries.push(this)-1;
	
		// Mark the functions for matching later on
		fn.$lqguid = fn.$lqguid || $.livequery.guid++;
		if (fn2) fn2.$lqguid = fn2.$lqguid || $.livequery.guid++;
	
		// Return the Live Query
		return this;
	};
	
	$.livequery.prototype = {
		stop: function() {
			var query = this;
	
			if ( this.type )
				// Unbind all bound events
				this.elements.unbind(this.type, this.fn);
			else if (this.fn2)
				// Call the second function for all matched elements
				this.elements.each(function(i, el) {
					query.fn2.apply(el);
				});
	
			// Clear out matched elements
			this.elements = [];
	
			// Stop the Live Query from running until restarted
			this.stopped = true;
		},
	
		run: function() {
			// Short-circuit if stopped
			if ( this.stopped ) return;
			var query = this;
	
			var oEls = this.elements,
				els  = $(this.selector, this.context),
				nEls = els.not(oEls);
	
			// Set elements to the latest set of matched elements
			this.elements = els;
	
			if (this.type) {
				// Bind events to newly matched elements
				nEls.bind(this.type, this.fn);
	
				// Unbind events to elements no longer matched
				if (oEls.length > 0)
					$.each(oEls, function(i, el) {
						if ( $.inArray(el, els) < 0 )
							$.event.remove(el, query.type, query.fn);
					});
			}
			else {
				// Call the first function for newly matched elements
				nEls.each(function() {
					query.fn.apply(this);
				});
	
				// Call the second function for elements no longer matched
				if ( this.fn2 && oEls.length > 0 )
					$.each(oEls, function(i, el) {
						if ( $.inArray(el, els) < 0 )
							query.fn2.apply(el);
					});
			}
		}
	};
	
	$.extend($.livequery, {
		guid: 0,
		queries: [],
		queue: [],
		running: false,
		timeout: null,
	
		checkQueue: function() {
			if ( $.livequery.running && $.livequery.queue.length ) {
				var length = $.livequery.queue.length;
				// Run each Live Query currently in the queue
				while ( length-- )
					$.livequery.queries[ $.livequery.queue.shift() ].run();
			}
		},
	
		pause: function() {
			// Don't run anymore Live Queries until restarted
			$.livequery.running = false;
		},
	
		play: function() {
			// Restart Live Queries
			$.livequery.running = true;
			// Request a run of the Live Queries
			$.livequery.run();
		},
	
		registerPlugin: function() {
			$.each( arguments, function(i,n) {
				// Short-circuit if the method doesn't exist
				if (!$.fn[n]) return;
	
				// Save a reference to the original method
				var old = $.fn[n];
	
				// Create a new method
				$.fn[n] = function() {
					// Call the original method
					var r = old.apply(this, arguments);
	
					// Request a run of the Live Queries
					$.livequery.run();
	
					// Return the original methods result
					return r;
				}
			});
		},
	
		run: function(id) {
			if (id != undefined) {
				// Put the particular Live Query in the queue if it doesn't already exist
				if ( $.inArray(id, $.livequery.queue) < 0 )
					$.livequery.queue.push( id );
			}
			else
				// Put each Live Query in the queue if it doesn't already exist
				$.each( $.livequery.queries, function(id) {
					if ( $.inArray(id, $.livequery.queue) < 0 )
						$.livequery.queue.push( id );
				});
	
			// Clear timeout if it already exists
			if ($.livequery.timeout) clearTimeout($.livequery.timeout);
			// Create a timeout to check the queue and actually run the Live Queries
			$.livequery.timeout = setTimeout($.livequery.checkQueue, 20);
		},
	
		stop: function(id) {
			if (id != undefined)
				// Stop are particular Live Query
				$.livequery.queries[ id ].stop();
			else
				// Stop all Live Queries
				$.each( $.livequery.queries, function(id) {
					$.livequery.queries[ id ].stop();
				});
		}
	});
	
	// Register core DOM manipulation methods
	$.livequery.registerPlugin('append', 'prepend', 'after', 'before', 'wrap', 'attr', 'removeAttr', 'addClass', 'removeClass', 'toggleClass', 'empty', 'remove', 'html');
	
	// Run Live Queries when the Document is ready
	$(function() { $.livequery.play(); });

})(jQuery);

/**
 * compareObjNew(obj1, obj2, order, key, type)
 */
Fai.compareObjNew = function(obj1, obj2, order, key, keyType){
	var valA = obj1[key];
	var valB = obj2[key];
	
	if (typeof valA === "undefined" || typeof valB === "undefined") {
		return;
	}
	
	// 如果有传type是number。这里特殊处理
	if (keyType === "number") {
		return Fai.ber(valA, valB, order);
	}
	
	// 如果是IE 6\7\8，调用浏览器自带的排序，自带排序不会太卡
	if (Fai.isIE6() || Fai.isIE7() || Fai.isIE8()) {
		return Fai.compareObjLocale(obj1, obj2, order, key);
	}
	
	return Fai.compareStrings(valA, valB, order);
}

/**
 * compareObjLocale(obj1, obj2, order, key)
 * use browser locale sort
 */
Fai.compareObjLocale = function(obj1, obj2, order, key){
	var valA = obj1[key];
	var valB = obj2[key];
	
	// change number to string
	if(typeof valA === "number" || typeof valA === "boolean"){
		valA = valA.toString();
	}
	if(typeof valB === "number" || typeof valB === "boolean"){
		valB = valB.toString();
	}
	
	if(order == "asc"){
		return valA.localeCompare(valB);
	}else{
		return valB.localeCompare(valA);
	}
};


/**
 * ber(obj1, obj2, order)
 */
Fai.ber = function( s1, s2, order ){
	if( s1 === s2 ){
		return 0;
	}
	if(order == "asc"){
		return s1 > s2 ? 1 : -1;
	}else{
		return s1 > s2 ? -1 : 1;
	}
};

/**
 * compareStrings(s1, s2, order)
 */
Fai.compareStrings = function(s1, s2, order){
	// change number to string
	if(typeof s1 === "number" || typeof s1 === "boolean"){
		s1 = s1.toString();
	}
	if(typeof s2 === "number" || typeof s2 === "boolean"){
		s2 = s2.toString();
	}
	
	var options = {
		str1	: s1,
		str2	: s2,
		len1	: s1.length,
		len2	: s2.length,
		pos1	: 0,
		pos2	: 0
	};

	var result = 0;
	while(result == 0 && options.pos1 < options.len1 && options.pos2 < options.len2){
		var ch1 = options.str1.charAt(options.pos1);
		var ch2 = options.str2.charAt(options.pos2);

		if(Fai.isDigit(ch1)){
			result = Fai.isDigit(ch2) ? bers(options) : -1;
		}else if(Fai.isChinese(ch1)){
			result = Fai.isChinese(ch2) ? comparePinyin(options) : 1;
		}else if(Fai.isLetter(ch1)){
			result = (Fai.isLetter(ch2) || Fai.isChinese(ch2)) ? compareOther(options, true) : 1;
		}else{
			result = Fai.isDigit(ch2) ? 1 : (Fai.isLetter(ch2) || Fai.isChinese(ch2)) ? -1 : compareOther(options, false);
		}

		options.pos1++;
		options.pos2++;
	}
	
	if(order == "asc"){
		return result == 0 ? options.len1 - options.len2 : result;
	}else{
		return -(result == 0 ? options.len1 - options.len2 : result);
	}
	
	// 内部方法bers
	function bers(options){
		var end1 = options.pos1 + 1;
		while(end1 < options.len1 && Fai.isDigit(options.str1.charAt(end1))){
			end1++;
		}
		var fullLen1 = end1 - options.pos1;
		while(options.pos1 < end1 && options.str1.charAt(options.pos1) == '0'){
			options.pos1++;
		}
	
		var end2 = options.pos2 + 1;
		while(end2 < options.len2 && Fai.isDigit(options.str2.charAt(end2))){
			end2++;
		}
		var fullLen2 = end2 - options.pos2;
		while(options.pos2 < end2 && options.str2.charAt(options.pos2) == '0'){
			options.pos2++;
		}
	
		var delta = (end1 - options.pos1) - (end2 - options.pos2);
		if(delta != 0){
			return delta;
		}
	
		while(options.pos1 < end1 && options.pos2 < end2){
			delta = options.str1.charCodeAt(options.pos1++) - options.str2.charCodeAt(options.pos2++);
			if(delta != 0){
				return delta;
			}
		}
	
		options.pos1--;
		options.pos2--;
	
		return fullLen2 - fullLen1;
	}
	
	// 内部方法compareOther
	function compareOther(options, isLetters){
		var ch1 = options.str1.charAt(options.pos1);
		var ch2 = options.str2.charAt(options.pos2);
	
		if(ch1 == ch2){
			return 0;
		}
	
		if(isLetters){
			ch1 = ch1.toUpperCase();
			ch2 = ch2.toUpperCase();
			if(ch1 != ch2){
				ch1 = ch1.toLowerCase();
				ch2 = ch2.toLowerCase();
			}
		}
		
		return ch1.charCodeAt(0) - ch2.charCodeAt(0);
	}
	
	// 内部方法comparePinyin
	function comparePinyin(options){
		var ch1 = options.str1.charAt(options.pos1);
		var ch2 = options.str2.charAt(options.pos2);
		
		if(ch1 == ch2){
			return 0;
		}
		
		var py1, py2;
		if(typeof Pinyin != "undefined"){
			py1 = Pinyin.getStringStriped(ch1, Pinyin.mode.LOWERCASE, true);
			py2 = Pinyin.getStringStriped(ch2, Pinyin.mode.LOWERCASE, true);
			return -Fai.compareStrings(py1, py2);
		}else{
			py1 = ch1.charCodeAt(0);
			py2 = ch2.charCodeAt(0);
			
			if(py1 == py2){
				return 0;
			}else{
				return py1 - py2;
			}
		}
	}
	
};

/**
 * 乱序
 */
Fai.compareRandom = function(obj1, obj2){
	return Math.random() > 0.5 ? (-1) : 1;
};

Fai.punycode = function(){
	var maxInt = 2147483647,
		floor = Math.floor,
		base = 36,
		tMin = 1,
		tMax = 26,
		initialBias = 72,
		stringFromCharCode = String.fromCharCode,
		damp = 700,
		delimiter = '-',
		baseMinusTMin = base - tMin,
		skew = 38,
		initialN = 128,
		errors = {
                'overflow': 'Overflow: input needs wider integers to process',
                'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
                'invalid-input': 'Invalid input'
        }; // 0x80
	
	function adapt(delta, numPoints, firstTime) {
		var k = 0;
		delta = firstTime ? floor(delta / damp) : delta >> 1;
		delta += floor(delta / numPoints);
		for (/* no initialization */; delta > baseMinusTMin * tMax >> 1; k += base) {
				delta = floor(delta / baseMinusTMin);
		}
		return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
	}
	
	function ucs2decode(string) {
		var output = [],
			counter = 0,
			length = string.length,
			value,
			extra;
		while (counter < length) {
			value = string.charCodeAt(counter++);
			if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
				// high surrogate, and there is a next character
				extra = string.charCodeAt(counter++);
				if ((extra & 0xFC00) == 0xDC00) { // low surrogate
						output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
						// unmatched surrogate; only append this code unit, in case the next
						// code unit is the high surrogate of a surrogate pair
						output.push(value);
						counter--;
				}
			} else {
				output.push(value);
			}
		}
		return output;
	}

	function digitToBasic(digit, flag) {
		//  0..25 map to ASCII a..z or A..Z
		// 26..35 map to ASCII 0..9
		return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
	}

	function basicToDigit(codePoint) {
		if (codePoint - 48 < 10) {
				return codePoint - 22;
		}
		if (codePoint - 65 < 26) {
				return codePoint - 65;
		}
		if (codePoint - 97 < 26) {
				return codePoint - 97;
		}
		return base;
	}
	
	 function ucs2encode(array) {
		return map(array, function(value) {
				var output = '';
				if (value > 0xFFFF) {
						value -= 0x10000;
						output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
						value = 0xDC00 | value & 0x3FF;
				}
				output += stringFromCharCode(value);
				return output;
		}).join('');
	}
	
	function map(array, fn) {
		var length = array.length;
		while (length--) {
				array[length] = fn(array[length]);
		}
		return array;
	}
	
	function encode(input) {
		var n,
			delta,
			handledCPCount,
			basicLength,
			bias,
			j,
			m,
			q,
			k,
			t,
			currentValue,
			output = [],
			/** `inputLength` will hold the number of code points in `input`. */
			inputLength,
			/** Cached calculation results */
			handledCPCountPlusOne,
			baseMinusT,
			qMinusT;
		
		// Convert the input in UCS-2 to Unicode
		input = ucs2decode(input);
		
		// Cache the length
		inputLength = input.length;
		
		// Initialize the state
		n = initialN;
		delta = 0;
		bias = initialBias;
		
		// Handle the basic code points
		for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				if (currentValue < 0x80) {
						output.push(stringFromCharCode(currentValue));
				}
		}
		
		handledCPCount = basicLength = output.length;
		
		// `handledCPCount` is the number of code points that have been handled;
		// `basicLength` is the number of basic code points.
		
		// Finish the basic string - if it is not empty - with a delimiter
		if (basicLength) {
				output.push(delimiter);
		}
		
		// Main encoding loop:
		while (handledCPCount < inputLength) {
		
			// All non-basic code points < n have been handled already. Find the next
			// larger one:
			for (m = maxInt, j = 0; j < inputLength; ++j) {
					currentValue = input[j];
					if (currentValue >= n && currentValue < m) {
							m = currentValue;
					}
			}
			
			// Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,

			// but guard against overflow
			handledCPCountPlusOne = handledCPCount + 1;
			if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
					throw RangeError(errors['overflow']);
			}
			
			delta += (m - n) * handledCPCountPlusOne;
			n = m;
			
			for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				
				if (currentValue < n && ++delta > maxInt) {
						throw RangeError(errors['overflow']);
				}
				
				if (currentValue == n) {
					// Represent delta as a generalized variable-length integer
					for (q = delta, k = base; /* no condition */; k += base) {
						t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
						if (q < t) {
								break;
						}
						qMinusT = q - t;
						baseMinusT = base - t;
						output.push(
								stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
						);
						q = floor(qMinusT / baseMinusT);
					}
			
					output.push(stringFromCharCode(digitToBasic(q, 0)));
					bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
					delta = 0;
					++handledCPCount;
				}
			}
			
			++delta;
			++n;
		
		}
		return output.join('');
	}
	function decode(input) {
		// Don't use UCS-2
		var output = [],
			inputLength = input.length,
			out,
			i = 0,
			n = initialN,
			bias = initialBias,
			basic,
			j,
			index,
			oldi,
			w,
			k,
			digit,
			t,
			/** Cached calculation results */
			baseMinusT;
	
		// Handle the basic code points: let `basic` be the number of input code
		// points before the last delimiter, or `0` if there is none, then copy
		// the first basic code points to the output.
	
		basic = input.lastIndexOf(delimiter);
		if (basic < 0) {
				basic = 0;
		}
	
		for (j = 0; j < basic; ++j) {
			// if it's not a basic code point
			if (input.charCodeAt(j) >= 0x80) {
				throw RangeError(errors['not-basic']);
			}
			output.push(input.charCodeAt(j));
		}
	
		// Main decoding loop: start just after the last delimiter if any basic code
		// points were copied; start at the beginning otherwise.
	
		for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */) {
	
			// `index` is the index of the next character to be consumed.
			// Decode a generalized variable-length integer into `delta`,
			// which gets added to `i`. The overflow checking is easier
			// if we increase `i` as we go, then subtract off its starting
			// value at the end to obtain `delta`.
			for (oldi = i, w = 1, k = base; /* no condition */; k += base) {

				if (index >= inputLength) {
						throw RangeError(errors['invalid-input']);
				}

				digit = basicToDigit(input.charCodeAt(index++));

				if (digit >= base || digit > floor((maxInt - i) / w)) {
					throw RangeError(errors['overflow']);
				}

				i += digit * w;
				t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

				if (digit < t) {
						break;
				}

				baseMinusT = base - t;
				if (w > floor(maxInt / baseMinusT)) {
					throw RangeError(errors['overflow']);
				}

				w *= baseMinusT;

			}

			out = output.length + 1;
			bias = adapt(i - oldi, out, oldi == 0);

			// `i` was supposed to wrap around from `out` to `0`,
			// incrementing `n` each time, so we'll fix that now:
			if (floor(i / out) > maxInt - n) {
				throw RangeError(errors['overflow']);
			}

			n += floor(i / out);
			i %= out;


			// Insert `n` at position `i` of the output
			output.splice(i++, 0, n);
	
		}
	
		return ucs2encode(output);
	}
	return {
		"encode" : encode,
		"decode" : decode
	};
}();

Fai.isFontIcon = function(iconId) {
	if (!iconId || iconId.length == 0 || iconId.length < "FontIcon_".length) {
		return false;
	}

	return iconId.substring(0, "FontIcon_".length) == "FontIcon_";
};

Fai.changeHeight = function(){

	var windowHeight = $(window).height()-40+"px";	
			$(".tableContainer").css("min-height",windowHeight);
			$(".tablePanel , .panel").css("min-height",windowHeight);
			if($.browser.msie) {
				$(".tableContainer").css("margin-top","0px");
				$(".tablePanel,.panel").css("margin-top","20px");
				$(".tableContainer").css("padding-bottom","20px");
				$(".tableContainer").css("height","auto");
				$(".tableContainer").css("padding-bottom","0px");		
				var tablePanelHeight=parseInt($(".tablePanel").height());		
				var IeHeight=tablePanelHeight+"px";
				if(tablePanelHeight>parseInt(windowHeight)){
					$(".tableContainer").css("height",tablePanelHeight);
					$(".tableContainer").css("padding-bottom","40px");			
				}		
			}

};