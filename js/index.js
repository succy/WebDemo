/**
 * 定义一个全局的临时数组，存放数据
 */
var jsonArray = [{
	"id": "1483668991282",
	"name": "张三",
	"birthday": "2010.1.1"
}, {
	"id": "1483668991293",
	"name": "李四",
	"birthday": "2011.1.1"
}, {
	"id": "1483668991299",
	"name": "王五",
	"birthday": "2012.1.1"
}];
// 设置一个全局变量保存点击的是“添加行”还是“修改”按钮
// var isAdd = true;
$(function() {
	// 使用本地数据生成

	createTable(jsonArray);
	// 调用ajax,直接在ajax请求成功返回之后生成表格
	//	ajax();

	// 可拖动的表格，使用jQuery插件实现的
	$(".fk-tb").sortable({
		cursor: "move",
		items: "tr:not(:first)", //设置只能拖动第一行以下的tr
		opacity: 0.6, //拖动时，透明度为0.6
		revert: true, //释放时，增加动画
		update: changeBtnState
	}); // end of sortable

	// 点击保存按钮
	$(".f-save").on('click', function() {
		var jsonStr = exchangeJson();
		alert(jsonStr);
		// 向服务器提交数据
		$.ajax({
			type: "get",
			url: "http://www.succy.iii.in/tmp/succy/parse.jsp",
			async: true,
			dataType: 'jsonp',
			data: {
				"jsonStr": jsonStr
			},
			jsonp: 'jsonpcallback',
			success: function(data) {
				console.log(data);
				console.log("----success---");
				alert(JSON.stringify(data));
			},
			error: function(err) {
				console.log(err);
				console.log("error");
			}
		});
	}); // end of save btn click

	// 点击添加行按钮
	$(".f-add").on('click', function() {
		$("#mask").show();
		createDlg('', '', true);
		
	}); // end of add btn click
});

/**
 * 生成表格的内容行
 * @param {Object} data 填充表格的数据
 */
function createTable(data) {
	//	动态生成表格行
	$.each(data, function(i, item) {
		createTr(item.id, item.name, item.birthday);
	});

}

/**
 * 使用ajax访问服务器，获取数据
 */
function ajax() {
	$.ajax({
		type: "get",
		url: "http://www.succy.iii.in/tmp/succy/index.jsp",
		async: true,
		dataType: 'jsonp',
		jsonp: 'jsonpcallback',
		success: function(data) {
			console.log(data);
			console.log("----success---")
			createTable(data);
		},
		error: function(err) {
			console.log(err);
			console.log("error");
		}
	});
}
/**
 * 动态生成对话框
 * @param {Object} name 对话框的名字输入框
 * @param {Object} birth 对话框的生日输入框
 * @param {Boolean} state true or false ‘true’表示此时点开对话框是
 * 做添加操作，‘false’表示此时点开对话框做的是更新操作
 * @param {Number} index 要修改的列的索引
 */
function createDlg(name, birth, state, index) {
	var el = "<div class='fk-dialog'>" +
		"<h3 class='f-dlg-title'>修改行</h3>" +
		"<div class='f-dlg-content fk-clearfix'>" +
		"<div class='f-content-name fk-form-group'>" +
		"<label for='name'>姓名：</label>" +
		"<input type='text' name='' id='name' value='" + name + "' />" +
		"</div>" +
		"<div class='f-content-name fk-form-group'>" +
		"<label for='birth'>生日：</label>" +
		"<input type='text' name='' id='birth' value='" + birth + "'/>" +
		"</div>" +
		"<div class='fk-form-group f-btn-group'>" +
		"<button type='button' class='mask-ok'>确定</button>" +
		"<button type='button' class='mask-cancel'>取消</button>" +
		"</div>" +
		"</div>" +
		"</div>";
		
		var dialog = $(el);
	$("body").append(dialog);

	// 为了兼容多浏览器，使用js做居中
	var d_height = $(window).height(); // 获取文档对象高度
	var d_width = $(window).width(); // 获取文档对象的宽度
	var p_height = dialog.height(); //获取弹出框的高度
	var p_width = dialog.width(); //获取弹出框的宽度
	// 计算偏移量
	var top = (d_height - p_height) / 2;
	var left = (d_width - p_width) / 2;
	dialog.css({
		'left': left + 'px',
		'top': top + 'px'
	});

	// 鼠标拖拽弹出框效果
	$(".fk-dialog .f-dlg-title").mousedown(function(e) {
		//	设置cursor
//		$(this).css('cursor', 'move');

		var m_old_x = e.pageX;
		var m_old_y = e.pageY;

		$(document).bind('mousemove', function(ev) {
			//alert(2);
			var m_new_x = ev.pageX;
			var m_new_y = ev.pageY;
			var diff_x = m_new_x - m_old_x;
			var diff_y = m_new_y - m_old_y;
			m_old_x = m_new_x;
			m_old_y = m_new_y;
			var pop_x = dialog.position().left + diff_x;
			var pop_y = dialog.position().top + diff_y;
			pop_x = pop_x < 0 ? 0 : pop_x;
			pop_x = pop_x >= ($(window).width() - dialog.outerWidth()) ? ($(window).width() - dialog.outerWidth()) : pop_x;
			pop_y = pop_y < 0 ? 0 : pop_y;
			pop_y = pop_y >= ($(window).height() - dialog.outerHeight()) ? ($(window).height() - dialog.outerHeight()) : pop_y;
			dialog.css({
				'top': pop_y + 'px',
				'left': pop_x + 'px'
			});
		});
	});

	$(document).mouseup(function() {
		$(document).unbind('mousemove');
//		$(".fk-dialog .f-dlg-title").css('cursor', 'default');
	})

	// 确定按钮
	$(".f-btn-group button.mask-ok").on("click", function() {
		if($('#name').val() == '') {
			alert('姓名不能为空');
			return;
		}
		var uName = $('#name').val();
		var uBirth = $('#birth').val();
		// 使用Fai.encodeHtml(html)去encode,防止XSS攻击
		uName = Fai.encodeHtml(uName);
		uBirth = Fai.encodeHtml(uBirth);
		// 如果state为true，调用添加到表格的方法
		if(state) {
			var id = new Date().getTime();
			addTotable(id, uName, uBirth);
		} else {
			// 此处做的是调用更新表格的方法
			updateTable(index, uName, uBirth);
		}

		console.log("uName:" + uName + " uBirth:" + uBirth);

		$('.fk-dialog').hide();
		$("#mask").hide();
		dialog.remove();
	});
	// 取消按钮
	$(".f-btn-group button.mask-cancel").on("click", function() {
		$('.fk-dialog').hide();
		$("#mask").hide();
		dialog.remove();
	});
}

/**
 * 解析表格，并将其数据封装成json对象的数组
 * @return {String} 把json数组转换成字符串返回
 */
function exchangeJson() {
	var array = [];
	var rows = $(".fk-tb tr").size();
	//	其实不需要知道有多少列的,我们只需要前面的两列
	//	console.log("rows count:" + rows + " cols count:" + cols);
	for(var i = 1; i < rows; i++) {
		var id = $(".fk-tb tr").eq(i).attr("uid");
		var name = $(".fk-tb tr").eq(i).find("td").eq(0).html();
		var birth = $(".fk-tb tr").eq(i).find("td").eq(1).html();

		var jsonObj = createJsonObj(id, name, birth);
		array.push(jsonObj);
	}

	return JSON.stringify(array);
}

/**
 * 根据传入的用户名和用户生日构造一个表格行
 * @param {Number} id 用户唯一标识
 * @param {Object} uName 用户名
 * @param {Object} uBirth 用户生日
 */
function createTr(id, uName, uBirth) {
	var tr = $('<tr></tr>');
	tr.attr("uid", id);
	var nameTd = "<td>" + uName + "</td>";
	tr.append(nameTd);
	var birthTd = "<td>" + uBirth + "</td>";
	tr.append(birthTd);

	var oprTd = $("<td></td>");
	var upBtn = $("<button class='fk-btn fk-primary f-btn-up' type='button'>上移</button>");
	var downBtn = $("<button class='fk-btn fk-primary f-btn-down' type='button'>下移</button>");
	var delBtn = $("<button class='fk-btn fk-danger f-btn-remove' type='button'>删除</button>");
	var updateBtn = $("<button class='fk-btn fk-success f-btn-update' type='button'>修改</button>");

	oprTd.append(upBtn);
	oprTd.append(downBtn);
	oprTd.append(delBtn);
	oprTd.append(updateBtn);
	tr.append(oprTd);
	$('.fk-tb tbody').append(tr);

	// 由于表格的行是动态生成的，因此，在表格生成之后才添加的事件要放在这里边
	// 上移按钮
	upBtn.on('click', function() {
		var curTr = $(this).parent('td').parent('tr');
		var index = curTr.index();
		if(index > 0) {
			var preTr = $(".fk-tb tbody tr").eq(index - 1);
			curTr.insertBefore(preTr);
		}
		changeBtnState();
	});
	// 下移按钮
	downBtn.on('click', function() {
		//var size = $(".fk-tb tbody tr").size();

		//console.log("size --> " + size);
		var curTr = $(this).parent('td').parent('tr');
		var index = curTr.index();
		var nextTr = $(".fk-tb tbody tr").eq(index + 1);

		curTr.insertAfter(nextTr);
		changeBtnState();
	})

	// 修改按钮
	updateBtn.on('click', function() {
		var tr = $(this).parent('td').parent('tr');
		var name = tr.find('td:first-child').html();
		var birth = tr.find('td:nth-child(2)').html();

		//	console.log("name: " + name + "birth: " + birth);
		var index = tr.index();
		console.log("tr index-->" + index);
		$("#mask").show();
		createDlg(name, birth, false, index);
	});

	// 删除按钮
	delBtn.on('click', function() {
		var tr = $(this).parent('td').parent('tr');
		var uid = tr.attr("uid");
		tr.remove();

		//	console.log("tr uid==>" + uid);
		$.each(jsonArray, function(index, item) {
			//	console.log("item uid -->" + item.id)
			if(item.id == uid) {
				//	console.log(index)
				jsonArray.splice(index, 1);
				return false;
			}
		});

		// 打印jsonArray，验证是否删除
		console.log(jsonArray);
		// 修改一下按钮的状态
		changeBtnState();
	});

}

/**
 * 将一行信息插入表格中
 * @param {Object} uName 用户姓名
 * @param {Object} uBirth 用户生日
 */
function addTotable(id, uName, uBirth) {
	// 把数据添加到表格中
	createTr(id, uName, uBirth);
	// 把数据添加到json数组中
	var jsonObj = createJsonObj(id, uName, uBirth);
	jsonArray.push(jsonObj);

	// 控制台打印输出一下看是否已经将数据添加到jsonArray里
	console.log(jsonArray);
	changeBtnState();
}

/**
 * 更新表格行操作
 * @param {Object} index 要更新的表格行索引
 * @param {Object} uName 要更新的用户名
 * @param {Object} uBirth 要更新的用户生日
 */
function updateTable(index, uName, uBirth) {
	var tr = $(".fk-tb tbody tr").eq(index);
	var uid = tr.attr("uid");
	tr.find('td').eq(0).html(uName);
	tr.find('td').eq(1).html(uBirth);

	//	console.log("uid==>"+uid);
	$.each(jsonArray, function(index, item) {
		console.log(item);
		if(item.id == uid) {
			//			console.log("id==>"+uid);
			item.name = uName;
			item.birthday = uBirth;
			// 跳出循环
			return false;
		}
	});

	// 打印输入jsonArray，以证明json的数据被修改了
	console.log(jsonArray);
}

/**
 * 构造json对象并返回
 * @param {Object} uName 用户名
 * @param {Object} uBirth 用户生日
 * @return {Object} jsonObj 封装好的jsonObj
 */
function createJsonObj(id, uName, uBirth) {
	var jsonObj = JSON.parse("{\"id\":\"\", \"name\":\"\", \"birthday\":\"\"}");
	jsonObj.id = id;
	jsonObj.name = uName;
	jsonObj.birthday = uBirth;

	return jsonObj;
}
/**
 * 改变按钮的状态，第一行的没有上移按钮，最后一行的没有下移按钮
 */
function changeBtnState() {
	$(".fk-tb tbody>tr>td:last-child").css({
		"textAlign": "right"
	});
	$(".fk-tb>tbody>tr>td>button").show();
	$(".fk-tb>tbody>tr:first-child>td:last-child>button:first-child").hide();
	$(".fk-tb>tbody>tr:last-child>td:last-child>button:nth-child(2)").hide();
}